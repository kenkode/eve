@extends('layouts.main')
@section('content')
<style>
.embed-container iframe,
.embed-container object,
.embed-container embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

@media (min-width: 1200px) {
    .card_story {
        height: 120px !important;
    }

    .main-cont {
        margin-top: 140px
    }
}

@media (max-width: 1200px) {
    .card_story {
        height: 100px !important;
    }

    .main-cont {
        margin-top: 100px
    }
}
</style>
<div class="container">
    <div id='div-gpt-ad-1513749913330-0' style='width:100%; text-align:center;'>
        <script>
        googletag.cmd.push(function() {
            googletag.display('div-gpt-ad-1513749913330-0');
        });
        </script>
    </div>

    <section class="mb-5 mt-3">
        <div id="article" class="container">
            <div class="row m-b-1">
                <div class="col-lg-12">
                    <h1 class="article-heading">Videos</h1>
                </div>
                <div class="row">
                    <?php
                    $dcnt = 0;
                    $d=[];
                    if(count($videos) > 0){
                    foreach ($videos as $allvideos){
                        $d=$allvideos;
                    foreach ($allvideos as $video){
                        $dcnt++;
                        ?>
                    <div class="col-md-3 mt-3 v-col">
                        <div class="card">
                            <a href="{{url('videos/watch/'.$video->id.'/'.Str::slug($video->title))}}">
                                <!-- <img class="card-img-top"
                                    src="{{'http://img.youtube.com/vi/'.$video->videoURL.'/0.jpg'}}"
                                    alt="No image found"
                                    onError="this.onerror=null;this.src='{{asset('/images/pic.jpg')}}';"> -->
                                <img src="{{ asset('/images/pic.jpg') }}"
                                    data-src="{{'http://img.youtube.com/vi/'.$video->videoURL.'/0.jpg'}}"
                                    class="lazy card-img-top" alt="{{$video->title}}">
                            </a>
                            <div class="card-body card-block">
                                <h3 class="h6"><a class="stext"
                                        href="{{url('videos/watch/'.$video->id.'/'.Str::slug($video->title))}}"
                                        class="card-link">{{$video->title}}</a></h3>
                            </div>
                        </div>
                    </div>
                    <?php

                    }}}
                        ?>
                    <div class="clearfix"></div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                {!! count($videos) > 0 ? $d->links():'' !!}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop