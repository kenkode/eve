@extends('layouts.main')
@section('content')
<style>
.pdfobject-container { height: 40rem; border: 1rem solid rgba(0,0,0,.1); }
#example1{
    margin-top:100px !important;
}
</style>

<div id="example1"></div>

<script async src="{{asset('assets/js/pdfobject.js')}}"></script>
<script>
PDFObject.embed("{{asset('assets/pdf/media kit eve.pdf')}}", "#example1");
</script>
@stop