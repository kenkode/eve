@extends('layouts.main')
@section('content')
<style>
.embed-container iframe,
.embed-container object,
.embed-container embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

@media (min-width: 1200px) {
    .card_story {
        height: 120px !important;
    }

    .main-cont {
        margin-top: 140px
    }
}

@media (max-width: 1200px) {
    .card_story {
        height: 100px !important;
    }

    .main-cont {
        margin-top: 100px
    }
}
</style>


<div class="container">
    <div id='div-gpt-ad-1513749913330-0' style='width:100%; text-align:center;'>
        <script>
        googletag.cmd.push(function() {
            googletag.display('div-gpt-ad-1513749913330-0');
        });
        </script>
    </div>

    <div class="row">
        <div class="col-md-8">
            @if(count($mainCatArticles) > 0)
            @foreach($mainCatArticles as $mainArticle)
            <a
                href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                <!-- <img src="https://cdn.standardmedia.co.ke<?php echo $mainArticle->thumbURL;?>"
                    class="card-img-top img-fluid mt-3 mb-4"
                    onError="this.onerror=null;this.src='{{asset('/images/pic.jpg')}};" alt="..."> -->
                <img src="{{ asset('/images/pic.jpg') }}"
                    data-src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                    class="lazy card-img-top mt-4 img-fluid" alt="{{$mainArticle->title}}">
            </a>
            <a href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}"
                class="">
                <h1>
                    <strong>
                        {{$mainArticle->title}}
                    </strong>
                </h1>
            </a>

            <span class="">
                <a href="{{url('category/' . $mainArticle->categoryid . '/' .  Str::slug(App\Eve::getCatName($mainArticle->categoryid)))}}"
                    style="color: #F62F5E">
                    <?php echo App\Eve::getCatName($mainArticle->categoryid); ?>
                </a>
                by
                <span class="bycolorpic">
                    <a href="{{url('author/' . $mainArticle->author_id . '/' . Str::slug($mainArticle->author))}}">
                        {{$mainArticle->author}}
                    </a>
                </span>
                <small class="text text-muted font-8">
                    - {{App\Eve::time_difference($mainArticle->publishdate)}}
                </small>

            </span>

            <br />
            <br />
            @endforeach
            @endif
        </div>

        <div class="col-md-3 ml-5 mt-3">
            <?php 
            if(count($sideCatLatest) > 0){
            foreach($sideCatLatest as $latest){

            if ($mainArticle->id == $latest->id){
            continue;
            }
            ?>
            <style>
            .side_text a {
                color: #000000;
                text-decoration: none;
                font-family: Futura Md BT;
            }
            </style>
            <p class="side_text">
                <small style="font-size: 15px">
                    <a href="{{url('category/' . $latest->categoryid . '/' . Str::slug(App\Eve::getCatName($latest->categoryid)))}}"
                        style="color: #F62F5E">
                        {{App\Eve::getCatName($latest->categoryid)}}
                    </a>
                </small>
                <br />
                <a href="{{url(Str::slug(App\Eve::getCatName($latest->categoryid)).'/article/' . $latest->id . '/' . Str::slug($latest->title))}}"
                    style="font-size: 18px" class="sidetitles">
                    {{$latest->title}}
                </a>
                <br />
                <small>
                    By
                    <a style="color: #a9a3a3"
                        href="{{url('author/' . $latest->author_id . '/' . Str::slug($latest->author))}}">
                        {{$latest->author}}
                        - {{App\Eve::time_difference($latest->publishdate)}}
                    </a>
                </small>
            </p>
            <hr />
            <?php }} ?>

            <div id='div-gpt-ad-1512394772255-0' style='width: 100%; text-align:center;'>
                <script>
                googletag.cmd.push(function() {
                    googletag.display('div-gpt-ad-1512394772255-0');
                });
                </script>
            </div>

        </div>

    </div>
</div>

<div class="container">
    <?php 
        if(count($bigCat) > 0){
        foreach($bigCat as $bigLatest){
        if ($mainArticle->id == $bigLatest->id || $latest->id == $bigLatest->id) {
            continue;
        }
        ?>
    <div class="row mt-4">
        <div class="col-md-6 linegrey py-4">

            <div class="titles mt-5">

                <div class="byline mb-2">
                    <a href="{{url(Str::slug(App\Eve::getCatName($bigLatest->categoryid)).'/article/' . $bigLatest->id . '/' . Str::slug($bigLatest->title))}}"
                        style="color: #F62F5E">
                        {{App\Eve::getCatName($bigLatest->categoryid)}}
                    </a>
                    by
                    <span class="bycolor mb-2">
                        <a href="{{url('author/' . $bigLatest->author_id . '/' . Str::slug($bigLatest->author))}}">
                            {{$bigLatest->author}}
                        </a>
                    </span>
                </div>

                <a href="{{url(Str::slug(App\Eve::getCatName($bigLatest->categoryid)).'/article/' . $bigLatest->id . '/' . Str::slug($bigLatest->title))}}"
                    style="font-size: 30px;color:#3a3a3a!important">
                    {{$bigLatest->title}}
                </a>
            </div>
            <div class="subtitles mt-3" style="font-size: 20px">
                {{$bigLatest->summary}}
            </div>
        </div>
        <div class=" col-md-6 linegrey py-4">

            <a
                href="{{url(Str::slug(App\Eve::getCatName($bigLatest->categoryid)).'/article/' . $bigLatest->id . '/' . Str::slug($bigLatest->title))}}">
                <!-- <img src="https://cdn.standardmedia.co.ke{{$bigLatest->thumbURL}}"
                    onError="this.onerror=null;this.src='{{asset('images/pic.jpg')}}';"
                    class="card-img-top img-fluid mt-4" alt="..."> -->
                <img src="{{ asset('/images/pic.jpg') }}"
                    data-src="{{'https://cdn.standardmedia.co.ke'.$bigLatest->thumbURL}}"
                    class="lazy card-img-top mt-4 img-fluid" alt="{{$bigLatest->title}}">
            </a>

        </div>
    </div>
    <?php }} ?>
</div>

<style>
.btn-danger {
    /* padding: 5px 150px 5px 150px; */
    font-size: 20px;
    border-radius: 0px;
    background: #ed008c
}

h2 {
    font-weight: 500;
    font-size: 18px;
    line-height: 24px;
}
</style>

<div class="container">
    <div class='row displayLoaded'></div>
</div>

<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col text-center">
            <div id='response'></div>
            <input type="hidden" id="current_limit" name="current_limit" value="6" />
            <input type="hidden" id="categoryid" name="categoryid" value="{{$categoryid}}" />
            <button type="button" id="topic-more" class="btn btn-danger">Load More</button>
        </div>
    </div>
</div>
@stop