<section id="footer" class="footer-area pt-75">
    <hr class="my-4">
    <div class="bggry">
        <div class="container">
            <div class="footer-widget  pb-120 ">
                <div class="d-flex flew-row justify-content-center">
                    <div class="mx-auto col-md-5 text-center ">
                        <div class="footer-logo text-left mt-40 ">
                            <h3 class="text-dark">Get Our Newsletter</h3>
                            <p class="mt-10 mb-4 text-dark">Subscribe to our newsletter and stay updated on the latest
                                developments and special offers!</p>
                            <form method="post" action="{{url('subscribe')}}">
                                @csrf
                                <input type="hidden" value="33" name="category_id" />
                                <input type="text" name="email" class="w-75" placeholder="Enter your email"
                                    required=""><button class="newslettericon ml-2" type="submit"> <i
                                        class="fas fa-chevron-right"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class=" bg-grey pl-3 w-100">
            <div class="container">
                <div class="row  bg-grey w-100">
                    <div class="mx-auto col-xs-12">
                        <ul class="nav nav-inline ">
                            <li class="nav-item">
                                <a class="nav-link active" href="https://www.standardmedia.co.ke/"><b>Eve from</b>
                                    <img src="https://www.standardmedia.co.ke/flash/standard_logo.png"></a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class=" bg-grey pl-3 w-100">
            <div class="container">
                <div class="row  bg-grey w-100">
                    <div class="mx-auto col-xs-12">
                        <ul class="nav nav-inline ">
                            <li class="nav-item">
                                <a class="nav-link active" href="https://www.standardmedia.co.ke/">THE STANDARD |</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="https://www.standardmedia.co.ke/ktnnews">
                                    KTN NEWS |</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="https://farmers.co.ke/">
                                    FARMKENYA |</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="https://www.standardmedia.co.ke/radiomaisha">
                                    RADIO MAISHA |</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.vas.standardmedia.co.ke/">BULK SMS |</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="http://tutorsoma.standardmedia.co.ke/">E LEARNING |</a>
                            </li>
                            <li class="nav-item border-0">
                                <a class="nav-link" href="https://www.standardmedia.co.ke/corporate">CORPORATE |</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="alert bg-black mt-3 text-center">
            @include('cookieConsent::index')
        </div>
        <div class="mx-auto col-xs-12">
            <nav class="text-center" style="background:#444444">
                <a class="nav-item text-white">
                    © 2020 Standard Group PLC
                </a>
            </nav>
        </div>
    </footer>
</section>
<!-- Core -->
<script async src="{{asset('eve/assets/vendor/jquery/jquery.min.js')}}"></script>
<!-- <script async src="{{asset('eve/assets/vendor/popper/popper.min.js')}}"></script> -->
<script async src="{{asset('eve/assets/js/bootstrap/bootstrap.min.js')}}"></script>
<!-- FontAwesome 5 -->
<script async src="{{asset('eve/assets/vendor/fontawesome/js/fontawesome-all.min.js')}}" defer></script>
<!-- Page plugins -->
<!-- Theme JS -->
<script async src="{{asset('eve/assets/js/theme.js')}}"></script>
<!-- <script async src="https://www.thenairobian.ke/assets/vendor/owl.carousel/owl.carousel.min.js"></script> -->
<script async src="{{asset('assets/js/pdfobject.js')}}"></script>
<script>
PDFObject.embed("{{asset('assets/pdf/media kit eve.pdf')}}", "#example1");
</script>
<script type="text/javascript">
$(document).ready(function() {
    document.title = '{{$name}}';
});
</script>
<script>
$(document).ready(function() {
    $('body').on('hidden.bs.modal', function() {
        if ($('.modal.in').length > 0) {
            $('body').addClass('modal-open');
        }
    });
    // var $window = $(window);
    // var $videoWrap = $('.video-wrap');
    // var $video = $('.video');
    // var videoHeight = $video.outerHeight();

    // $window.on('scroll', function() {
    //     var windowScrollTop = $window.scrollTop();
    //     var videoBottom = videoHeight + $videoWrap.offset().top;

    //     if (windowScrollTop > videoBottom) {
    //         $videoWrap.height(videoHeight);
    //         $video.addClass('stuck');
    //     } else {
    //         $videoWrap.height('auto');
    //         $video.removeClass('stuck');
    //     }
    // });
    // var password = document.getElementById("password"),
    //     confirm_password = document.getElementById("confirm_password");

    // function validatePassword() {
    //     if (password.value != confirm_password.value) {
    //         confirm_password.setCustomValidity("Passwords Don't Match");
    //     } else {
    //         confirm_password.setCustomValidity('');
    //     }
    // }

    // password.onchange = validatePassword;
    // confirm_password.onkeyup = validatePassword;
});
</script>

<!-- Modal -->
<div id="loginModals" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <!-- <div class="modal-header">
                <div class="modal-header border-bottom-0">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div> -->
            <div class="text-center auth-logo mb-1 mt-3">
                <img src="https://www.standardmedia.co.ke/flash/standard_logo.png" style="width:50%"
                    alt="Eve Woman Logo">
            </div>
            <div class="form-title text-center">
                <h4>Login with your Standard Account</h4>
            </div>
            <form method="post" action="{{url('login')}}">
                @csrf
                <div class="modal-body" style="margin-bottom:25px">
                    <div class="input-group">
                        <input type="email" placeholder="Email" name="email" class="form-control" required />
                    </div>

                    <div class="input-group mt-5">
                        <input type="password" placeholder="Password" name="password" class="form-control" required />
                    </div>
                    <div style="float:left;margin-top:20px" data-toggle="modal" style="cursor: pointer"
                        data-target="#registerModal" data-dismiss="modal">
                        <span style="cursor: pointer" class="text-danger">No account? Sign Up</span>
                    </div>
                    <div style="float:right;margin-top:20px" data-toggle="modal" style="cursor: pointer"
                        data-target="#forgotModal" data-dismiss="modal">
                        <span style="cursor: pointer" class="text-danger">Forgot Password?</span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" value="login" name="login"
                        class="btn btn-default">Close</button>
                    <button type="submit" value="login" name="login" class="btn btn-danger">Login</button>
                </div>

            </form>
        </div>

    </div>
</div>

<div id="forgotModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <!-- <div class="modal-header">
                <h4 class="modal-title">Forgot Password</h4>
            </div> -->
            <form method="post" action="{{ route('password.email') }}">
                @csrf
                <div class="modal-body">
                    <div class="text-center auth-logo mb-1">
                        <img src="https://www.standardmedia.co.ke/flash/standard_logo.png" style="width:50%"
                            alt="Eve Woman Logo">
                    </div>
                    <div class="form-title text-center">
                        <h4>Enter your registered Standard Account Email</h4>
                    </div>
                    <div class="input-group">
                        <input type="text" placeholder="Email" name="email" class="form-control" required />
                    </div>

                </div>

                <div data-toggle="modal" style="cursor: pointer" data-target="#loginModals" data-dismiss="modal">
                    <span style="cursor: pointer" class="ml-4 text-danger">Sign In</span>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" value="login" name="login"
                        class="btn btn-default">Close</button>
                    <button type="submit" value="login" name="login" class="btn btn-danger">Submit</button>
                </div>

            </form>
        </div>

    </div>
</div>

<div id="registerModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <!-- <div class="modal-header">
                <h4 class="modal-title">Register</h4>
            </div> -->

            <form method="post" enctype="multipart/form-data" action="{{url('register')}}">
                @csrf
                <div class="modal-body">
                    <div class="text-center auth-logo mb-1">
                        <img src="https://www.standardmedia.co.ke/flash/standard_logo.png" style="width:50%"
                            alt="Eve Woman Logo">
                    </div>
                    <div class="form-title text-center">
                        <h4>Sign Up for the Standand Account</h4>
                    </div>
                    <div class="form-group my_form">
                        <label for="email">Name:</label>
                        <input type="text" class="form-control" name="name" required>
                    </div>

                    <div class="form-group my_form">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" name="email" required>
                    </div>

                    <div class="form-group my_form">
                        <label for="email">Phone Number:</label>
                        <input type="text" class="form-control" name="phone">
                    </div>

                    <div class="form-group my_form">
                        <label for="email">Password:</label>
                        <input type="password" class="form-control" name="password" id="password" required>
                    </div>


                    <div class="form-group my_form">
                        <label for="email">Confirm Password:</label>
                        <input type="password" class="form-control" name="password_confirmation"
                            id="password_confirmation" required>
                    </div>

                </div>
                <div data-toggle="modal" style="cursor: pointer" data-target="#loginModals" data-dismiss="modal">
                    <span style="cursor: pointer" class="ml-4 text-danger">I have an account...Sign In</span>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" value="login" name="login"
                        class="btn btn-default">Close</button>
                    <button type="submit" name="submit_user" class="btn btn-danger">Submit</button>
                </div>
            </form>
        </div>

    </div>
</div>

@if(Session::get('user') != null)
<div id="profileModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Profile</h4>
            </div>

            <form method="post" enctype="multipart/form-data" action="{{ url('profile/'.Session::get('user')->id) }}">
                @csrf
                <div class="modal-body">
                    <div class="text-center auth-logo mb-1">
                        <img src="https://www.standardmedia.co.ke/flash/standard_logo.png" style="width:50%"
                            alt="Eve Woman Logo">
                    </div>
                    <div class="form-title text-center">
                        <h4>My Standand Account Profile</h4>
                    </div>
                    <div class="form-group my_form">
                        <label for="email">Name:</label>
                        <input type="text" class="form-control" name="name" required
                            value="{{ Session::get('user')->name }}">
                    </div>

                    <div class="form-group my_form">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" name="email" required
                            value="{{ Session::get('user')->email }}">
                    </div>

                    <div class="form-group my_form">
                        <label for="email">Phone Number:</label>
                        <input type="text" class="form-control" name="phone" value="{{ Session::get('user')->phone }}">
                    </div>
                    <input type="hidden" required class="form-control" name="password" id="password"
                        placeholder="Enter your password..." value="{{Session::get('user')->password}}">
                </div>
                <div data-toggle="modal" style="cursor: pointer" data-target="#passwordModal" data-dismiss="modal">
                    <span style="cursor: pointer" class="ml-4 text-danger">Update Password</span>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" value="login" name="login"
                        class="btn btn-default">Close</button>
                    <button type="submit" name="submit_user" class="btn btn-danger">Update Profile</button>
                </div>
            </form>
        </div>

    </div>
</div>

<div id="passwordModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update Password</h4>
            </div>

            <form method="post" enctype="multipart/form-data" action="{{url('password/'.Session::get('user')->id) }}">
                @csrf
                <input type="hidden" required class="form-control" name="email" id="email"
                    value="{{Session::get('user')->email}}">
                <div class="modal-body">
                    <div class="text-center auth-logo mb-1">
                        <img src="https://www.standardmedia.co.ke/flash/standard_logo.png" style="width:50%"
                            alt="Eve Woman Logo">
                    </div>
                    <div class="form-title text-center">
                        <h4>Update your Standand Account Password</h4>
                    </div>
                    <div class="form-group my_form">
                        <label for="email">Current Password:</label>
                        <input type="password" class="form-control" name="current_password" id="current_password"
                            required>
                    </div>

                    <div class="form-group my_form">
                        <label for="email">New Password:</label>
                        <input type="password" class="form-control" name="password" id="password" required>
                    </div>

                    <div class="form-group my_form">
                        <label for="email">Confirm Password:</label>
                        <input type="password" class="form-control" name="password_confirmation"
                            id="password_confirmation" required>
                    </div>

                </div>
                <div data-toggle="modal" style="cursor: pointer" data-target="#profileModal" data-dismiss="modal">
                    <span style="cursor: pointer" class="ml-4 text-danger">Update Profile</span>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" value="login" name="login"
                        class="btn btn-default">Close</button>
                    <button type="submit" name="submit_user" class="btn btn-danger">Update Password</button>
                </div>
            </form>
        </div>

    </div>
</div>
@endif
<script type="text/javascript">
_atrk_opts = {
    atrk_acct: "XVpWq1Y1Mn20Io",
    domain: "standardmedia.co.ke",
    dynamic: true
};
(function() {
    var as = document.createElement('script');
    as.type = 'text/javascript';
    as.async = true;
    as.src = "https://certify-js.alexametrics.com/atrk.js";
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(as, s);
})();
</script>
<noscript><img src="https://certify.alexametrics.com/atrk.gif?account=XVpWq1Y1Mn20Io" style="display:none" height="1"
        width="1" alt="" /></noscript>
<script src="{{asset('eve/assets/js/lazy.js')}}"></script>
<script>
$(document).ready(function() {
    $("#inpt_search").on('focus', function() {
        $(this).parent('label').addClass('active');
    });

    $("#inpt_search").on('blur', function() {
        if ($(this).val().length == 0)
            $(this).parent('label').removeClass('active');
    });

    $('a[href="#search"]').on('click', function(event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
    });

    $('#search, #search button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });
    $('body').on('click', '.login', function() {
        document.getElementById("mySidenavMob").style.width = "0";
    });

    $('body').on('click', '#load-more', function() {
        var limit = $('#current_limit').val();
        var end = parseInt($('#current_limit').val()) + 6;
        var keywords = $('#keywords').val();
        var categoryid = $('#categoryid').val();
        $.ajax({
            url: "{{url('api/load_more')}}",
            type: "POST",
            data: {
                limit: limit,
                categoryid: categoryid,
                end: end
            },
            beforeSend: function() {
                $('#response').html(
                    "<img src='<?= asset('images/load_more.gif');?>' width='64' height='64' />"
                );
            },
            success: function(response) {
                // you will get response from your php page (what you echo or print)
                $('#current_limit').val(limit);
                $('.displayLoaded').append(response)
                $('#response').html("");
                $('#current_limit').val(end);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert("An error occurred! Please try again later.....");
                $('#response').html("");
            }
        });
    })

    $(".logomob").click(function() {
        window.location.href = "{{url('/')}}";
    });

    $('body').on('click', '#topic-more', function() {
        var limit = $('#current_limit').val();
        var end = parseInt($('#current_limit').val()) + 6;
        var keywords = $('#keywords').val();
        var categoryid = $('#categoryid').val();
        $.ajax({
            url: "{{url('api/topic_load_more')}}",
            type: "POST",
            data: {
                limit: limit,
                keywords: keywords,
                categoryid: categoryid,
                end: end
            },
            beforeSend: function() {
                $('#response').html(
                    "<img src='<?= asset('images/load_more.gif');?>' width='64' height='64' />"
                );
            },
            success: function(response) {
                // you will get response from your php page (what you echo or print)
                $('#current_limit').val(limit);
                $('.displayLoaded').append(response)
                $('#response').html("");
                $('#current_limit').val(end);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert("An error occurred! Please try again later.....");
                $('#response').html("");
            }
        });
    })

    function authorUrl(url) {
        window.location.href = url;
    }

    $('body').on('click', '#author-more', function() {
        var limit = $('#current_limit').val();
        var end = parseInt($('#current_limit').val()) + 6;
        var keywords = $('#keywords').val();
        var categoryid = $('#categoryid').val();
        $.ajax({
            url: "{{url('api/author_load_more')}}",
            type: "POST",
            data: {
                limit: limit,
                keywords: keywords,
                categoryid: categoryid,
                end: end
            },
            beforeSend: function() {
                $('#response').html(
                    "<img src='<?= asset('images/load_more.gif');?>' width='64' height='64' />"
                );
            },
            success: function(response) {
                // you will get response from your php page (what you echo or print)
                $('#current_limit').val(limit);
                $('.displayLoaded').append(response)
                $('#response').html("");
                $('#current_limit').val(end);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert("An error occurred! Please try again later.....");
                $('#response').html("");
            }
        });
    })
});
</script>
<script>
// var owl = $('.owl-carousel');
// owl.owlCarousel({

//     loop: true,
//     margin: 10,
//     nav: true,
//     responsive: {
//         0: {
//             items: 1
//         },
//         600: {
//             items: 2
//         },
//         700: {
//             items: 2
//         },
//         1000: {
//             items: 6
//         }
//     },
//     autoplay: true,
//     autoplayTimeout: 4000,
//     autoplayHoverPause: true
// });
// $('.play').on('click', function() {
//     owl.trigger('play.owl.autoplay', [1000])
// });
// $('.stop').on('click', function() {
//     owl.trigger('stop.owl.autoplay')
// });
jQuery(function($) {
    if ($(window).width > 700) {


        $('.navbar .dropdown').hover(function() {

            $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

        }, function() {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

        });

        $('.navbar .dropdown > a').click(function() {
            location.href = this.href;
        });
    }
});
// var owl = $('.owl-carousels');
// owl.owlCarousel({

//     loop: true,
//     margin: 10,
//     nav: true,
//     responsive: {
//         0: {
//             items: 1
//         },
//         600: {
//             items: 2
//         },
//         700: {
//             items: 3
//         },
//         1000: {
//             items: 6
//         }
//     },
//     autoplay: true,
//     autoplayTimeout: 4000,
//     autoplayHoverPause: true
// });
// $('.play').on('click', function() {
//     owl.trigger('play.owl.autoplay', [1000])
// });
// $('.stop').on('click', function() {
//     owl.trigger('stop.owl.autoplay')
// });
jQuery(function($) {
    if ($(window).width > 700) {


        $('.navbar .dropdown').hover(function() {

            $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

        }, function() {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

        });

        $('.navbar .dropdown > a').click(function() {
            location.href = this.href;
        });
    }
});
// var sc = $(".secondnav").position().top;
// $(window).scroll(function() {
//     var height = $(window).scrollTop();
//     if (height >= sc) {
//         $(".secondnav").addClass("fixed-top");
//     } else {
//         $(".secondnav").removeClass("fixed-top");
//     }
// });
</script>
<script type="text/javascript">
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
};

function openNavMob() {
    document.getElementById("mySidenavMob").style.width = "250px";
}

function closeNavMob() {
    document.getElementById("mySidenavMob").style.width = "0";
}
</script>
<script type="text/javascript">
$(document).ready(function() {
    //hides them logo when the page loads
    $("#myImages").show();
});

$(document).scroll(function() {
    var y = $(document).scrollTop(),
        image = $("#myImages"),
        header = $("#menu");


    if (y >= 100) {
        //show the image and make the header fixed
        header.removeClass('fixed');
        image.hide();
    } else {
        //put the header in original position and hide image
        header.addClass('fixed');
        image.show();
    }
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    //hides them logo when the page loads
    $("#hide").show();
});

$(document).scroll(function() {
    var y = $(document).scrollTop(),
        image = $("#hide"),
        header = $("#menu");


    if (y >= 100) {
        //show the image and make the header fixed
        header.removeClass('fixed');
        image.hide();
    } else {
        //put the header in original position and hide image
        header.addClass('fixed');
        image.show();
    }
});
</script>
<script type="text/javascript">
$(function() {
    $(window).on("scroll", function() {
        if ($(window).scrollTop() > 50) {
            $(".header").addClass("active");
        } else {
            //remove the background property so it comes transparent again (defined in your css)
            $(".header").removeClass("active");
        }
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    //hides them logo when the page loads
    $("#myImagese").show();
});

$(document).scroll(function() {
    var y = $(document).scrollTop(),
        image = $("#myImagese"),
        header = $("#menu");


    if (y >= 100) {
        //show the image and make the header fixed
        header.removeClass('fixed');
        image.hide();
    } else {
        //put the header in original position and hide image
        header.addClass('fixed');
        image.show();
    }
});
$(document).ready(function() {
    //hides them logo when the page loads
    $("#myImage").hide();
});

$(document).scroll(function() {
    var y = $(document).scrollTop(),
        image = $("#myImage"),
        header = $("#menu");


    if (y >= 100) {
        //show the image and make the header fixed
        header.addClass('fixed');
        image.show();
    } else {
        //put the header in original position and hide image
        header.removeClass('fixed');
        image.hide();
    }
});
</script>
</body>

</html>