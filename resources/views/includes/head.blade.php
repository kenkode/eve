<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#ed008c">
    <link rel="canonical" href="{{url()->current()}}">
    <meta name="keywords"
        content="{{ isset($article) ? $article->keywords : 'Eve,Woman,Eve Woman,Living,Relationships,Ladies,Girls,Parenting,Bridal,Fashion,Beauty,Reproducctive Health,Fashion and Beauty,Readers Lounge,Videos'}}" />
    <meta name="author" content="{{ isset($article) ? $article->author : 'Standard Digital' }}" />
    <meta name="description"
        content="{{ isset($article) ? $article->summary : 'For the modern woman who wants it all from relationships, career, recipes, business, health, fashion and beauty.'}}" />
    <link rel="image_src"
        href="{{ isset($article) ? 'https://cdn.standardmedia.co.ke'.$article->thumbURL : asset('eve/assets/images/logo.png')}}">
    <link rel="shortcut icon" href="{{asset('eve/assets/images/logo.png')}}">
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@EveWomanKenya" />
    <meta name="twitter:creator" content="@EveWomanKenya" />
    <meta property="twitter:title" content="{{ isset($article) ? $article->title : 'Eve Woman' }}" />
    <meta property="twitter:description"
        content="{{ isset($article) ? $article->summary : 'For the modern woman who wants it all from relationships, career, recipes, business, health, fashion and beauty.'}}" />
    <meta property="twitter:url" content="https://twitter.com/evewomankenya" />
    <meta property="twitter:image"
        content="{{ isset($article) ? 'https://cdn.standardmedia.co.ke'.$article->thumbURL : asset('eve/assets/images/logo.png')}}" />
    <meta property="og:type" content="website" />
    <meta property="fb:app_id" content="118898458656" />
    <meta property="og:title" content="{{ isset($article) ? $article->title : 'Eve Woman' }}" />
    <meta property="og:description"
        content="{{ isset($article) ? $article->summary : 'For the modern woman who wants it all from relationships, career, recipes, business, health, fashion and beauty.'}}" />
    <meta property="og:site_name" content="Eve Woman" />
    <meta property="og:url" content="{{url()->current()}}">
    <meta property="og:image"
        content="{{ isset($article) ? 'http://cdn.standardmedia.co.ke'.$article->thumbURL : asset('eve/assets/images/logo.png')}}" />
    <meta property="og:image:secure_url"
        content="{{ isset($article) ? 'https://cdn.standardmedia.co.ke'.$article->thumbURL : asset('eve/assets/images/logo.png')}}" />
    <title>
        {{ isset($article) ? $article->title.' - Eve Woman' : 'Eve magazine - Relationships, career, recipes, business, health, fashion and beauty' }}
    </title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,800|Roboto:400,500,700&display=swap"
        rel="stylesheet">
    <link rel="amphtml" href="{{str_replace('/evewoman','/evewoman/amp',url()->current())}}">
    <link rel="dns-prefetch" href="https://www.googletagservices.com/tag/js/gpt.js" as="script">
    <!-- <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css')}}"
        integrity="sha256-UhQQ4fxEeABh4JrcmAJ1+16id/1dnlOEVCFOxDef9Lw=" crossorigin="anonymous"> -->
    <!-- Theme CSS -->
    <!-- <link rel="stylesheet" type="text/css" href="{{asset('eve/css/style2.css')}}" />
    <link type="text/css" href="{{asset('eve/assets/css/theme.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('eve/assets/css/style.css')}}" rel="stylesheet"> -->
    <link type="text/css" href="{{asset('eve/css/eve.min.css')}}" rel="stylesheet">
    <!-- Google Tag Manager -->
    <script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-TNB7V35');
    </script>
    <!-- End Google Tag Manager -->
    <script src='https://www.googletagservices.com/tag/js/gpt.js' type="text/javascript"></script>
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-2204615711705377",
        enable_page_level_ads: true
    });
    </script>
    <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
    <script>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
    </script>
    <script>
    googletag.cmd.push(function() {
        var leaderboard1 = googletag.sizeMapping().addSize([1050, 200], [728, 90]).addSize([970, 250], [970,
                90
            ]).addSize([980, 250], [728, 90]).addSize([980, 90], [468, 60]).addSize([0, 0], [320, 100])
            .addSize([0, 0], [320, 50]).build();
        var squarebanner = googletag.sizeMapping().addSize([320, 100], [336, 280]).addSize([500, 500], [300,
                250
            ]).addSize([300, 600], [300, 250]).addSize([250, 250], [200, 200]).addSize([0, 0], [300,
                250
            ])
            .build();
        var evewomantopadvert = googletag.defineSlot('/24409412/evewoman_top_advert', [
            [728, 90],
            [980, 250],
            [970, 250],
            [980, 90],
            [970, 90],
            [1050, 200],
            [468, 60],
            [320, 100],
            [320, 50]
        ], 'div-gpt-ad-1513749913330-0').defineSizeMapping(leaderboard1).addService(googletag.pubads());
        googletag.defineSlot('/24409412/evewoman_leaderboard_2', [
            [728, 90],
            [980, 250],
            [970, 250],
            [980, 90],
            [970, 90],
            [1050, 200],
            [468, 60],
            [320, 100],
            [320, 50]
        ], 'div-gpt-ad-1592288489505-0').defineSizeMapping(leaderboard1).addService(googletag.pubads());
        var evewomanad1 = googletag.defineSlot('/24409412/Evewoman_rightpanel_advert5', [
            [300, 250],
            [336, 280],
            [250, 250],
            [200, 200],
            [320, 100],
            [300, 600]
        ], 'div-gpt-ad-1512394772255-0').defineSizeMapping(squarebanner).addService(googletag.pubads());
        var evewomanad2 = googletag.defineSlot('/24409412/Evewoman_rightpanel_advert1', [
            [300, 250],
            [336, 280],
            [250, 250],
            [200, 200],
            [320, 100],
            [300, 600]
        ], 'div-gpt-ad-1507642839100-0').defineSizeMapping(squarebanner).addService(googletag.pubads());

        var evewomanad3 = googletag.defineSlot('/24409412/Evewoman_rightpanel_advert2', [
            [300, 250],
            [336, 280],
            [250, 250],
            [200, 200],
            [320, 100],
            [300, 600]
        ], 'div-gpt-ad-1507642883277-0').defineSizeMapping(squarebanner).addService(googletag.pubads());

        googletag.defineSlot('/24409412/evewoman_leaderboard_3', [
            [728, 90],
            [980, 250],
            [970, 250],
            [980, 90],
            [970, 90],
            [1050, 200],
            [468, 60],
            [320, 100],
            [320, 50]
        ], 'div-gpt-ad-1592288750650-0').defineSizeMapping(leaderboard1).addService(googletag.pubads());
        googletag.defineSlot('/24409412/evewoman_leaderboard_4', [
            [728, 90],
            [980, 250],
            [970, 250],
            [980, 90],
            [970, 90],
            [1050, 200],
            [468, 60],
            [320, 100],
            [320, 50]
        ], 'div-gpt-ad-1592288909624-0').defineSizeMapping(leaderboard1).addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.pubads().collapseEmptyDivs();
        googletag.enableServices();
    });
    </script>
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "NewsArticle",
        "mainEntityOfPage": "{{url()->current()}}",
        "headline": "{{ isset($article) ? $article->title : 'Eve Woman' }}",
        "image": {
            "@type": "ImageObject",
            "url": "{{ isset($article) ? 'https://cdn.standardmedia.co.ke'.$article->thumbURL : asset('eve/assets/images/logo.png')}}",
            "height": 500,
            "width": 800
        },
        "datePublished": "{{ isset($article) ? date('D, d M Y H:i:s',strtotime($article->publishdate)) : date('D, d M Y H:i:s') }} EAT",
        "dateModified": "{{ isset($article) ? date('D, d M Y H:i:s',strtotime($article->updateddate)) : date('D, d M Y H:i:s') }} EAT",
        "author": {
            "@type": "Person",
            "name": "{{ isset($article) ? $article->author : 'Standard Digital' }}"
        },
        "publisher": {
            "@type": "Organization",
            "name": "Eve Woman",
            "logo": {
                "@type": "ImageObject",
                "url": "{{asset('eve/assets/images/logo.png')}}",
                "width": 600,
                "height": 60
            }
        },
        "description": "{{ isset($article) ? $article->summary : 'For the modern woman who wants it all from relationships, career, recipes, business, health, fashion and beauty.'}}"
    }
    </script>
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [{
            "@type": "ListItem",
            "position": 1,
            "name": "Home",
            "item": "{{url('/')}}"
        }, {
            "@type": "ListItem",
            "position": 2,
            "name": "{{ isset($category) ? $category->name : 'Eve Woman' }}",
            "item": "{{isset($link) ? $link : url('/')}}"
        }, {
            "@type": "ListItem",
            "position": 3,
            "name": "{{ isset($article) ? $article->title : 'Eve Woman' }}",
            "item": "{{url()->current()}}"
        }]
    }
    </script>

    <style>
    a {
        color: #3a3a3a;
    }

    iframe {
        width: 100%;
    }

    @font-face {
        font-family: Futura Md BT;
        src: url("{{asset('eve/assets/fonts/futura_medium_bt.ttf')}}");
    }

    * {
        font-family: Futura Md BT !important;
    }

    @media only screen and (max-width: 480px) {
        .image_article p {
            font-size: 19px;
            color: #343434;
        }
    }

    @media only screen and (max-width: 480px) {

        h1,
        .h1 {
            font-size: 1.6rem;
        }
    }

    @media only screen and (max-width: 480px) {
        .mt-50 {
            margin: 0;
            padding: 0 10px 5% !important;
        }

        .no-gutters {
            margin-right: 10px;
            margin-left: 0;
        }
    }

    .main {
        position: relative;
        overflow: hidden;
    }

    .navbar-expand-md .navbar-nav .nav-link {
        padding-right: .5rem;
        padding-left: .5rem;
    }

    @media (min-width: 1441px) {
        .navbar-dark .navbar-nav .nav-link {
            font-size: 16px;
            font-weight: 600;
            text-transform: uppercase;
        }

        .border .nav-link {
            border: 1px solid;
            padding: 0px 7px !important;
            height: 30px !important;
        }
    }


    @media (max-width: 1440px) and (min-width: 1240px) {
        .navbar-dark .navbar-nav .nav-link {
            font-size: 14px;
            font-weight: 600;
            text-transform: uppercase;
        }

        .border .nav-link {
            border: 1px solid;
            padding: 0px 7px !important;
            height: 26px !important;
        }
    }

    @media (min-width: 768px) {
        .navbar-expand-md .navbar-nav .nav-link {
            padding-right: .5rem;
            padding-left: .5rem;
        }

    }

    @media (max-width: 768px) {

        .wa {
            width: 20% !important;
        }

    }

    .navbar-expand-md {
        overflow: hidden !important;
    }

    #mySidenav a {
        color: #d2d0d0;
    }

    .navheading a {
        color: #e31818 !important;
        background: #1f1f1f;
    }

    @media (max-width: 1239px) and (min-width: 1000px) {
        .navbar-dark .navbar-nav .nav-link {
            color: #fff;
            font-size: 12px;
            font-weight: 600;
            text-transform: uppercase;
        }

        .logosmall {
            width: 142px;
            margin: -20px 0 0 -25px;
            padding: 7px;
        }
    }

    @media (max-width: 1440px) and (min-width: 1240px) {
        .logo {
            width: 250px;
            padding: 0;
            margin: -30px 0 0;
        }

        .logotop {
            margin-right: 9rem !important;
        }
    }

    .border a {
        /* border: 1px solid black !important; */
        padding: 2px 7px 0 !important;
        height: 25px;
        text-transform: none !important;
        background: white !important;
        border-radius: 0 !important;
    }

    #search .btn {
        position: absolute;
        top: 50%;
        left: 15% !important;
        margin-top: 61px;
        width: 60% !important;
    }

    #search-icon {
        color: #ed008c !important;
    }

    #search {
        position: fixed;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.7);
        z-index: 9999;
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;

        -webkit-transform: translate(0px, -100%) scale(0, 0);
        -moz-transform: translate(0px, -100%) scale(0, 0);
        -o-transform: translate(0px, -100%) scale(0, 0);
        -ms-transform: translate(0px, -100%) scale(0, 0);
        transform: translate(0px, -100%) scale(0, 0);

        opacity: 0;
    }

    #search.open {
        -webkit-transform: translate(0px, 0px) scale(1, 1);
        -moz-transform: translate(0px, 0px) scale(1, 1);
        -o-transform: translate(0px, 0px) scale(1, 1);
        -ms-transform: translate(0px, 0px) scale(1, 1);
        transform: translate(0px, 0px) scale(1, 1);
        opacity: 1;
    }

    #search input[type="search"] {
        position: absolute;
        top: 50%;
        width: 100%;
        color: rgb(255, 255, 255);
        background: rgba(0, 0, 0, 0);
        font-size: 60px;
        font-weight: 300;
        text-align: center;
        border: 0px;
        margin: 0px auto;
        margin-top: -51px;
        padding-left: 30px;
        padding-right: 30px;
        outline: none;
    }

    #search .btn {
        position: absolute;
        background: #ed008c;
        top: 50%;
        left: 37%;
        margin-top: 61px;
        width: 20%;
    }

    #search .close {
        position: fixed;
        top: 15px;
        right: 15px;
        color: #fff;
        background-color: #ed008c;
        border-color: #ed008c;
        opacity: 1;
        padding: 10px 17px;
        font-size: 27px;
    }

    @media (min-width: 1450px) {
        .navbar-expand-md .navbar-collapse {
            display: flex !important;
            flex-basis: auto;
            height: 100px;
            padding: 9px;
        }
    }
    </style>
    <style>
    .name {
        margin-top: 20px !important;
        margin-right: 30px !important;
        margin-left: 30px !important;
    }

    .nav-link a:active,
    .nav-link a:focus {
        color: #ed008c !important;
    }
    </style>
    <style>
    .btn-danger {
        font-size: 20px;
        border-radius: 0px;
        background: #ed008c;
    }

    .modal-content {
        overflow-y: scroll;
        max-height: 500px;
    }

    .page-item.active .page-link {
        z-index: 1;
        color: #fff;
        background-color: #ed008c !important;
        border-color: #ed008c !important;
    }

    a:hover {
        color: #ed008c !important;
    }

    .side_text a {
        color: #000000;
        text-decoration: none;
        font-family: Futura Md BT;
    }

    .text-danger {
        color: #ed008c !important;
    }

    .text-default strong {
        font-weight: 700;
        color: black;
    }

    figcaption {
        display: block;
        background: #efefef;
        padding: 6px;
    }

    @media (max-width: 1500px) and (min-width: 1255px) {
        .header-area .navbar .navbar-nav .nav-item a {
            font-size: 1em !important;
        }

        .marginlogo {
            margin-right: 10rem !important;
        }
    }

    @media (max-width: 1440px) and (min-width: 1240px) {
        .navbar-dark .navbar-nav .nav-link {
            font-size: 14px;
            font-weight: 400 !important;
            text-transform: uppercase;
        }
    }

    .js-cookie-consent {
        background: #272626;
        color: #fff !important;
        width: 100%;
        display: table;
        position: fixed;
        bottom: 0;
        z-index: 9999;
        padding: .75rem 1.25rem;
        margin-bottom: 1rem;
        border: 1px solid transparent;
        border-radius: .25rem;
        left:0;
    }

    .js-cookie-consent-agree {
        display: inline-block;
        font-weight: 400;
        color: #fff !important;
        color: #212529;
        text-align: center;
        vertical-align: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background: #ed008c;
        border: 1px solid transparent;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: .25rem;
        transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    }
    </style>
</head>

<?php 
    $side_menus = App\Eve::getNav();
    $all_menus = App\Eve::getAllNav();
    $nav_menus = App\Eve::getLimitNav();
?>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TNB7V35" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div id="mySidenav" class="sidenav" style="z-index:10000">
        <a href="javascript:void(0)" class="closebtn border-0" onclick="closeNav()" style="font-size:25px">x</a>
        <a class="navheading">Eve Woman</a>
        @foreach($side_menus as $menu)
        <a href="{{url('category/'.$menu->id.'/'.Str::slug($menu->name))}}">{{$menu->name}}</a>
        @endforeach
        <!-- <a class="nav-link" href="{{url('/topic/reproductive-health')}}">Reproductive Health</a> -->
        <!-- <a class="nav-link" href="{{url('videos')}}">Videos</a> -->
        <a class="nav-link" href="{{url('/about-us')}}">About Us</a>
        <a class="navheading">Digital News</a>
        <a href="https://www.standardmedia.co.ke/videos/">Videos</a>
        <a href="https://www.standardmedia.co.ke/category/7/opinion">Opinions</a>
        <a href="https://www.standardmedia.co.ke/category/52/cartoons">Cartoons</a>
        <a href="https://www.standardmedia.co.ke/category/56/education">Education</a>
        <a href="https://www.standardmedia.co.ke/ureport">U-Report</a>
        <a target="_blank"
            href="https://newsstand.standardmedia.co.ke/?utm_source=standard&utm_medium=webiste">E-Paper</a>
        <a class="navheading">Lifestyle & Entertainment</a>
        <a target="_blank" href="https://thenairobian.ke/">Nairobian</a>
        <a target="_blank" href="https://www.sde.ke/">SDE</a>
        <a target="_blank" href="https://www.standardmedia.co.ke/evewoman/?utm_source=standard&utm_medium=webiste">Eve
            Woman</a>
        <a target="_blank" href="https://www.travelog.ke/?utm_source=standard&utm_medium=webiste">Travelog</a>
        <a class="navheading">TV Stations</a>
        <a href="https://www.standardmedia.co.ke/ktn?utm_source=standard&utm_medium=webiste">KTN Home</a>
        <a href="https://www.standardmedia.co.ke/ktnnews?utm_source=standard&utm_medium=webiste">KTN News</a>
        <a href="http://www.btvkenya.ke/?utm_source=standard&utm_medium=webiste">BTV</a>
        <a href="https://www.farmers.co.ke/videolist?utm_source=standard&utm_medium=webiste">KTN Farmers TV</a>
        <a class="navheading">Radio Stations</a>
        <a href="https://www.standardmedia.co.ke/radiomaisha">Radio Maisha</a>
        <a href="https://www.spicefm.co.ke?utm_source=standard&utm_medium=webiste">Spice FM</a>
        <a href="https://www.vybezradio.co.ke?utm_source=standard&utm_medium=webiste">Vybez Radio</a>
        <a class="navheading">Enterprise</a>
        <a target="_blank" href="http://vas.standardmedia.co.ke/?utm_source=standard&utm_medium=webiste">BULK SMS</a>
        <a target="_blank"
            href="https://tutorsoma.standardmedia.co.ke/?utm_source=standard&utm_medium=webiste">E-Learning</a>
        <a target="_blank" href="https://digger.co.ke/?utm_source=standard&utm_medium=webiste">Digger Classified</a>
        <a class="navheading">The Standard Group</a>
        <a href="https://www.standardmedia.co.ke/corporate">Corporate</a>
        <a href="https://www.standardgroup.co.ke/contact-us">Contact Us</a>
        <a href="https://www.standardmedia.co.ke/ratecard/rate_card.pdf">Rate Card</a>
        <a href="https://www.standardmedia.co.ke/recruitment">Vacancies</a>
        <a href="https://portal.standardmedia.co.ke/dcx_sg">DCX</a>
        <a href="https://portal.standardmedia.co.ke/omportal">O.M Portal</a>
        <a href="https://smtp.standardmedia.co.ke/owa">Corporate Email</a>
        <a href="https://rms.standardmedia.co.ke:73/">RMS</a>
    </div>
    <div id="mySidenavMob" class="sidenav sidenavMob" style="z-index:9999">
        <a href="javascript:void(0)" class="closebtn border-0" onclick="closeNavMob()" style="font-size:25px">x</a>
        <ul>
            <li class="nav-item">
                <a class="nav-link navheading sectionborder" onclick="openNav()"><i class="fa fa-bars text-white"></i>
                    MORE </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link d-md-block d-lg-block " href="{{url('/')}}">HOME</a>
            </li>
            <li class="nav-item d-md-block d-lg-block">
                <a class="nav-link search-link" href="#search"><i class="fa fa-search"></i></a>
            </li>
            @foreach($all_menus as $menu)
            <li class="nav-item ">
                <a class="nav-link" style="text-transform:uppercase"
                    href="{{url('category/'.$menu->id.'/'.Str::slug($menu->name))}}">{{$menu->name}}</a>
            </li>
            @endforeach
            <!-- <li class="nav-item ">
                <a class="nav-link" href="{{url('/topic/reproductive-health')}}">REPRODUCTIVE HEALTH</a>
            </li> -->
            <li class="nav-item ">
                <a class="nav-link" href="{{url('videos')}}">VIDEOS</a>
            </li>
            <!-- <li class="nav-item ">
                <a class="nav-link" href="{{url('/living')}}">LIVING</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="{{url('fashion-aand-beauty')}}">FASHION AND BEAUTY</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="{{url('relationships')}}">RELATIONSHIPS</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="{{url('parenting')}}">PARENTING</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="{{url('/bridal')}}">BRIDAL</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="{{url('videos')}}">VIDEOS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/readers-lounge')}}">READERS LOUNGE
                </a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link" href="{{url('/about-us')}}">ABOUT US
                </a>

            </li>
            @if(Session::get('user'))
            <li class="nav-item">
                <a class="nav-link" style="text-transform:uppercase" href="#">
                    {{Session::get('user')->name}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="text-transform:uppercase" data-toggle="modal" data-dismiss="modal"
                    data-target="#profileModal" href="#">Profile</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="text-transform:uppercase" href="#">View Payments</a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                    LOGOUT</a>
            </li>
            @else
            <li class="nav-item login">
                <a data-scroll-nav="0" class="nav-link sectionborder" href="#" data-toggle="modal"
                    data-target="#loginModals">LOGIN</a>
            </li>
            @endif
            <li class="nav-item ctabg ">
                <a class="nav-link" style="text-transform:uppercase" target="_blank"
                    href="https://newsstand.standardmedia.co.ke">E-Paper @Ksh.20 </a>
            </li>
        </ul>
    </div>


    <nav class="navbar navbar-expand-md navbar-dark mt-4 d-none d-md-block d-lg-block"
        style="background: white !important;">


        <div class="collapse navbar-collapse flex-column container" id="navbar" id="myImages">


            <ul class="navbar-nav justify-content-end w-100 bg-secondary px-3 pr-0">

                <li class="nav-item marginlogo mr-5">
                    <a class="nav-link" href="{{url('/')}}">

                        <img src="{{asset('eve/assets/images/logo.png')}}" class="logo d-none d-md-block d-lg-block">
                    </a>

                </li>

                <li class="nav-item icons">
                    <a class="nav-link" href="{{url('/')}}">
                        <ul class="socialmedia-buttons btn-group ml-auto">
                            <a href="https://www.facebook.com/EveWoman/" target="_blank" style="cursor:pointer"
                                class="btn btn-facebook rounded-0 text-white">
                                <svg class="svg-inline--fa fa-facebook fa-w-14" aria-hidden="true" data-prefix="fab"
                                    data-icon="facebook" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 448 512" data-fa-i2svg="">
                                    <path fill="currentColor"
                                        d="M448 56.7v398.5c0 13.7-11.1 24.7-24.7 24.7H309.1V306.5h58.2l8.7-67.6h-67v-43.2c0-19.6 5.4-32.9 33.5-32.9h35.8v-60.5c-6.2-.8-27.4-2.7-52.2-2.7-51.6 0-87 31.5-87 89.4v49.9h-58.4v67.6h58.4V480H24.7C11.1 480 0 468.9 0 455.3V56.7C0 43.1 11.1 32 24.7 32h398.5c13.7 0 24.8 11.1 24.8 24.7z">
                                    </path>
                                </svg><!-- <i class="fab fa-facebook"></i> -->
                            </a>
                            <a href="https://twitter.com/evewomankenya" target="_blank" style="cursor:pointer"
                                class="btn btn-twitter rounded-0 text-white">
                                <svg class="svg-inline--fa fa-twitter fa-w-16 texu-white" aria-hidden="true"
                                    data-prefix="fab" data-icon="twitter" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 512 512" data-fa-i2svg="">
                                    <path fill="currentColor"
                                        d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z">
                                    </path>
                                </svg><!-- <i class="fab fa-twitter texu-white"></i> -->
                            </a>
                            <a href="https://www.instagram.com/evewomankenya/" target="_blank" style="cursor:pointer"
                                class="btn btn-whatsapp rounded-0 text-white">
                                <svg class="svg-inline--fa fa-instagram fa-w-14" aria-hidden="true" data-prefix="fab"
                                    data-icon="instagram" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 448 512" data-fa-i2svg="">
                                    <path fill="currentColor"
                                        d="m373.40625 0h-234.8125c-76.421875 0-138.59375 62.171875-138.59375 138.59375v234.816406c0 76.417969 62.171875 138.589844 138.59375 138.589844h234.816406c76.417969 0 138.589844-62.171875 138.589844-138.589844v-234.816406c0-76.421875-62.171875-138.59375-138.59375-138.59375zm108.578125 373.410156c0 59.867188-48.707031 108.574219-108.578125 108.574219h-234.8125c-59.871094 0-108.578125-48.707031-108.578125-108.574219v-234.816406c0-59.871094 48.707031-108.578125 108.578125-108.578125h234.816406c59.867188 0 108.574219 48.707031 108.574219 108.578125zm0 0" />
                                    <path fill="currentColor"
                                        d="m256 116.003906c-77.195312 0-139.996094 62.800782-139.996094 139.996094s62.800782 139.996094 139.996094 139.996094 139.996094-62.800782 139.996094-139.996094-62.800782-139.996094-139.996094-139.996094zm0 249.976563c-60.640625 0-109.980469-49.335938-109.980469-109.980469 0-60.640625 49.339844-109.980469 109.980469-109.980469 60.644531 0 109.980469 49.339844 109.980469 109.980469 0 60.644531-49.335938 109.980469-109.980469 109.980469zm0 0" />
                                    <path fill="currentColor"
                                        d="m399.34375 66.285156c-22.8125 0-41.367188 18.558594-41.367188 41.367188 0 22.8125 18.554688 41.371094 41.367188 41.371094s41.371094-18.558594 41.371094-41.371094-18.558594-41.367188-41.371094-41.367188zm0 52.71875c-6.257812 0-11.351562-5.09375-11.351562-11.351562 0-6.261719 5.09375-11.351563 11.351562-11.351563 6.261719 0 11.355469 5.089844 11.355469 11.351563 0 6.257812-5.09375 11.351562-11.355469 11.351562zm0 0" />
                                </svg><!-- <i class="fab fa-whatsapp"></i> -->
                            </a>
                            <a href="https://www.youtube.com/channel/UC28IZ7s_u_e--8wRFyaycbA" target="_blank"
                                style="cursor:pointer" class="btn btn-telegram rounded-0 text-white">
                                <svg class="svg-inline--fa fa-youtube fa-w-16" aria-hidden="true" data-prefix="fab"
                                    data-icon="youtube" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 496 512" data-fa-i2svg="">
                                    <path fill="currentColor" d="M443.734,236.557l-112.939-9.813c-49.812-4.334-99.906-4.334-149.717,0L68.14,236.557
	c-38.659,3.187-68.348,35.6-68.139,74.389V412.28c-0.204,38.835,29.557,71.265,68.267,74.389l112.939,9.813
	c24.832,2.133,49.835,3.243,74.859,3.243c25.024,0,50.027-1.067,74.859-3.243l112.939-9.813
	c38.667-3.188,68.359-35.613,68.139-74.411V310.925C512.194,272.098,482.436,239.681,443.734,236.557z" />
                                    <g>
                                        <path style="fill:#FAFAFA;" d="M96.001,438.946c-5.891,0-10.667-4.776-10.667-10.667v-128c0-5.891,4.776-10.667,10.667-10.667
		c5.891,0,10.667,4.776,10.667,10.667v128C106.668,434.171,101.892,438.946,96.001,438.946z" />
                                        <path style="fill:#FAFAFA;" d="M138.668,310.946H53.334c-5.891,0-10.667-4.776-10.667-10.667s4.776-10.667,10.667-10.667h85.333
		c5.891,0,10.667,4.776,10.667,10.667S144.559,310.946,138.668,310.946z" />
                                    </g>
                                    <g>
                                        <path style="fill:#E53935;" d="M96.001,118.946c-3.357,0-6.519-1.581-8.533-4.267l-64-85.333
		c-3.536-4.712-2.582-11.398,2.13-14.934s11.398-2.582,14.934,2.13c0.001,0.001,0.002,0.002,0.003,0.004l55.467,73.941
		l55.467-73.941c3.535-4.713,10.22-5.668,14.933-2.133c4.713,3.535,5.668,10.221,2.133,14.933l0,0l-64,85.333
		C102.52,117.366,99.359,118.946,96.001,118.946z" />
                                        <path style="fill:#E53935;" d="M96.001,204.28c-5.891,0-10.667-4.776-10.667-10.667V108.28c0-5.891,4.776-10.667,10.667-10.667
		c5.891,0,10.667,4.776,10.667,10.667v85.333C106.668,199.504,101.892,204.28,96.001,204.28z" />
                                        <path style="fill:#E53935;" d="M213.334,204.28c-35.346,0-64-28.654-64-64v-21.333c0-35.346,28.654-64,64-64s64,28.654,64,64
		v21.333C277.334,175.626,248.681,204.28,213.334,204.28z M213.334,76.28c-23.564,0-42.667,19.103-42.667,42.667v21.333
		c0,23.564,19.103,42.667,42.667,42.667s42.667-19.103,42.667-42.667v-21.333C256.001,95.382,236.899,76.28,213.334,76.28
		C213.334,76.28,213.334,76.28,213.334,76.28z" />
                                        <path style="fill:#E53935;"
                                            d="M362.668,204.28c-35.346,0-64-28.654-64-64V65.613c0-5.891,4.776-10.667,10.667-10.667
		s10.667,4.776,10.667,10.667v74.667c0,23.564,19.103,42.667,42.667,42.667s42.667-19.103,42.667-42.667V65.613
		c0-5.891,4.776-10.667,10.667-10.667c5.891,0,10.667,4.776,10.667,10.667v74.667C426.668,175.626,398.014,204.28,362.668,204.28z" />
                                        <path style="fill:#E53935;" d="M458.668,204.28c-53.333,0-53.333-56.192-53.333-74.667c0-5.891,4.776-10.667,10.667-10.667
		c5.891,0,10.667,4.776,10.667,10.667c0,44.267,12.8,53.333,32,53.333c5.891,0,10.667,4.776,10.667,10.667
		S464.559,204.28,458.668,204.28z" />
                                    </g>
                                    <g>
                                        <path style="fill:#FAFAFA;" d="M192.001,438.946c-23.564,0-42.667-19.102-42.667-42.667v-53.333
		c0-5.891,4.776-10.667,10.667-10.667c5.891,0,10.667,4.776,10.667,10.667v53.333c0,11.782,9.551,21.333,21.333,21.333
		c11.782,0,21.333-9.551,21.333-21.333v-53.333c0-5.891,4.776-10.667,10.667-10.667c5.891,0,10.667,4.776,10.667,10.667v53.333
		C234.668,419.844,215.565,438.946,192.001,438.946z" />
                                        <path style="fill:#FAFAFA;" d="M266.668,438.946c-5.891,0-10.667-4.776-10.667-10.667v-128c0-5.891,4.776-10.667,10.667-10.667
		c5.891,0,10.667,4.776,10.667,10.667v128C277.334,434.171,272.559,438.946,266.668,438.946z" />
                                        <path style="fill:#FAFAFA;" d="M448.001,438.946h-21.333c-23.564,0-42.667-19.102-42.667-42.667v-21.333
		c0-23.564,19.103-42.667,42.667-42.667c23.564,0,42.667,19.103,42.667,42.667v10.667c0,5.891-4.776,10.667-10.667,10.667h-53.333
		c0,11.782,9.551,21.333,21.333,21.333h21.333c5.891,0,10.667,4.776,10.667,10.667S453.892,438.946,448.001,438.946z
		 M405.334,374.946h42.667c0-11.782-9.551-21.333-21.333-21.333C414.886,353.613,405.334,363.164,405.334,374.946z" />
                                        <path style="fill:#FAFAFA;" d="M309.334,438.946c-29.455,0-53.333-23.878-53.333-53.333s23.878-53.333,53.333-53.333
		s53.333,23.878,53.333,53.333S338.79,438.946,309.334,438.946z M309.334,353.613c-17.673,0-32,14.327-32,32s14.327,32,32,32
		s32-14.327,32-32S327.008,353.613,309.334,353.613z" />
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg><!-- <i class="fab fa-telegram"></i> -->
                            </a>
                        </ul>
                    </a>

                </li>

            </ul>
        </div>
    </nav>
    <nav class="navbar navbar-expand-md navbar-dark sticky-top">
        <!-- One of the primary actions on mobile is to call a business - This displays a phone button on mobile only -->
        <div class="navbar-toggler-right d-flex">
            <img src="{{asset('eve/assets/images/logo.png')}}" class="logomob mr-auto d-block d-md-none d-lg-none">
            <button class="navbar-toggler " type="button" onclick="openNavMob()">
                <i class="fas fa-bars text-white"></i>
            </button>
        </div>



        <div class="collapse navbar-collapse flex-column px90 shads" id="navbar">


            <ul class="navbar-nav justify-content-around w-100 bg-secondary px-3">


                <li class="nav-item" id="myImage">
                    <a class="nav-link" href="{{url('/')}}">

                        <img src="{{asset('eve/assets/images/logo.png')}}" class="logosmall">
                    </a>

                </li>
                <li class="nav-item border border-0">
                    <a class="nav-link" style="cursor:pointer" onclick="openNav()">
                        <i class="fas fa-bars"></i> More
                    </a>

                </li>
                <li class="nav-item">
                    <a class="nav-link search-link" href="#search">
                        <i class="fas fa-search"></i>
                    </a>

                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/')}}">
                        <i class="fas fa-home"></i>
                    </a>
                </li>
                @foreach($nav_menus as $menu)
                <li class="nav-item ">
                    <a class="nav-link"
                        href="{{url('category/'.$menu->id.'/'.Str::slug($menu->name))}}">{{$menu->name}}</a>
                </li>
                @endforeach
                <!-- <li class="nav-item ">
                    <a class="nav-link" href="{{url('/topic/reproductive-health')}}">REPRODUCTIVE HEALTH</a>
                </li> -->

                <!-- <li class="nav-item">
                    <a class="nav-link" href="{{url('/living')}}">
                        LIVING
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/fashion-and-beauty')}}">
                        FASHION AND BEAUTY

                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/relationships')}}">
                        RELATIONSHIPS
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/parenting')}}">
                        PARENTING
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{url('/bridal')}}">BRIDAL</a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/videos')}}">
                        VIDEOS
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/about-us')}}">ABOUT US
                    </a>

                </li>
                @if(Session::get('user'))
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{Session::get('user')->name}}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" data-toggle="modal" data-dismiss="modal" data-target="#profileModal"
                            href="#">Profile</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">View Payments</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                            Logout</a>
                    </div>
                </li>
                @else
                <li class="nav-item login border border-0">
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#loginModals">
                        Login
                    </a>

                </li>
                @endif
                <li class="nav-item border border-0">
                    <a class="nav-link bg-danger" href="https://newsstand.standardmedia.co.ke" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        e-Paper @ Ksh 20
                    </a>

                </li>
            </ul>


        </div>

    </nav>
    @if (Session::has('success'))
    <div class="alert alert-success alert-dismissible mb-5">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ Session::get('success') }}
    </div>
    @endif

    @if (Session::has('error'))

    <div class="alert alert-danger alert-dismissible mb-5">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ Session::get('error') }}
    </div>
    @endif

    @if ( count( $errors ) > 0 )
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="icofont icofont-close-line-circled"></i>
        </button>
        @foreach ($errors->all() as $error)
        {{ $error }}<br>
        @endforeach
    </div><br><br>
    @endif
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    <div id="search">
        <button type="button" class="close">×</button>
        <form id="search-form" method="get" action="{{url('search')}}">
            <input type="search" id="inpt_search" name="q" aria-label="Search" placeholder="type keyword(s) here" />
            <button type="submit" class="btn btn-lg btn-danger">Search</button>
        </form>
    </div>