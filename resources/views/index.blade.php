@extends('layouts.main')
@section('content')

<style>
a:hover {
    cursor: pointer !important;
}

p {
    font-size: 20px;
    color: #444;
}

.mt-50 {
    margin: 0;
    padding: 0 0 5%;
}

.logomob {
    width: 127px;
    padding: 9px 0;
}

.list-group-heading {
    cursor: pointer;
}

.list-group-heading:hover {
    color: #ed008c;
}
</style>
<main class="main">
    <section>
        <!-- <span class="mask alpha-7"></span> -->
        <div class="container mx-auto mt-50 mb-3">
            <div id='div-gpt-ad-1513749913330-0' style='width:100%;text-align:center;'>
                <script>
                googletag.cmd.push(function() {
                    googletag.display('div-gpt-ad-1513749913330-0');
                });
                </script>
            </div>
            <div class="row mb-4 mt-3">
                <div class="col-md-8">
                    <a style="text-decoration:none"
                        href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                        <div class="card bg-dark text-white">
                            <!-- <img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                                onError="this.onerror=null;this.src='{{asset('/images/pic.jpg')}}';"
                                class="card-img img-fluid px500" alt="..."> -->
                            <img src="{{ asset('/images/pic.jpg') }}"
                                data-src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                                class="lazy card-img img-fluid px500" alt="{{$mainArticle->title}}">
                            <div class="card-img-overlay">
                                <div class="list-group-heading"><a style="color:#ed008c !important"
                                        href="{{url('author/' . $mainArticle->author_id . '/' . Str::slug($mainArticle->author))}}">By
                                        {{$mainArticle->author}}
                                        <span class="card-title text-white">{{App\Eve::time_difference($mainArticle->publishdate)}}</span></a></div>
                                <h5 class="card-title text-white">
                                    <a class="text-white" style="text-decoration:none"
                                        href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                        {{$mainArticle->title}}
                                    </a>
                                </h5>
                                <hr class="bg-yellow">
                                <p class="card-text">
                                    <a class="card-text text-white" style="text-decoration:none"
                                        href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                        {{$mainArticle->summary}}
                                    </a>
                                </p>

                            </div>
                    </a>
                </div>
                </a>
            </div>
            <div class="col-md-4">
                <div class="row" style="margin:0 !important">
                    <div class="card">
                        <div class="list-group">
                            @foreach($getSideLatest as $mainArticle)
                            <?php $url = url('author/' . $mainArticle->author_id . '/' . Str::slug($mainArticle->author));?>
                            <div class="list-group-content">
                                <a
                                    href="{{url('author/' . $mainArticle->author_id . '/' . Str::slug($mainArticle->author))}}">
                                    <div class="list-group-heading">
                                        {{$mainArticle->author}}
                                        <span>{{App\Eve::time_difference($mainArticle->publishdate)}}</span></div>
                                </a>
                                <a
                                    href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                    <p class="text-sm">
                                        {{$mainArticle->title}}

                                    </p>
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-deck">
            @foreach ($getLatest as $mainArticle)
            <div class="card mt-4">
                <a style="text-decoration:none"
                    href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                    <!-- <img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                            onError="this.onerror=null;this.src='{{asset('/images/pic.jpg')}}';" class="card-img px500"
                            alt="..."> -->
                    <img src="{{ asset('/images/pic.jpg') }}"
                        data-src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                        class="lazy card-img img-fluid px500" alt="{{$mainArticle->title}}">
                    <div class="card-body px-0 pb-0">
                        <h5 class="card-title mb-1">{{$mainArticle->title}}</h5>
                    </div>
                </a>
                <div class="card-footer text-left px-0 pt-0">
                    <a href="{{url('author/' . $mainArticle->author_id . '/' . Str::slug($mainArticle->author))}}">
                        <div class="list-group-heading">{{$mainArticle->author}} |
                            <span>{{App\Eve::time_difference($mainArticle->publishdate)}}</span></div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
        <!-- /24409412/evewoman_leaderboard_2 -->
        <div id='div-gpt-ad-1592288489505-0' style='width: 100%; text-align:center;'>
            <script>
            googletag.cmd.push(function() {
                googletag.display('div-gpt-ad-1592288489505-0');
            });
            </script>
        </div>
        </div>
        </div>
    </section>
    <section class="px323">
        <div class="container">
            <div class="row">
                @foreach ($getTwoLatest as $mainArticle)
                <div class="card mb-3">
                    <hr class="bg-grey">
                    <div class="row no-gutters">
                        <div class="col-md-6">
                            <div class="card-body">
                                <a
                                    href="{{url('author/' . $mainArticle->author_id . '/' . Str::slug($mainArticle->author))}}">
                                    <div class="list-group-heading mb-2">{{$mainArticle->author}} |
                                        <span>{{App\Eve::time_difference($mainArticle->publishdate)}}</span></div>
                                </a>
                                <a style="text-decoration:none"
                                    href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                    <h5 class="card-title">
                                        {{$mainArticle->title}}
                                    </h5>
                                    <p class="card-text">{{$mainArticle->summary}}</p>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <a style="text-decoration:none"
                                href="{{url(App\Eve::getCatName($mainArticle->categoryid).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                <!-- <img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                                    onError="this.onerror=null;this.src='{{asset('/images/pic.jpg')}}';"
                                    class="card-img px500" alt="..."> -->
                                <img src="{{ asset('/images/pic.jpg') }}"
                                    data-src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                                    class="lazy card-img img-fluid px500" alt="{{$mainArticle->title}}">
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <!-- /24409412/evewoman_leaderboard_3 -->
            <div id='div-gpt-ad-1592288750650-0' style='width: 100%; text-align:center;'>
                <script>
                googletag.cmd.push(function() {
                    googletag.display('div-gpt-ad-1592288750650-0');
                });
                </script>
            </div>
        </div>
    </section>
    @if($homeVideo != null)
    <section class="bg-lights">
        <div class="px90">
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-7">
                        <div class="embed-responsive embed-responsive-21by9">
                            <iframe class="embed-responsive-item"
                                src="https://www.youtube.com/embed/<?= $homeVideo->videoURL.'?autoplay=1'; ?>"
                                allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="card-body text-center">
                            <div class="center">
                                <h1 class="text-dark">
                                    {{$homeVideo->title}}</h1>

                                <div class="text-center">
                                    <div class="list-group-heading mb-2">{{$homeVideo->author}} |
                                        <span>{{App\Eve::time_difference($homeVideo->publishdate)}}</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    @endif
    <section class="px323">
        <div class="container">
            <div class="row">
                @foreach($getMoreTwoLatest as $mainArticle)
                <div class="card mb-3">
                    <hr class="bg-grey">
                    <div class="row no-gutters">
                        <div class="col-md-6">
                            <div class="card-body">
                                <a
                                    href="{{url('author/' . $mainArticle->author_id . '/' . Str::slug($mainArticle->author))}}">
                                    <div class="list-group-heading mb-2">{{$mainArticle->author}} |
                                        <span>{{App\Eve::time_difference($mainArticle->publishdate)}}</span></div>
                                </a>
                                <a style="text-decoration:none"
                                    href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                    <h5 class="card-title">
                                        {{$mainArticle->title}}
                                    </h5>
                                    <p class="card-text">{{$mainArticle->summary}}</p>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <a style="text-decoration:none"
                                href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                <!-- <img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                                    onError="this.onerror=null;this.src='{{asset('/images/pic.jpg')}}';"
                                    class="card-img px500" alt="..."> -->
                                <img src="{{ asset('/images/pic.jpg') }}"
                                    data-src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                                    class="lazy card-img img-fluid px500" alt="{{$mainArticle->title}}">
                            </a>
                        </div>
                    </div>

                </div>
                @endforeach
            </div>
            <!-- /24409412/evewoman_leaderboard_4 -->
            <div id='div-gpt-ad-1592288909624-0' style='width: 100%; text-align:center;'>
                <script>
                googletag.cmd.push(function() {
                    googletag.display('div-gpt-ad-1592288909624-0');
                });
                </script>
            </div>
        </div>
    </section>
    <section class="bgrey">
        <div class="container">
            <h2 class="text-center ">Fashion And Beauty</h2>
            <div class="card-deck">
                @foreach($getFashionAndBeauty as $mainArticle)
                <div class="card mt-4">
                    <a style="text-decoration:none"
                        href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                        <!-- <img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                            onError="this.onerror=null;this.src='{{asset('/images/pic.jpg')}}';" class="card-img-top"
                            alt="..."> -->

                        <img src="{{ asset('/images/pic.jpg') }}"
                            data-src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                            class="lazy card-img img-fluid px500" alt="{{$mainArticle->title}}">
                    </a>
                    <div class="card-body px-0 pb-0">
                        <a href="{{url('author/' . $mainArticle->author_id . '/' . Str::slug($mainArticle->author))}}">
                            <div class="list-group-heading mb-2"> {{$mainArticle->author}} |
                                <span>{{App\Eve::time_difference($mainArticle->publishdate)}}</span></div>
                        </a>
                        <a style="text-decoration:none"
                            href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                            <h5 class="card-title mb-1">{{$mainArticle->title}}</h5>
                        </a>
                    </div>
                    <div class="card-footer text-left px-0 pt-0">
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="px323">
        <div class="container">
            <div class="row">
                @foreach($getMoreBigLatest as $mainArticle)
                <div class="card mb-3">
                    <hr class="bg-grey">
                    <div class="row no-gutters">
                        <div class="col-md-6">
                            <div class="card-body">
                                <a
                                    href="{{url('author/' . $mainArticle->author_id . '/' . Str::slug($mainArticle->author))}}">
                                    <div class="list-group-heading mb-2">
                                        {{$mainArticle->author}} |
                                        <span>{{App\Eve::time_difference($mainArticle->publishdate)}}</span></div>
                                </a>
                                <a style="text-decoration:none"
                                    href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                    <h5 class="card-title">
                                        {{$mainArticle->title}}
                                    </h5>
                                    <p class="card-text">{{$mainArticle->summary}}</p>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <a style="text-decoration:none"
                                href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                <!-- <img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                                    onError="this.onerror=null;this.src='{{asset('/images/pic.jpg')}}';"
                                    class="card-img" alt="..."> -->
                                <img src="{{ asset('/images/pic.jpg') }}"
                                    data-src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                                    class="lazy card-img img-fluid px500" alt="{{$mainArticle->title}}">
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
    </section>
    <section class="decks">
        <div class="container">
            <h2 class="text-center mb-4 pink">Relationships</h2>
            <div class="card-deck">
                @foreach($getRelationshipArticle as $mainArticle)
                <div class="card">
                    <a style="text-decoration:none"
                        href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                        <!-- <img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                            onError="this.onerror=null;this.src='{{asset('/images/pic.jpg')}}';" class="card-img-top"
                            alt="..."> -->
                        <img src="{{ asset('/images/pic.jpg') }}"
                            data-src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                            class="lazy card-img img-fluid px500" alt="{{$mainArticle->title}}">
                    </a>
                    <div class="card-body">
                        <a style="text-decoration:none"
                            href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                            <h5 class="card-title">{{$mainArticle->title}}
                            </h5>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <!-- <section>
        <div class="container mx-auto mt-50">
            <div class="row mb-4">
                <div class="col-md-8">
                    <div class="card bg-dark text-white">
                        <img src="https://cdn.standardmedia.co.ke/evemedia/eveimages/saturday/the_pain_of_miscarri5ed209d4db267.jpg"
                            class="card-img px500" alt="...">
                        <div class="card-img-overlay">

                            <div class="list-group-heading"> By Mirror <span>10:05 PM</span></div>
                            <h5 class="card-title text-white">Chero Orywa: l lived and travelled for 18 years in the USA
                            </h5>
                            <hr class="bg-yellow">
                            <p class="card-text">Drinking beer could improve your concentration and reduce risk of
                                dementia, study claims
                            </p>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="text-center">
                        <img src="https://tpc.googlesyndication.com/simgad/5668565607239405244" class="ads">
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="bg-lights">
        <div class="px90">

            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-7">
                        <img src="https://cdn.standardmedia.co.ke/evemedia/eveimages/thursday/what_you_need_to_kno5e609212eb911.jpg"
                            class="card-img" alt="...">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body text-center backg">
                            <div class="center">
                                <h1>
                                    Meet the chef who believes it’s evil to waste food
                                    Carlos Espindola </h1>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section> -->
    <section class="px323">
        <div class="container">
            <div class="row">
                @foreach ($getMore2BigLatest as $mainArticle)
                <div class="card mb-3">
                    <div class="row no-gutters">
                        <div class="col-md-6">
                            <div class="card-body">
                                <a
                                    href="{{url('author/' . $mainArticle->author_id . '/' . Str::slug($mainArticle->author))}}">
                                    <div class="list-group-heading mb-2">{{$mainArticle->author}} |
                                        <span>{{App\Eve::time_difference($mainArticle->publishdate)}}</span>
                                    </div>
                                </a>
                                <a style="text-decoration:none"
                                    href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                    <h5 class="card-title">{{$mainArticle->title}}
                                    </h5>
                                    <p class="card-text">{{$mainArticle->summary}}</p>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <a style="text-decoration:none"
                                href="{{url(Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                <!-- <img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                                    onError="this.onerror=null;this.src='{{asset('/images/pic.jpg')}}';"
                                    class="card-img" alt="..."> -->
                                <img src="{{ asset('/images/pic.jpg') }}"
                                    data-src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                                    class="lazy card-img img-fluid px500" alt="{{$mainArticle->title}}">
                            </a>
                        </div>
                    </div>

                    <hr class="bg-grey">
                </div>
                @endforeach
            </div>

        </div>
    </section>
</main>
@stop