@extends('layouts.main')
@section('content')

<div class="container py-3 main-cont">
    <div id='div-gpt-ad-1513749913330-0' style='width:100%; text-align:center;'>
        <script>
        googletag.cmd.push(function() {
            googletag.display('div-gpt-ad-1513749913330-0');
        });
        </script>
    </div>
    <style>
    .image_article img {
        width: 100%;
    }

    .image_article li {
        font-size: 20px;
    }

    .image_article p {
        font-size: 20px !important;
    }
    </style>
    <div class="row mt-3">
        <div class="col-md-8 image_article">
            <h1>
                <strong>
                    <?php echo $article->title; ?>
                </strong>
            </h1>
            <?php echo App\Eve::getCatName($article->categoryid); ?> By
            <span class="bycolorpic">
                <a href="{{url('author/' . $article->author_id . '/' . Str::slug($article->author))}}">
                    {{$article->author}}
                </a>
                <div class="row mb-3 ml-3 mt-3">
                    <div class="btn-group float-right" style="float:right !important">
                        <a style="margin-right:30px"
                            href="https://www.facebook.com/sharer.php?u={{url(Str::slug(App\Eve::getCatName($article->categoryid)).'/article/'.$article->id.'/'.Str::slug($article->title))}}">
                            <img src="https://img.icons8.com/color/50/000000/facebook-circled.png"
                                style="width:100% !important" />
                        </a>
                        <a style="margin-right:30px"
                            href="https://twitter.com/share?url={{url(Str::slug(App\Eve::getCatName($article->categoryid)).'/article/'.$article->id.'/'.Str::slug($article->title))}}">
                            <img src="https://img.icons8.com/color/50/000000/twitter-circled.png"
                                style="width:100% !important" />
                        </a>
                        <a style="margin-right:30px"
                            href="https://telegram.me/share/url?url={{url(Str::slug(App\Eve::getCatName($article->categoryid)).'/article/'.$article->id.'/'.Str::slug($article->title))}}">
                            <img src="https://img.icons8.com/fluent/48/000000/telegram-app.png"
                                style="width:100% !important" />
                        </a>
                        @if((new \Jenssegers\Agent\Agent())->isMobile())
                        <a href="https://api.whatsapp.com/send?text={{ url()->current() }}">
                            <img src="{{asset('images/whatsapp.png')}}" class="wa" style="width:100% !important" />
                        </a>
                        @else
                        <a href="https://web.whatsapp.com/send?text={{ url()->current() }}">
                            <img src="{{asset('images/whatsapp.png')}}" style="width:100% !important" />
                        </a>
                        @endif
                        <!-- <a
                            href="https://web.whatsapp.com/send?text={{url(Str::slug(App\Eve::getCatName($article->categoryid)).'/article/'.$article->id.'/'.Str::slug($article->title))}}">
                            <img src="{{asset('images/whatsapp.png')}}" style="width:10% !important" />
                        </a> -->
                    </div>
                </div>
                <?php
                $content_table = explode("</p>", str_replace("/evemedia/","https://cdn.standardmedia.co.ke/evemedia/",$article->story));
                if (count($content_table) <= 1) {
                    $content_table = explode("<br />", str_replace("/evemedia/","https://cdn.standardmedia.co.ke/evemedia/",$article->story));
                }
                $x = count($content_table);
                $x = (int)$x;
                $y = 0;
                $z = 0;
                foreach ($content_table as $value) {
                echo $value;
                if (($y % 7 == 0) && ($z <= 2)) {
                    if (is_array($related_news) && (isset($related_news[$z]))) {?>
                <p class="text text-default" style="color:rgb(246,47,94);">
                    <strong>ALSO READ: </strong>
                    <?php $value = $related_news[$z];?>
                    <a class="alert-link text-danger"
                        href="{{url(Str::slug(App\Eve::getCatName($value->categoryid)).'/article/' . $value->id . '/' . Str::slug($value->title))}}">
                        {{$value->title}} </a>
                    <?php $z++;
                    }
                }

                if($y == 1){
                    if((new \Jenssegers\Agent\Agent())->isMobile()){?>
                    <div id='div-gpt-ad-1513749913330-0' style='width:100%; text-align:center;'>
                        <script>
                        googletag.cmd.push(function() {
                            googletag.display('div-gpt-ad-1513749913330-0');
                        });
                        </script>
                    </div>
                    <?php }}
                if($y == 6){
                    if((new \Jenssegers\Agent\Agent())->isMobile()){?>
                    <div id='div-gpt-ad-1512394772255-0' style='width:100%; text-align:center;'>
                        <script>
                        googletag.cmd.push(function() {
                            googletag.display('div-gpt-ad-1512394772255-0');
                        });
                        </script>
                    </div>
                    <?php }}

                if ($y == 2) {
                    //$this->view("ads/desktop_article_center_300x250");
                }


                $y++;
            }
            /*echo $article->story;*/ ?>
                    <div class="row ml-3" style="clear:both">
                        <h4 class="mr-3">Share this article</h4>
                        <div class="btn-group float-right" style="float:right !important">
                            <a style="margin-right:30px"
                                href="https://www.facebook.com/sharer.php?u={{url(Str::slug(App\Eve::getCatName($article->categoryid)).'/article/'.$article->id.'/'.Str::slug($article->title))}}">
                                <img src="https://img.icons8.com/color/50/000000/facebook-circled.png"
                                    style="width:100% !important" />
                            </a>
                            <a style="margin-right:30px"
                                href="https://twitter.com/share?url={{url(Str::slug(App\Eve::getCatName($article->categoryid)).'/article/'.$article->id.'/'.Str::slug($article->title))}}">
                                <img src="https://img.icons8.com/color/50/000000/twitter-circled.png"
                                    style="width:100% !important" />
                            </a>

                            <a style="margin-right:30px"
                                href="https://telegram.me/share/url?url={{url(Str::slug(App\Eve::getCatName($article->categoryid)).'/article/'.$article->id.'/'.Str::slug($article->title))}}">
                                <img src="https://img.icons8.com/fluent/48/000000/telegram-app.png"
                                    style="width:100% !important" />
                            </a>
                            @if((new \Jenssegers\Agent\Agent())->isMobile())
                            <a href="https://api.whatsapp.com/send?text={{ url()->current() }}">
                                <img src="{{asset('images/whatsapp.png')}}" class="wa" style="width:100% !important" />
                            </a>
                            @else
                            <a href="https://web.whatsapp.com/send?text={{ url()->current() }}">
                                <img src="{{asset('images/whatsapp.png')}}" style="width:100% !important" />
                            </a>
                            @endif
                        </div>
                    </div>
                    <div class="social mt-3" style="background: #ddd;">
                        <strong>
                            Register to advertise your products & services on our classifieds website
                            <a href="https://www.digger.co.ke/">Digger.co.ke</a> and enjoy one
                            month subscription free of charge and 3 free ads on the Standard newspaper.
                        </strong>

                    </div>
                    <div class="mt-2 mb-2">
                        <!-- Google Ads -->
                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <ins class="adsbygoogle" style="display:block" data-ad-format="autorelaxed"
                            data-ad-client="ca-pub-2204615711705377" data-ad-slot="7206855923"></ins>
                        <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                        <!-- Google Ads End -->
                    </div>

        </div>

        <div class="col-md-4 mt-3">

            <style>
            .side_text a {
                color: #000000;
                text-decoration: none;
                font-family: Futura Md BT;
            }
            </style>
            <div id='div-gpt-ad-1507642839100-0' style='width: 100%; text-align:center;'>
                <script>
                googletag.cmd.push(function() {
                    googletag.display('div-gpt-ad-1507642839100-0');
                });
                </script>
            </div>​
            <?php 
            if(count($popular) > 0){?>
                <h1 class="text-danger mt-4"><strong>Top Stories</strong></h1>
            <?php foreach ($popular as $art) : 
                $articleID = explode('/',$art->url)[3] == "article" ? explode('/',$art->url)[4] : explode('/',$art->url)[3];
                $value = App\Eve::get_single_article($articleID);
                // echo dd($articleID, $value);
                ?>

            <p class="side_text">
                <a href="{{url(Str::slug(App\Eve::getCatName($value->categoryid)).'/article/' . $value->id . '/' . Str::slug($value->title))}}"
                    style="font-size: 18px" class="sidetitles">
                    <?php echo $value->title; ?>
                </a>
                <br />
                <small>
                    <a href="{{url('category/' . $value->categoryid . '/' . Str::slug(App\Eve::getCatName($value->categoryid))); ?>"
                        style="color: #a9a3a3"><?php echo App\Eve::getCatName($value->categoryid); ?>
                        By
                        <a href="{{url('author/' . $value->author_id . '/' . Str::slug($value->author))}}">
                        {{$value->author}} | {{App\Eve::time_difference($value->publishdate)}}
                    </a>
                    </a>
                </small>
            </p>
            <hr />
            <?php endforeach; ?>
            <div id='div-gpt-ad-1507642883277-0' style='width: 100%; text-align:center;'>
                <script>
                googletag.cmd.push(function() {
                    googletag.display('div-gpt-ad-1507642883277-0');
                });
                </script>
            </div>​

            <?php } ?>
           
            <a href=" https://chat.whatsapp.com/EHrRa5PINh67lQqv3BuCFT" target="_blank">
                <img src="{{asset('/assets/img/whatsapp2.png')}}" class="mt-3 img-fluid"
                    alt="">
            </a>

            <h1 class="text-danger mt-3"><strong>{{count($popular) > 0 ? 'Latest Stories':'Top Stories'}}</strong></h1>

            <?php foreach ($getArticleLatest as $articleLatest) :
                if ($article->id == $articleLatest->id) {
                    continue;
                }
                ?>
            <p class="side_text">
                <a href="{{url(Str::slug(App\Eve::getCatName($articleLatest->categoryid)).'/article/' . $articleLatest->id . '/' . Str::slug($articleLatest->title))}}"
                    style="font-size: 18px" class="sidetitles">
                    <?php echo $articleLatest->title; ?>
                </a>
                <br />
                <small>
                    <a
                        href="{{url('category/' . $articleLatest->categoryid . '/' . Str::slug(App\Eve::getCatName($articleLatest->categoryid))); ?>"
                        style="color: #a9a3a3"><?php echo App\Eve::getCatName($articleLatest->categoryid); ?>
                        By
                        <a href="{{url('author/' . $articleLatest->author_id . '/' . Str::slug($articleLatest->author))}}">
                        {{$articleLatest->author}}
                    </a>
                    </a>
                </small>
            </p>
            <hr />
            <?php endforeach; ?>
            @if((new \Jenssegers\Agent\Agent())->isDesktop())
            <div id='div-gpt-ad-1512394772255-0' style='width: 100%; text-align:center;'>
                <script>
                googletag.cmd.push(function() {
                    googletag.display('div-gpt-ad-1512394772255-0');
                });
                </script>
            </div>
            @endif
        </div>
    </div>
</div>

<section class=" backgroundgrey">
    <div class="container">
        <h3 class="titletwo"> RELATED STORIES</h3>
        <div class="row">
            <?php foreach ($related_news as $articles) :
                ?>
            <div class="col-md-3">
                <div class="card border-0">
                    <a
                        href="{{url(Str::slug(App\Eve::getCatName($articles->categoryid)).'/article/' . $articles->id . '/' . Str::slug($articles->title))}}">
                        <!-- <img src="{{'https://cdn.standardmedia.co.ke'.$articles->thumbURL}}"
                            class="card-img-top img-fluid"
                            onError="this.onerror=null;this.src='{{asset('/images/pic.jpg')}}';" alt="..."> -->
                        <img src="{{ asset('/images/pic.jpg') }}"
                            data-src="{{'https://cdn.standardmedia.co.ke'.$articles->thumbURL}}"
                            class="lazy card-img-top img-fluid" alt="{{$articles->title}}">
                    </a>

                    <span class="byline pt-2" style="color: #F62F5E!important;">
                        <a href="{{url('category/' . $articles->categoryid . '/' . Str::slug(App\Eve::getCatName($articles->categoryid))); ?>" style="color: #F62F5E">
                            <?php echo App\Eve::getCatName($articles->categoryid); ?>
                        </a>
                        by
                        <span class="bycolor" style="color: #F62F5E">
                        <a href="{{url('author/' . $articles->author_id . '/' . Str::slug($articles->author))}}">
                            <?php echo $articles->author ?>
                        </a>
                    </span>
                    </span>

                    <a
                        href="{{url(Str::slug(App\Eve::getCatName($articles->categoryid)).'/article/' . $articles->id . '/' . Str::slug($articles->title))}}">
                        <p class="py-3">
                            <?php echo $articles->title ?>
                        </p>
                    </a>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
@stop