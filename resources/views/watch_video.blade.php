@extends('layouts.main')
@section('content')
<style>
@media (min-width: 1200px) {

    .main-cont {
        margin-top: 140px
    }
}

@media (max-width: 1200px) {

    .main-cont {
        margin-top: 100px
    }
}

a {
    color: #3a3a3a !important;
}
</style>

<div class="container">
    <div id='div-gpt-ad-1513749913330-0' style='width:100%; text-align:center;'>
        <script>
        googletag.cmd.push(function() {
            googletag.display('div-gpt-ad-1513749913330-0');
        });
        </script>
    </div>

    <style>
    .image_article img {
        width: 100%;
    }

    .social {
        margin-bottom: 467px !important;
    }
    </style>
    <div class="row">
        <div class="col-md-8 image_article">
            <h1>
                <strong>
                    <?php echo $video->title; ?>
                </strong>
            </h1>
            <div class="row">
                <div class="headertitle">
                    <ul class="navbar-nav justify-content-end w-100 px-3 pr-0">
                        <li class="nav-item icons">
                            <a class="nav-link" href="" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <ul class="socialmedia-buttons btn-group ml-auto">
                                    <a href="https://www.facebook.com/sharer.php?u={{url('videos/watch/'.$video->id.'/'.Str::slug($video->title))}}"
                                        class="btn btn-facebook rounded-0 text-white">
                                        <svg class="svg-inline--fa fa-facebook fa-w-14" aria-hidden="true"
                                            data-prefix="fab" data-icon="facebook" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                            <path fill="currentColor"
                                                d="M448 56.7v398.5c0 13.7-11.1 24.7-24.7 24.7H309.1V306.5h58.2l8.7-67.6h-67v-43.2c0-19.6 5.4-32.9 33.5-32.9h35.8v-60.5c-6.2-.8-27.4-2.7-52.2-2.7-51.6 0-87 31.5-87 89.4v49.9h-58.4v67.6h58.4V480H24.7C11.1 480 0 468.9 0 455.3V56.7C0 43.1 11.1 32 24.7 32h398.5c13.7 0 24.8 11.1 24.8 24.7z">
                                            </path>
                                        </svg><!-- <i class="fab fa-facebook"></i> -->
                                    </a>
                                    <a href="https://twitter.com/share?url={{url('videos/watch/'.$video->id.'/'.Str::slug($video->title))}}"
                                        class="btn btn-twitter rounded-0 text-white">
                                        <svg class="svg-inline--fa fa-twitter fa-w-16 texu-white" aria-hidden="true"
                                            data-prefix="fab" data-icon="twitter" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                            <path fill="currentColor"
                                                d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z">
                                            </path>
                                        </svg><!-- <i class="fab fa-twitter texu-white"></i> -->
                                    </a>
                                    <a href="https://web.whatsapp.com/send?text={{url('videos/watch/'.$video->id.'/'.Str::slug($video->title))}}"
                                        class="btn btn-whatsapp rounded-0 text-white">
                                        <svg class="svg-inline--fa fa-whatsapp fa-w-14" aria-hidden="true"
                                            data-prefix="fab" data-icon="whatsapp" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                            <path fill="currentColor"
                                                d="M380.9 97.1C339 55.1 283.2 32 223.9 32c-122.4 0-222 99.6-222 222 0 39.1 10.2 77.3 29.6 111L0 480l117.7-30.9c32.4 17.7 68.9 27 106.1 27h.1c122.3 0 224.1-99.6 224.1-222 0-59.3-25.2-115-67.1-157zm-157 341.6c-33.2 0-65.7-8.9-94-25.7l-6.7-4-69.8 18.3L72 359.2l-4.4-7c-18.5-29.4-28.2-63.3-28.2-98.2 0-101.7 82.8-184.5 184.6-184.5 49.3 0 95.6 19.2 130.4 54.1 34.8 34.9 56.2 81.2 56.1 130.5 0 101.8-84.9 184.6-186.6 184.6zm101.2-138.2c-5.5-2.8-32.8-16.2-37.9-18-5.1-1.9-8.8-2.8-12.5 2.8-3.7 5.6-14.3 18-17.6 21.8-3.2 3.7-6.5 4.2-12 1.4-32.6-16.3-54-29.1-75.5-66-5.7-9.8 5.7-9.1 16.3-30.3 1.8-3.7.9-6.9-.5-9.7-1.4-2.8-12.5-30.1-17.1-41.2-4.5-10.8-9.1-9.3-12.5-9.5-3.2-.2-6.9-.2-10.6-.2-3.7 0-9.7 1.4-14.8 6.9-5.1 5.6-19.4 19-19.4 46.3 0 27.3 19.9 53.7 22.6 57.4 2.8 3.7 39.1 59.7 94.8 83.8 35.2 15.2 49 16.5 66.6 13.9 10.7-1.6 32.8-13.4 37.4-26.4 4.6-13 4.6-24.1 3.2-26.4-1.3-2.5-5-3.9-10.5-6.6z">
                                            </path>
                                        </svg><!-- <i class="fab fa-whatsapp"></i> -->
                                    </a>
                                    <a href="https://telegram.me/share/url?url={{url('videos/watch/'.$video->id.'/'.Str::slug($video->title))}}"
                                        class="btn btn-telegram rounded-0 text-white">
                                        <svg class="svg-inline--fa fa-youtube fa-w-16" aria-hidden="true"
                                            data-prefix="fab" data-icon="youtube" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" data-fa-i2svg="">
                                            <path fill="currentColor"
                                                d="M248 8C111 8 0 119 0 256s111 248 248 248 248-111 248-248S385 8 248 8zm121.8 169.9l-40.7 191.8c-3 13.6-11.1 16.9-22.4 10.5l-62-45.7-29.9 28.8c-3.3 3.3-6.1 6.1-12.5 6.1l4.4-63.1 114.9-103.8c5-4.4-1.1-6.9-7.7-2.5l-142 89.4-61.2-19.1c-13.3-4.2-13.6-13.3 2.8-19.7l239.1-92.2c11.1-4 20.8 2.7 17.2 19.5z">
                                            </path>
                                        </svg><!-- <i class="fab fa-telegram"></i> -->
                                    </a>
                                </ul>
                            </a>
                        </li>
                    </ul>
                    <!-- Sharingbutton Facebook -->
                    <!-- <a class="resp-sharing-button__link ml-4"
                        href="https://facebook.com/sharer/sharer.php?u=http%3A%2F%2Fsharingbuttons.io" target="_blank"
                        rel="noopener" aria-label="">
                        <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--small">
                            <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                    <path
                                        d="M18.77 7.46H14.5v-1.9c0-.9.6-1.1 1-1.1h3V.5h-4.33C10.24.5 9.5 3.44 9.5 5.32v2.15h-3v4h3v12h5v-12h3.85l.42-4z" />
                                </svg>
                            </div>
                        </div>
                    </a> -->

                    <!-- Sharingbutton Twitter -->
                    <!-- <a class="resp-sharing-button__link"
                        href="https://twitter.com/intent/tweet/?text=Super%20fast%20and%20easy%20Social%20Media%20Sharing%20Buttons.%20No%20JavaScript.%20No%20tracking.&amp;url=http%3A%2F%2Fsharingbuttons.io"
                        target="_blank" rel="noopener" aria-label="">
                        <div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--small">
                            <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                    <path
                                        d="M23.44 4.83c-.8.37-1.5.38-2.22.02.93-.56.98-.96 1.32-2.02-.88.52-1.86.9-2.9 1.1-.82-.88-2-1.43-3.3-1.43-2.5 0-4.55 2.04-4.55 4.54 0 .36.03.7.1 1.04-3.77-.2-7.12-2-9.36-4.75-.4.67-.6 1.45-.6 2.3 0 1.56.8 2.95 2 3.77-.74-.03-1.44-.23-2.05-.57v.06c0 2.2 1.56 4.03 3.64 4.44-.67.2-1.37.2-2.06.08.58 1.8 2.26 3.12 4.25 3.16C5.78 18.1 3.37 18.74 1 18.46c2 1.3 4.4 2.04 6.97 2.04 8.35 0 12.92-6.92 12.92-12.93 0-.2 0-.4-.02-.6.9-.63 1.96-1.22 2.56-2.14z" />
                                </svg>
                            </div>
                        </div>
                    </a> -->



                    <!-- Sharingbutton E-Mail -->
                    <!-- <a class="resp-sharing-button__link"
                        href="mailto:?subject=Super%20fast%20and%20easy%20Social%20Media%20Sharing%20Buttons.%20No%20JavaScript.%20No%20tracking.&amp;body=http%3A%2F%2Fsharingbuttons.io"
                        target="_self" rel="noopener" aria-label="">
                        <div class="resp-sharing-button resp-sharing-button--email resp-sharing-button--small">
                            <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                    <path
                                        d="M22 4H2C.9 4 0 4.9 0 6v12c0 1.1.9 2 2 2h20c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zM7.25 14.43l-3.5 2c-.08.05-.17.07-.25.07-.17 0-.34-.1-.43-.25-.14-.24-.06-.55.18-.68l3.5-2c.24-.14.55-.06.68.18.14.24.06.55-.18.68zm4.75.07c-.1 0-.2-.03-.27-.08l-8.5-5.5c-.23-.15-.3-.46-.15-.7.15-.22.46-.3.7-.14L12 13.4l8.23-5.32c.23-.15.54-.08.7.15.14.23.07.54-.16.7l-8.5 5.5c-.08.04-.17.07-.27.07zm8.93 1.75c-.1.16-.26.25-.43.25-.08 0-.17-.02-.25-.07l-3.5-2c-.24-.13-.32-.44-.18-.68s.44-.32.68-.18l3.5 2c.24.13.32.44.18.68z" />
                                </svg>
                            </div>
                        </div>
                    </a> -->

                    <!-- Sharingbutton Pinterest -->
                    <!-- <a class="resp-sharing-button__link"
                        href="https://pinterest.com/pin/create/button/?url=http%3A%2F%2Fsharingbuttons.io&amp;media=http%3A%2F%2Fsharingbuttons.io&amp;description=Super%20fast%20and%20easy%20Social%20Media%20Sharing%20Buttons.%20No%20JavaScript.%20No%20tracking."
                        target="_blank" rel="noopener" aria-label="">
                        <div class="resp-sharing-button resp-sharing-button--pinterest resp-sharing-button--small">
                            <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                    <path
                                        d="M12.14.5C5.86.5 2.7 5 2.7 8.75c0 2.27.86 4.3 2.7 5.05.3.12.57 0 .66-.33l.27-1.06c.1-.32.06-.44-.2-.73-.52-.62-.86-1.44-.86-2.6 0-3.33 2.5-6.32 6.5-6.32 3.55 0 5.5 2.17 5.5 5.07 0 3.8-1.7 7.02-4.2 7.02-1.37 0-2.4-1.14-2.07-2.54.4-1.68 1.16-3.48 1.16-4.7 0-1.07-.58-1.98-1.78-1.98-1.4 0-2.55 1.47-2.55 3.42 0 1.25.43 2.1.43 2.1l-1.7 7.2c-.5 2.13-.08 4.75-.04 5 .02.17.22.2.3.1.14-.18 1.82-2.26 2.4-4.33.16-.58.93-3.63.93-3.63.45.88 1.8 1.65 3.22 1.65 4.25 0 7.13-3.87 7.13-9.05C20.5 4.15 17.18.5 12.14.5z" />
                                </svg>
                            </div>
                        </div>
                    </a> -->

                </div>
            </div>
            <div class="mt-3">
                <div class="card border-0 mb-2">
                    <div class="video-wrap">
                        <div class="embed-responsive embed-responsive-item embed-responsive-16by9 m-b-1 video">
                            <iframe class="embed-responsive-item" style="height: 420px; border: 0px;"
                                src="//www.youtube.com/embed/<?php echo $video->videoURL; ?>?rel=0&modestbranding=1&autohide=1&showinfo=0&controls=0&autoplay=1"
                                allowfullscreen></iframe>
                        </div>
                    </div>
                </div>

                <h3 class="titletwo pt-5"> RECOMMENDED VIDEOS</h3>
                <div class="container">
                    <div class="row py-5">
                        <?php
                            $tcnt = 0;
                            if (count($recommended) > 0){
                            foreach ($recommended as $allrecommended){
                            foreach ($allrecommended as $rec){
                                $tcnt++;
                                ?>
                        <div class="col-md-4 mt-3 v-col">
                            <div class="card">
                                <a href="{{url('videos/watch/'.$rec->id.'/'.Str::slug($rec->title))}}">
                                    <!-- <img class="card-img-top"
                                        src="<?= "http://img.youtube.com/vi/".$rec->videoURL."/0.jpg"; ?>"
                                        alt="No image found"
                                        onError="this.onerror=null;this.src='{{asset('/images/pic.jpg')}}';"> -->
                                    <img src="{{ asset('/images/pic.jpg') }}"
                                        data-src="{{'http://img.youtube.com/vi/'.$rec->videoURL.'/0.jpg'}}"
                                        class="lazy card-img-top" alt="{{$rec->title}}">
                                </a>
                                <div class="card-body card-block">
                                    <h3 class="h6"><a class="stext"
                                            href="{{url('videos/watch/'.$rec->id.'/'.Str::slug($rec->title))}}"
                                            class="card-link"><?= $rec->title; ?></a></h3>
                                </div>
                            </div>
                        </div>

                        <?php
                                if ($tcnt % 3 == 0) {
                                    ?>
                        <div class="clearfix"></div>
                        <?php
                                }
                            }}}
                        ?>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-3 ml-5 mt-3">

            <style>
            .side_text a {
                color: #000000;
                text-decoration: none;
                font-family: Futura Md BT;
            }
            </style>
            <?php 
            if(count($popular) > 0){?>
            <h1 class="text-danger mt-4"><strong>Top Stories</strong></h1>
            <?php foreach ($popular as $art) : 
                $articleID = explode('/',$art->url)[3] == "article" ? explode('/',$art->url)[4] : explode('/',$art->url)[3];
                $value = App\Eve::get_single_article($articleID);
                // echo dd($articleID, $value);
                ?>
            <p class="side_text">
                <a href="<?php echo url('article/' . $value->id . '/' . Str::slug($value->title)); ?>"
                    style="font-size: 18px" class="sidetitles">
                    <?php echo $value->title; ?>
                </a>
                <br />
                <small>
                    <a href="<?php echo url('article/' . $value->id . '/' . Str::slug($value->title)); ?>"
                        style="color: #a9a3a3"><?php echo App\Eve::getCatName($value->categoryid); ?>
                        By
                        {{$value->author}}
                    </a>
                </small>
            </p>
            <hr />
            <?php endforeach; ?>

            <div id='div-gpt-ad-1512394772255-0' style='width: 100%; text-align:center;'>
                <script>
                googletag.cmd.push(function() {
                    googletag.display('div-gpt-ad-1512394772255-0');
                });
                </script>
            </div>

            <?php } ?>

            <a href="https://chat.whatsapp.com/EHrRa5PINh67lQqv3BuCFT" target="_blank">
                <img src="https://www.standardmedia.co.ke/evewoman/assets/img/whatsapp2.png" class="img-fluid" alt="">
            </a>

            <h1 class="text-danger"><strong>{{count($popular) > 0 ? 'Latest Stories':'Top Stories'}}</strong></h1>

            <?php if(count($getArticleLatest)){
                  foreach ($getArticleLatest as $articleLatest) {
                ?>
            <p class="side_text">
                <a href="<?php echo url('article/' . $articleLatest->id . '/' . Str::slug($articleLatest->title)); ?>"
                    style="font-size: 18px" class="sidetitles">
                    <?php echo $articleLatest->title; ?>
                </a>
                <br />
                <small>
                    <a href="<?php echo url('article/' . $articleLatest->id . '/' . Str::slug($articleLatest->title)); ?>"
                        style="color: #a9a3a3"><?php echo App\Eve::getCatName($articleLatest->categoryid); ?>
                        By
                        {{$articleLatest->author}}
                    </a>
                </small>
            </p>
            <hr />
            <?php }} ?>

        </div>
    </div>
</div>
@stop