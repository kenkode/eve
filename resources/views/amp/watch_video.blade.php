@extends('layouts.amp')
@section('content')
<div class="container py-3 mt-3 main-cont">
    <main id="content" role="main" class="">
        <amp-ad width=320 height=100 layout="responsive" type="doubleclick" data-slot="/24409412/evewoman_top_advert"
            data-multi-size="320x50">
        </amp-ad>
        <div class="col-md-12" style="margin-top:550px">
            <h4>
                <strong>
                    <?php echo $video->title; ?>
                </strong>
            </h4>
            <div class="btn-group float-right">
                <a href="https://www.facebook.com/sharer.php?u={{ url()->current() }}">
                    <amp-img src="https://img.icons8.com/color/50/000000/facebook-circled.png" width="48" height="48"
                        class="mx1"></amp-img>
                </a>
                <a href="https://twitter.com/share?url={{ url()->current() }}">
                    <amp-img src="https://img.icons8.com/color/50/000000/twitter-circled.png" width="48" height="48"
                        class="mx1"></amp-img>
                </a>
                <a href="https://telegram.me/share/url?url={{ url()->current() }}">
                    <amp-img src="https://img.icons8.com/fluent/48/000000/telegram-app.png" width="48" height="48"
                        class="mx1"></amp-img>
                </a>
                @if((new \Jenssegers\Agent\Agent())->isMobile())
                <a href="https://api.whatsapp.com/send?text={{ url()->current() }}">
                    <amp-img src="{{asset('images/whatsapp.png')}}" width="48" height="48" class="mx1"></amp-img>
                </a>
                @else
                <a href="https://web.whatsapp.com/send?text={{ url()->current() }}">
                    <amp-img src="{{asset('images/whatsapp.png')}}" width="48" height="48" class="mx1"></amp-img>
                </a>
                @endif
            </div>
            <div class="mt-3">
                <div class="card border-0 mb-2">
                    <div class="video-wrap">
                        <div class="embed-responsive embed-responsive-item embed-responsive-16by9 m-b-1 video">
                            <amp-iframe sandbox="allow-scripts allow-same-origin allow-popups" height="400" width="600"
                                layout="responsive" frameborder="0"
                                src="//www.youtube.com/embed/<?php echo $video->videoURL; ?>?rel=0&modestbranding=1&autohide=1&showinfo=0&controls=0&autoplay=1"
                                allowfullscreen></amp-iframe>
                        </div>
                    </div>
                </div>

                <h3 class="titletwo pt-5" style="margin-top:20px"> RECOMMENDED VIDEOS</h3>
                <div class="container">
                    <div class="row py-5">
                        <?php
                            $tcnt = 0;
                            if (count($recommended) > 0){
                            foreach ($recommended as $allrecommended){
                            foreach ($allrecommended as $rec){
                                $tcnt++;
                                ?>
                        <div class="col-md-12" style="margin-top:20px">
                            <div class="card">
                                <a href="{{url('videos/watch/'.$rec->id.'/'.Str::slug($rec->title))}}">
                                    <!-- <img class="card-img-top"
                                        src="<?= "http://img.youtube.com/vi/".$rec->videoURL."/0.jpg"; ?>"
                                        alt="No image found"
                                        onError="this.onerror=null;this.src='{{asset('/images/pic.jpg')}}';"> -->
                                    <amp-img src="{{'http://img.youtube.com/vi/'.$rec->videoURL.'/0.jpg'}}"
                                        class="lazy card-img-top" alt="" width="339" height="225" layout="responsive">
                                    </amp-img>
                                </a>
                                <p class="side_text mt-3 mb-3">
                                    <a href="{{url('amp/videos/watch/'.$rec->id.'/'.Str::slug($rec->title))}}"
                                        class="card-link">{{$rec->title}}</a>
                                </p>
                            </div>
                        </div>

                        <?php
                        if ($tcnt == 5) {?>
                            <amp-ad width=300 height=250 type="doubleclick"
                                data-slot="/24409412/Evewoman_rightpanel_advert1" data-multi-size="336x280">
                            </amp-ad>
                        <?php }
                                if ($tcnt % 5 == 0) {?>
                        <div class="clearfix"></div>
                        <?php
                                }
                            }}}
                        ?>
                    </div>
                </div>
            </div>
            <amp-ad width=300 height=250 type="doubleclick" data-slot="/24409412/Evewoman_rightpanel_advert2"
                data-multi-size="336x280">
            </amp-ad>
            <?php 
            if(count($popular) > 0){?>
            <h1 class="text-danger mt-4" style="margin-top:20px"><strong>Top Stories</strong></h1>
            <?php foreach ($popular as $art) : 
                $articleID = explode('/',$art->url)[3] == "article" ? explode('/',$art->url)[4] : explode('/',$art->url)[3];
                $value = App\Eve::get_single_article($articleID);
                // echo dd($articleID, $value);
                ?>
            <p class="side_text">
                <a href="<?php echo url('article/' . $value->id . '/' . Str::slug($value->title)); ?>"
                    style="font-size: 18px" class="sidetitles">
                    <?php echo $value->title; ?>
                </a>
                <br />
                <small>
                    <a href="<?php echo url('article/' . $value->id . '/' . Str::slug($value->title)); ?>"
                        style="color: #a9a3a3"><?php echo App\Eve::getCatName($value->categoryid); ?>
                        By
                        {{$value->author}}
                    </a>
                </small>
            </p>
            <hr />
            <?php endforeach; ?>

            <amp-ad width=300 height=250 type="doubleclick" data-slot="/24409412/Evewoman_rightpanel_advert5"
                data-multi-size="336x280">
            </amp-ad>

            <?php } ?>

            <a href="https://chat.whatsapp.com/EHrRa5PINh67lQqv3BuCFT" style="margin-top:20px" target="_blank">
                <amp-img src="https://www.standardmedia.co.ke/evewoman/assets/img/whatsapp2.png" width="339" height="225" layout="responsive" class="img-fluid" alt=""></amp-img>
            </a>

            <h1 class="text-danger"><strong>{{count($popular) > 0 ? 'Latest Stories':'Top Stories'}}</strong></h1>

            <?php if(count($getArticleLatest)){
                  foreach ($getArticleLatest as $articleLatest) {
                ?>
            <p class="side_text">
                <a href="<?php echo url('amp/article/' . $articleLatest->id . '/' . Str::slug($articleLatest->title)); ?>"
                    style="font-size: 18px" class="sidetitles">
                    <?php echo $articleLatest->title; ?>
                </a>
                <br />
                <small>
                    <a href="<?php echo url('amp/article/' . $articleLatest->id . '/' . Str::slug($articleLatest->title)); ?>"
                        style="color: #a9a3a3"><?php echo App\Eve::getCatName($articleLatest->categoryid); ?>
                        By
                        {{$articleLatest->author}}
                    </a>
                </small>
            </p>
            <hr />
            <?php }} ?>
            <amp-ad width=300 height=250 type="doubleclick" data-slot="/24409412/evewoman_leaderboard_3"
                data-multi-size="320x100">
            </amp-ad>
        </div>
    </main>
</div>
@stop