@extends('layouts.amp')
@section('content')

<div class="container py-3 main-cont">
    <amp-ad width=320 height=100 layout="responsive" type="doubleclick" data-slot="/24409412/evewoman_top_advert" data-multi-size="320x50">
    </amp-ad>
    <main id="content" role="main" class="">
        <article class="article-holder">
            <header>

                <h1 class="mb1 px0"> <?php echo $article->title; ?></h1>
                <?php echo App\Eve::getCatName($article->categoryid); ?> By
                <span class="bycolorpic">
                    <a href="{{url('author/' . $article->author_id . '/' . Str::slug($article->author))}}">
                        {{$article->author}}
                    </a>
                </span>
                <!-- Start byline -->
                <div class="btn-group float-right">
                    <a
                        href="https://www.facebook.com/sharer.php?u={{url('amp/'.Str::slug(App\Eve::getCatName($article->categoryid)).'/article/'.$article->id.'/'.Str::slug($article->title))}}">
                        <amp-img src="https://img.icons8.com/color/50/000000/facebook-circled.png" width="48"
                            height="48" class="mx1"></amp-img>
                    </a>
                    <a
                        href="https://twitter.com/share?url={{url('amp/'.Str::slug(App\Eve::getCatName($article->categoryid)).'/article/'.$article->id.'/'.Str::slug($article->title))}}">
                        <amp-img src="https://img.icons8.com/color/50/000000/twitter-circled.png" width="48" height="48"
                            class="mx1"></amp-img>
                    </a>
                    <a
                        href="https://telegram.me/share/url?url={{url('amp/'.Str::slug(App\Eve::getCatName($article->categoryid)).'/article/'.$article->id.'/'.Str::slug($article->title))}}">
                        <amp-img src="https://img.icons8.com/fluent/48/000000/telegram-app.png" width="48" height="48"
                            class="mx1"></amp-img>
                    </a>
                    @if((new \Jenssegers\Agent\Agent())->isMobile())
                    <a href="https://api.whatsapp.com/send?text={{ url()->current() }}">
                        <amp-img src="{{asset('images/whatsapp.png')}}" width="48" height="48" class="mx1"></amp-img>
                    </a>
                    @else
                    <a href="https://web.whatsapp.com/send?text={{ url()->current() }}">
                        <amp-img src="{{asset('images/whatsapp.png')}}" width="48"
                            height="48" class="mx1"></amp-img>
                    </a>
                    @endif
                </div>
                <!-- End byline -->
            </header>
            <?php
                function ampify($html='') {

                    # Replace img, audio, and video elements with amp custom elements
                    $html = str_ireplace(
                        ['<img','<video','/video>','<audio','/audio>'],
                        ['<amp-img','<amp-video','/amp-video>','<amp-audio','/amp-audio>'],
                        $html
                    );
                
                    # Add closing tags to amp-img custom element
                    $html = preg_replace('/<amp-img(.*?)\/>/', '<amp-img$1 width="339" height="225" layout="responsive"></amp-img>',$html);
                
                    # Whitelist of HTML tags allowed by AMP
                    $html = strip_tags($html,'<h1><h2><h3><h4><h5><h6><a><p><ul><ol><li><blockquote><q><cite><ins><del><strong><em><code><pre><svg><table><thead><tbody><tfoot><th><tr><td><dl><dt><dd><article><section><header><footer><aside><figure><time><abbr><div><span><hr><small><br><amp-img><amp-audio><amp-video><amp-ad><amp-anim><amp-carousel><amp-fit-rext><amp-image-lightbox><amp-instagram><amp-lightbox><amp-twitter><amp-youtube>');
                
                    return $html;
                
                }
            
                $story = ampify($article->story); 

                $content_table = explode("</p>",
                str_replace("/evemedia/","https://cdn.standardmedia.co.ke/evemedia/",$story));
                if (count($content_table)
                <= 1) { $content_table=explode("<br />",
                str_replace("/evemedia/","https://cdn.standardmedia.co.ke/evemedia/",$story));
                }
                
                $x = count($content_table);
                $x = (int)$x;
                $y = 0;
                $z = 0;
                foreach ($content_table as $value) {
                echo $value;
                if (($y % 7 == 0) && ($z <= 2)) { if (is_array($related_news) && (isset($related_news[$z]))) {?>
            <p class="text text-default also-read">
                <strong>ALSO READ: </strong>
                <?php $value = $related_news[$z];?>
                <a class="alert-link text-danger"
                    href="{{url(Str::slug(App\Eve::getCatName($value->categoryid)).'/article/' . $value->id . '/' . Str::slug($value->title))}}">
                    {{$value->title}} </a>
                <?php $z++;
                    }
                 }

                if($y == 2){?>
                <amp-ad width=300 height=250 type="doubleclick" data-slot="/24409412/Evewoman_rightpanel_advert1"
                    data-multi-size="336x280">
                </amp-ad>
                <?php }
                if($y == 7){?>
                <amp-ad width=300 height=250 type="doubleclick" data-slot="/24409412/Evewoman_rightpanel_advert2"
                    data-multi-size="336x280">
                </amp-ad>

                <?php }

                if ($y == 2) {
                    //$this->view("ads/desktop_article_center_300x250");
                }


                $y++;
            }
        ?>
                <amp-ad width=300 height=250 type="doubleclick" data-slot="/24409412/Evewoman_rightpanel_advert5"
                    data-multi-size="336x280">
                </amp-ad>
                <h4 class="mr-3">
                    Share
                    this
                    article
                </h4>
                <div class="btn-group float-right">
                    <a
                        href="https://www.facebook.com/sharer.php?u=https://www.standardmedia.co.ke/evewoman/crazy-monday/article/2001378124/five-reasons-your-child-is-acting-out-and-how-to-handle-it">
                        <amp-img src="https://img.icons8.com/color/50/000000/facebook-circled.png" width="48"
                            height="48" class="mx1">
                        </amp-img>
                    </a>
                    <a
                        href="https://twitter.com/share?url=https://www.standardmedia.co.ke/evewoman/crazy-monday/article/2001378124/five-reasons-your-child-is-acting-out-and-how-to-handle-it">
                        <amp-img src="https://img.icons8.com/color/50/000000/twitter-circled.png" width="48" height="48"
                            class="mx1">
                        </amp-img>
                    </a>
                    <a
                        href="https://telegram.me/share/url?url=https://www.standardmedia.co.ke/evewoman/crazy-monday/article/2001378124/five-reasons-your-child-is-acting-out-and-how-to-handle-it">
                        <amp-img src="https://img.icons8.com/fluent/48/000000/telegram-app.png" width="48" height="48"
                            class="mx1">
                        </amp-img>
                    </a>
                    @if((new \Jenssegers\Agent\Agent())->isMobile())
                    <a href="https://api.whatsapp.com/send?text={{ url()->current() }}">
                        <amp-img src="{{asset('images/whatsapp.png')}}" width="48" height="48" class="mx1">
                        </amp-img>
                    </a>
                    @else
                    <a href="https://web.whatsapp.com/send?text={{ url()->current() }}">
                        <amp-img src="{{asset('images/whatsapp.png')}}" width="48"
                            height="48" class="mx1"></amp-img>
                    </a>
                    @endif
                </div>
                <amp-ad width=300 height=250 type="doubleclick" data-slot="/24409412/evewoman_leaderboard_3"
                    data-multi-size="320x100">
                </amp-ad>
                <!-- <amp-ad layout="responsive" width=300 height=800 type="adsense" data-ad-client="ca-pub-2204615711705377"
                    data-ad-slot="7206855923">
                </amp-ad> -->
                <a href=" https://chat.whatsapp.com/EHrRa5PINh67lQqv3BuCFT" target="_blank">
                    <amp-img src="{{asset('/assets/img/whatsapp2.png')}}" layout="responsive" width="339" height="225"
                        alt=""></amp-img>
                </a>

                <h1 class="text-danger mt-3"><strong>{{count($popular) > 0 ? 'Latest Stories':'Top Stories'}}</strong>
                </h1>

                <?php foreach ($getArticleLatest as $articleLatest) :
                if ($article->id == $articleLatest->id) {
                    continue;
                }
                ?>
                <p class="side_text">
                    <a href="{{url('amp/'.Str::slug(App\Eve::getCatName($articleLatest->categoryid)).'/article/' . $articleLatest->id . '/' . Str::slug($articleLatest->title))}}"
                       class="sidetitles">
                        <?php echo $articleLatest->title; ?>
                    </a>
                    <br />
                    <small>
                        <a
                            href="{{url('amp/category/' . $articleLatest->categoryid . '/' . Str::slug(App\Eve::getCatName($articleLatest->categoryid)))}}"
                        ><?php echo App\Eve::getCatName($articleLatest->categoryid); ?>
                        By
                        <a href="{{url('amp/author/' . $articleLatest->author_id . '/' . Str::slug($articleLatest->author))}}">
                            {{$articleLatest->author}}
                        </a>
                        </a>
                    </small>
                </p>
                <hr />
                <?php endforeach; ?>
        </article>
    </main>
    @stop