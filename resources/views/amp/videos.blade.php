@extends('layouts.amp')
@section('content')
<div class="container py-3 mt-3 main-cont">
    <main id="content" role="main" class="">
        <amp-ad width=320 height=100 layout="responsive" type="doubleclick" data-slot="/24409412/evewoman_top_advert"
            data-multi-size="320x50">
        </amp-ad>

        <section class="mb-5 mt-3">
            <div id="article" class="container">
                <div class="row m-b-1">
                    <div class="col-lg-12">
                        <h1 class="article-heading">Videos</h1>
                    </div>
                    <div class="row">
                        <?php
                    $dcnt = 0;
                    $d=[];
                    if(count($videos) > 0){
                    foreach ($videos as $allvideos){
                        $d=$allvideos;
                    foreach ($allvideos as $video){
                        $dcnt++;
                        if($dcnt == 6){?>
                        <amp-ad width=300 height=250 type="doubleclick"
                            data-slot="/24409412/Evewoman_rightpanel_advert1" data-multi-size="336x280">
                        </amp-ad>
                        <?php }
                            if($dcnt == 13){?>
                        <amp-ad width=300 height=250 type="doubleclick"
                            data-slot="/24409412/Evewoman_rightpanel_advert2" data-multi-size="336x280">
                        </amp-ad>

                        <?php }
                        if($dcnt == 21){?>
                        <amp-ad width=300 height=250 type="doubleclick"
                            data-slot="/24409412/Evewoman_rightpanel_advert5" data-multi-size="336x280">
                        </amp-ad>
                        <?php }
                        ?>
                        <div class="col-md-12 mt-3 v-col">
                            <div class="card">
                                <a href="{{url('amp/videos/watch/'.$video->id.'/'.Str::slug($video->title))}}">
                                    <!-- <img class="card-img-top"
                                    src="{{'http://img.youtube.com/vi/'.$video->videoURL.'/0.jpg'}}"
                                    alt="No image found"
                                    onError="this.onerror=null;this.src='{{asset('/images/pic.jpg')}}';"> -->
                                    <amp-img width="339" height="225" layout="responsive" style="margin-top:20px"
                                        src="{{'http://img.youtube.com/vi/'.$video->videoURL.'/0.jpg'}}"
                                        class="lazy card-img-top" alt=""></amp-img>
                                </a>
                                <p class="side_text mt-3 mb-3">
                                    <a href="{{url('amp/videos/watch/'.$video->id.'/'.Str::slug($video->title))}}"
                                        class="card-link">{{$video->title}}</a>
                                </p>
                            </div>
                        </div>
                        <?php

                    }}}
                        ?>
                        <div class="clearfix"></div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    {!! count($videos) > 0 ? $d->links():'' !!}
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <amp-ad width=300 height=250 type="doubleclick" data-slot="/24409412/evewoman_leaderboard_3"
                        data-multi-size="320x100">
                    </amp-ad>
                </div>
            </div>
        </section>
    </main>
</div>
@stop