@extends('layouts.amp')
@section('content')
<div class="container py-3 mt-3 main-cont">
    <main id="content" role="main" class="">
        <amp-ad width=320 height=100 layout="responsive" type="doubleclick" data-slot="/24409412/evewoman_top_advert"
            data-multi-size="320x50">
        </amp-ad>
        <div class="col-md-12">
            <a style="text-decoration:none"
                href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                <div class="card bg-dark text-white">
                    <amp-img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}" width="339" height="225"
                        layout="responsive" class="lazy card-img img-fluid px500" alt=""></amp-img>
                    <div class="card-img-overlay">
                        <div class="list-group-heading"><a style="color:#ed008c"
                                href="{{url('amp/author/' . $mainArticle->author_id . '/' . Str::slug($mainArticle->author))}}">By
                                {{$mainArticle->author}}
                                <span
                                    class="card-title text-white">{{App\Eve::time_difference($mainArticle->publishdate)}}</span></a>
                        </div>
                        <h4 class="card-text">
                            <a class="text-white" style="text-decoration:none"
                                href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                {{$mainArticle->title}}
                            </a>
                        </h4>
                        <h5 class="card-title text-white">
                            <a class="card-text text-white" style="text-decoration:none"
                                href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                {{$mainArticle->summary}}
                            </a>
                        </h5>
                        <hr class="bg-yellow">


                    </div>
            </a>
        </div>
        <div class="row" style="margin:0">
            <div class="card">
                <div class="list-group">
                    @foreach($getSideLatest as $mainArticle)
                    <?php $url = url('amp/author/' . $mainArticle->author_id . '/' . Str::slug($mainArticle->author));?>

                    <div class=" col-md-6 linegrey py-4">

                        <a
                            href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                            <amp-img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}" width="339"
                                height="225" layout="responsive" class="lazy card-img-top mt-4 img-fluid" alt="">
                            </amp-img>
                        </a>

                    </div>
                    <p class="side_text">
                        <small>
                            <a href="{{url('amp/category/' . $mainArticle->categoryid . '/' . Str::slug(App\Eve::getCatName($mainArticle->categoryid)))}}"
                                style="color: #F62F5E">
                                {{App\Eve::getCatName($mainArticle->categoryid)}}
                            </a>
                        </small>
                        <br />
                        <a href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}"
                            style="font-size: 18px" class="sidetitles">
                            {{$mainArticle->title}}
                        </a>
                        <br />
                        <small>
                            <a href="#" style="color: #a9a3a3">
                                By {{$mainArticle->author}}
                                - {{App\Eve::time_difference($mainArticle->publishdate)}}
                            </a>
                        </small>


                    </p>
                    <hr />

                    <!-- <div class="list-group-content">
                        <a
                            href="{{url('amp/author/' . $mainArticle->author_id . '/' . Str::slug($mainArticle->author))}}">
                            <div class="list-group-heading">
                                {{$mainArticle->author}}
                                <span>{{App\Eve::time_difference($mainArticle->publishdate)}}</span></div>
                        </a>
                        <a
                            href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                            <p class="text-sm">
                                {{$mainArticle->title}}

                            </p>
                        </a>
                    </div> -->
                    @endforeach
                </div>
            </div>
        </div>

        <div class="card-deck">
            @foreach ($getLatest as $mainArticle)
            <div class=" col-md-6 linegrey py-4">

                <a
                    href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                    <amp-img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}" width="339" height="225"
                        layout="responsive" class="lazy card-img-top mt-4 img-fluid" alt="">
                    </amp-img>
                </a>

            </div>
            <p class="side_text">
                <small>
                    <a href="{{url('amp/category/' . $mainArticle->categoryid . '/' . Str::slug(App\Eve::getCatName($mainArticle->categoryid)))}}"
                        style="color: #F62F5E">
                        {{App\Eve::getCatName($mainArticle->categoryid)}}
                    </a>
                </small>
                <br />
                <a href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}"
                    style="font-size: 18px" class="sidetitles">
                    {{$mainArticle->title}}
                </a>
                <br />
                <small>
                    <a href="#" style="color: #a9a3a3">
                        By {{$mainArticle->author}}
                        - {{App\Eve::time_difference($mainArticle->publishdate)}}
                    </a>
                </small>


            </p>
            <hr />
            <!-- <div class="card mt-4">
                <a style="text-decoration:none"
                    href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                    <img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}" width="339" height="225"
                        layout="responsive" class="lazy card-img img-fluid px500" alt="">
                    <div class="card-body px-0 pb-0">
                        <h5 class="card-title mb-1">{{$mainArticle->title}}</h5>
                    </div>
                </a>
                <div class="card-footer text-left px-0 pt-0">
                    <a href="{{url('amp/author/' . $mainArticle->author_id . '/' . Str::slug($mainArticle->author))}}">
                        <div class="list-group-heading">{{$mainArticle->author}} |
                            <span>{{App\Eve::time_difference($mainArticle->publishdate)}}</span></div>
                    </a>
                </div>
            </div> -->
            @endforeach
        </div>
        <!-- /24409412/evewoman_leaderboard_2 -->
        <amp-ad width=300 height=250 type="doubleclick" data-slot="/24409412/Evewoman_rightpanel_advert1"
            data-multi-size="336x280">
        </amp-ad>

        <div class="container">
            <div class="row">
                @foreach ($getTwoLatest as $mainArticle)
                <div class="card mb-3">
                    <div class=" col-md-6 linegrey py-4">

                        <a
                            href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                            <amp-img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}" width="339"
                                height="225" layout="responsive" class="lazy card-img-top mt-4 img-fluid" alt="">
                            </amp-img>
                        </a>

                    </div>
                    <p class="side_text">
                        <small>
                            <a href="{{url('amp/category/' . $mainArticle->categoryid . '/' . Str::slug(App\Eve::getCatName($mainArticle->categoryid)))}}"
                                style="color: #F62F5E">
                                {{App\Eve::getCatName($mainArticle->categoryid)}}
                            </a>
                        </small>
                        <br />
                        <a href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}"
                            style="font-size: 18px" class="sidetitles">
                            {{$mainArticle->title}}
                        </a>
                        <br />
                        <small>
                            <a href="#" style="color: #a9a3a3">
                                By {{$mainArticle->author}}
                                - {{App\Eve::time_difference($mainArticle->publishdate)}}
                            </a>
                        </small>


                    </p>
                    <hr />
                </div>
                @endforeach
            </div>
            <!-- /24409412/evewoman_leaderboard_3 -->
            <amp-ad width=300 height=250 type="doubleclick" data-slot="/24409412/Evewoman_rightpanel_advert2"
                data-multi-size="336x280">
            </amp-ad>
        </div>
        </section>
        @if($homeVideo != null)
        <section class="bg-lights">
            <div class="px90">
                <div class="card mb-3">
                    <div class="row no-gutters">
                        <div class="col-7">
                            <div class="embed-responsive embed-responsive-21by9">
                                <iframe class="embed-responsive-item"
                                    src="https://www.youtube.com/embed/<?= $homeVideo->videoURL.'?autoplay=1'; ?>"
                                    allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="card-body text-center">
                                <div class="center">
                                    <h1 class="text-dark">
                                        {{$homeVideo->title}}</h1>

                                    <div class="text-center">
                                        <div class="list-group-heading mb-2">{{$homeVideo->author}} |
                                            <span>{{App\Eve::time_difference($homeVideo->publishdate)}}</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        @endif
        <section class="px323">
            <div class="container">
                <div class="row">
                    @foreach($getMoreTwoLatest as $mainArticle)
                    <div class="card mb-3">
                        <div class=" col-md-6 linegrey py-4">

                            <a
                                href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                <amp-img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}" width="339"
                                    height="225" layout="responsive" class="lazy card-img-top mt-4 img-fluid" alt="">
                                </amp-img>
                            </a>

                        </div>
                        <p class="side_text">
                            <small>
                                <a href="{{url('amp/category/' . $mainArticle->categoryid . '/' . Str::slug(App\Eve::getCatName($mainArticle->categoryid)))}}"
                                    style="color: #F62F5E">
                                    {{App\Eve::getCatName($mainArticle->categoryid)}}
                                </a>
                            </small>
                            <br />
                            <a href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}"
                                style="font-size: 18px" class="sidetitles">
                                {{$mainArticle->title}}
                            </a>
                            <br />
                            <small>
                                <a href="#" style="color: #a9a3a3">
                                    By {{$mainArticle->author}}
                                    - {{App\Eve::time_difference($mainArticle->publishdate)}}
                                </a>
                            </small>


                        </p>
                        <hr />

                    </div>
                    @endforeach
                </div>
                <!-- /24409412/evewoman_leaderboard_4 -->
                <amp-ad width=300 height=250 type="doubleclick" data-slot="/24409412/Evewoman_rightpanel_advert5"
                    data-multi-size="336x280">
                </amp-ad>
            </div>
        </section>
        <section class="bgrey">
            <div class="container">
                <h2 class="text-center ">Fashion And Beauty</h2>
                <div class="card-deck">
                    @foreach($getFashionAndBeauty as $mainArticle)
                    <div class="card mt-4">
                        <div class=" col-md-6 linegrey py-4">

                            <a
                                href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                <amp-img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}" width="339"
                                    height="225" layout="responsive" class="lazy card-img-top mt-4 img-fluid" alt="">
                                </amp-img>
                            </a>

                        </div>
                        <p class="side_text">
                            <small>
                                <a href="{{url('amp/category/' . $mainArticle->categoryid . '/' . Str::slug(App\Eve::getCatName($mainArticle->categoryid)))}}"
                                    style="color: #F62F5E">
                                    {{App\Eve::getCatName($mainArticle->categoryid)}}
                                </a>
                            </small>
                            <br />
                            <a href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}"
                                style="font-size: 18px" class="sidetitles">
                                {{$mainArticle->title}}
                            </a>
                            <br />
                            <small>
                                <a href="#" style="color: #a9a3a3">
                                    By {{$mainArticle->author}}
                                    - {{App\Eve::time_difference($mainArticle->publishdate)}}
                                </a>
                            </small>


                        </p>
                        <hr />
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        <section class="px323">
            <div class="container">
                <div class="row">
                    @foreach($getMoreBigLatest as $mainArticle)
                    <div class="card mb-3">
                        <div class=" col-md-6 linegrey py-4">

                            <a
                                href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                <amp-img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}" width="339"
                                    height="225" layout="responsive" class="lazy card-img-top mt-4 img-fluid" alt="">
                                </amp-img>
                            </a>

                        </div>
                        <p class="side_text">
                            <small>
                                <a href="{{url('amp/category/' . $mainArticle->categoryid . '/' . Str::slug(App\Eve::getCatName($mainArticle->categoryid)))}}"
                                    style="color: #F62F5E">
                                    {{App\Eve::getCatName($mainArticle->categoryid)}}
                                </a>
                            </small>
                            <br />
                            <a href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}"
                                style="font-size: 18px" class="sidetitles">
                                {{$mainArticle->title}}
                            </a>
                            <br />
                            <small>
                                <a href="#" style="color: #a9a3a3">
                                    By {{$mainArticle->author}}
                                    - {{App\Eve::time_difference($mainArticle->publishdate)}}
                                </a>
                            </small>


                        </p>
                        <hr />
                    </div>
                    @endforeach
                </div>

            </div>
        </section>
        <section class="decks">
            <div class="container">
                <h2 class="text-center mb-4 pink">Relationships</h2>
                <div class="card-deck">
                    @foreach($getRelationshipArticle as $mainArticle)
                    <div class="card">
                        <div class=" col-md-6 linegrey py-4">

                            <a
                                href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                <amp-img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}" width="339"
                                    height="225" layout="responsive" class="lazy card-img-top mt-4 img-fluid" alt="">
                                </amp-img>
                            </a>

                        </div>
                        <p class="side_text">
                            <small>
                                <a href="{{url('amp/category/' . $mainArticle->categoryid . '/' . Str::slug(App\Eve::getCatName($mainArticle->categoryid)))}}"
                                    style="color: #F62F5E">
                                    {{App\Eve::getCatName($mainArticle->categoryid)}}
                                </a>
                            </small>
                            <br />
                            <a href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}"
                                style="font-size: 18px" class="sidetitles">
                                {{$mainArticle->title}}
                            </a>
                            <br />
                            <small>
                                <a href="#" style="color: #a9a3a3">
                                    By {{$mainArticle->author}}
                                    - {{App\Eve::time_difference($mainArticle->publishdate)}}
                                </a>
                            </small>


                        </p>
                        <hr />
                    </div>
                    @endforeach
                </div>
            </div>
        </section>

        <!-- <section>
        <div class="container mx-auto mt-50">
            <div class="row mb-4">
                <div class="col-md-8">
                    <div class="card bg-dark text-white">
                        <img src="https://cdn.standardmedia.co.ke/evemedia/eveimages/saturday/the_pain_of_miscarri5ed209d4db267.jpg"
                            class="card-img px500" alt="...">
                        <div class="card-img-overlay">

                            <div class="list-group-heading"> By Mirror <span>10:05 PM</span></div>
                            <h5 class="card-title text-white">Chero Orywa: l lived and travelled for 18 years in the USA
                            </h5>
                            <hr class="bg-yellow">
                            <p class="card-text">Drinking beer could improve your concentration and reduce risk of
                                dementia, study claims
                            </p>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="text-center">
                        <img src="https://tpc.googlesyndication.com/simgad/5668565607239405244" class="ads">
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="bg-lights">
        <div class="px90">

            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-7">
                        <img src="https://cdn.standardmedia.co.ke/evemedia/eveimages/thursday/what_you_need_to_kno5e609212eb911.jpg"
                            class="card-img" alt="...">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body text-center backg">
                            <div class="center">
                                <h1>
                                    Meet the chef who believes it’s evil to waste food
                                    Carlos Espindola </h1>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section> -->
        <section class="px323">
            <div class="container">
                <div class="row">
                    @foreach ($getMore2BigLatest as $mainArticle)
                    <div class="card mb-3">
                        <div class=" col-md-6 linegrey py-4">

                            <a
                                href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                                <amp-img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}" width="339"
                                    height="225" layout="responsive" class="lazy card-img-top mt-4 img-fluid" alt="">
                                </amp-img>
                            </a>

                        </div>
                        <p class="side_text">
                            <small>
                                <a href="{{url('amp/category/' . $mainArticle->categoryid . '/' . Str::slug(App\Eve::getCatName($mainArticle->categoryid)))}}"
                                    style="color: #F62F5E">
                                    {{App\Eve::getCatName($mainArticle->categoryid)}}
                                </a>
                            </small>
                            <br />
                            <a href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}"
                                style="font-size: 18px" class="sidetitles">
                                {{$mainArticle->title}}
                            </a>
                            <br />
                            <small>
                                <a href="#" style="color: #a9a3a3">
                                    By {{$mainArticle->author}}
                                    - {{App\Eve::time_difference($mainArticle->publishdate)}}
                                </a>
                            </small>


                        </p>
                        <hr />

                    </div>
                    @endforeach
                </div>

            </div>
        </section>
    </main>
</div>
@stop