@extends('layouts.amp')
@section('content')
<div class="container py-3 mt-3 main-cont">
    <main id="content" role="main" class="">
        <amp-ad width=320 height=100 layout="responsive" type="doubleclick" data-slot="/24409412/evewoman_top_advert"
            data-multi-size="320x50">
        </amp-ad>
        <div class="col-md-12">
            @if(count($mainCatArticles) > 0)
            @foreach($mainCatArticles as $mainArticle)
            <a
                href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}">
                <!-- <img src="https://cdn.standardmedia.co.ke<?php echo $mainArticle->thumbURL;?>"
                    class="card-img-top img-fluid mt-3 mb-4"
                    onError="this.onerror=null;this.src='{{asset('/images/pic.jpg')}};" alt="..."> -->
                <amp-img src="{{'https://cdn.standardmedia.co.ke'.$mainArticle->thumbURL}}"
                    class="lazy card-img-top mt-4 img-fluid" alt="" width="339" height="225" layout="responsive">
                </amp-img>
            </a>
            <a href="{{url('amp/'.Str::slug(App\Eve::getCatName($mainArticle->categoryid)).'/article/' . $mainArticle->id . '/' . Str::slug($mainArticle->title))}}"
                class="">
                <h4>
                    <strong>
                        {{$mainArticle->title}}
                    </strong>
                </h4>
            </a>

            <span class="">
                <a href="{{url('amp/category/' . $mainArticle->categoryid . '/' .  Str::slug(App\Eve::getCatName($mainArticle->categoryid)))}}"
                    style="color: #F62F5E">
                    <?php echo App\Eve::getCatName($mainArticle->categoryid); ?>
                </a>
                by
                <span class="bycolorpic">
                    <a href="{{url('amp/author/' . $mainArticle->author_id . '/' . Str::slug($mainArticle->author))}}">
                        {{$mainArticle->author}}
                    </a>
                </span>
                <small class="text text-muted font-8">
                    - {{App\Eve::time_difference($mainArticle->publishdate)}}
                </small>

            </span>

            <br />
            <br />
            @endforeach
            @endif
            <?php 
            if(count($sideCatLatest) > 0){
            foreach($sideCatLatest as $latest){

            if ($mainArticle->id == $latest->id){
            continue;
            }
            ?>
            <div class=" col-md-6 linegrey py-4">
                <a
                    href="{{url('amp/'.Str::slug(App\Eve::getCatName($latest->categoryid)).'/article/' . $latest->id . '/' . Str::slug($latest->title))}}">
                    <amp-img src="{{'https://cdn.standardmedia.co.ke'.$latest->thumbURL}}" width="339" height="225"
                        layout="responsive" class="lazy card-img-top mt-4 img-fluid" alt="">
                    </amp-img>
                </a>
            </div>
            <p class="side_text">
                <small>
                    <a
                        href="{{url('amp/category/' . $latest->categoryid . '/' . Str::slug(App\Eve::getCatName($latest->categoryid)))}}">
                        {{App\Eve::getCatName($latest->categoryid)}}
                    </a>
                </small>
                <br />
                <a href="{{url('amp/'.Str::slug(App\Eve::getCatName($latest->categoryid)).'/article/' . $latest->id . '/' . Str::slug($latest->title))}}"
                    class="sidetitles">
                    {{$latest->title}}
                </a>
                <br />
                <small>
                    By
                    <a style="color: #a9a3a3"
                        href="{{url('amp/author/' . $latest->author_id . '/' . Str::slug($latest->author))}}">
                        {{$latest->author}}
                        - {{App\Eve::time_difference($latest->publishdate)}}
                    </a>
                </small>
            </p>
            <hr />
            <?php }} ?>

            <amp-ad width=300 height=250 type="doubleclick" data-slot="/24409412/Evewoman_rightpanel_advert1"
                data-multi-size="336x280">
            </amp-ad>

            <?php 
            if(count($bigCat) > 0){
            foreach($bigCat as $bigLatest){
            if ($mainArticle->id == $bigLatest->id || $latest->id == $bigLatest->id) {
                continue;
            }
            ?>
            <div class="row mt-4">
                <div class=" col-md-6 linegrey py-4">
                    <a
                        href="{{url('amp/'.Str::slug(App\Eve::getCatName($bigLatest->categoryid)).'/article/' . $bigLatest->id . '/' . Str::slug($bigLatest->title))}}">
                        <amp-img src="{{'https://cdn.standardmedia.co.ke'.$bigLatest->thumbURL}}" width="339"
                            height="225" layout="responsive" class="lazy card-img-top mt-4 img-fluid"
                            alt="{{$bigLatest->title}}"></amp-img>
                    </a>
                </div>
                <p class="side_text">
                    <small>
                        <a
                            href="{{url('amp/category/' . $bigLatest->categoryid . '/' . Str::slug(App\Eve::getCatName($bigLatest->categoryid)))}}">
                            {{App\Eve::getCatName($bigLatest->categoryid)}}
                        </a>
                    </small>
                    <br />
                    <a href="{{url('amp/'.Str::slug(App\Eve::getCatName($bigLatest->categoryid)).'/article/' . $bigLatest->id . '/' . Str::slug($bigLatest->title))}}"
                        class="sidetitles">
                        {{$bigLatest->title}}
                    </a>
                    <br />
                    <small>
                        By
                        <a style="color: #a9a3a3"
                            href="{{url('amp/author/' . $bigLatest->author_id . '/' . Str::slug($bigLatest->author))}}">
                            {{$bigLatest->author}}
                            - {{App\Eve::time_difference($bigLatest->publishdate)}}
                        </a>
                    </small>
                </p>
                <hr />
            </div>
            <?php }} ?>
        </div>

        <div class="container">
            <div class='row displayLoaded'></div>
        </div>
    </main>
</div>

<!-- <div class="container mt-5 mb-5">
            <div class="row">
                <div class="col text-center">
                    <div id='response'></div>
                    <input type="hidden" id="current_limit" name="current_limit" value="6" />
                    <input type="hidden" id="categoryid" name="categoryid" value="{{$categoryid}}" />
                    <button type="button" id="topic-more" class="btn btn-danger">Load More</button>
                </div>
            </div>
        </div> -->
@stop