<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

use GuzzleHttp\Client;
use Session;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {

        $url = "https://vas.standardmedia.co.ke/api/login";
    
        $client = new Client(['headers' => [ 'Content-Type' => 'application/json' ],'verify'=> false,'http_errors'=>false]);
        $password = $request->password;
        try {
            $res = $client->request('POST', $url, [
                'form_params' => [
                    'username' => $request->email,
                    'password' => $request->password,
                    "grant_type" => "password",
                    "client_id" => 6,
                    "client_secret" => "UscV6CfXGiu99jt7cTlyUQpjiTNj2BTEG9Cqg6bp",
                    'id' => 10,
                    'secret' => 'pFvwrdA3ycw6VKq3',
                    'app_id' => 2,
			        'app_secret' => 'DrpRd4DqZsEEH3DW'
                ]]);
            if(!isset(json_decode($res->getBody())->access_token)){
                return back()->with('error','Your credentials are invalid....Please try again or register an account if you dont have!');
            }
            $access_token = json_decode($res->getBody())->access_token;
            $url = "https://vas.standardmedia.co.ke/api/me";
    
            $request = $client->get(
                $url,
                ['headers' => 
                    [
                        'Authorization' => "Bearer {$access_token}"
                    ]
                ]);
            $response = json_decode($request->getBody());
            
            $user = new User(
                [
                    "id" => $response->id,
                    "name" => $response->name,
                    "email"=>$response->email,
                    "phone"=>$response->phone,
                    "password"=>$password,
                ]
            );

            Session::put('user', $user);
            
            Auth::login($user);
            return redirect('/');
        } catch (ClientException $e) {
            return back()->with('error','Your credentials are invalid....Please try again!');
       } catch (ServerException $e) {
            return back()->with('error','An error has occurred....Please try again!');
        }
    }

    public function logout(Request $request) {
        Session::flush();
        return redirect('/');
      }
}