<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

use GuzzleHttp\Client;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $url = "https://vas.standardmedia.co.ke/api/register";
    
        $client = new Client(['headers' => [ 'Content-Type' => 'application/json' ],'verify'=> false,'http_errors'=>false]);

        try {
            $res = $client->request('POST', $url, [
                'form_params' => [
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'phone' => $data['phone'],
                    'password' => $data['password'],
                    'password_confirmation' => $data['password_confirmation'],
                    "otp"=> 0,
                    "verify"=> 0,
                    'id' => 10,
                    'secret' => 'pFvwrdA3ycw6VKq3',
                    'app_id' => 2,
			        'app_secret' => 'DrpRd4DqZsEEH3DW'
                ]]);
            
            $response = json_decode($res->getBody());
            // dd($response->message);
            if(isset($response->message)){
                // dd($response->message);
                $user = new User(
                    [
                        "name" => $data['name'],
                        "email"=> $data['email'],
                        "phone"=> $data['phone'],
                    ]
                );

                Session::put('user', $user);
                return redirect('/');
            }else{
                $user = new User(
                    [
                        "name" => $response->name,
                        "email"=>$response->email,
                        "phone"=>$response->phone,
                    ]
                );

                Session::put('user', $user);
                
                Auth::login($user);
                return redirect('/');
            }
        } catch (ClientException $e) {
            return back()->with('error','An error has occurred....Please try again!');
            dd($e);
        } catch (ServerException $e) {
            return back()->with('error','An error has occurred....Please try again!');
            dd($e);
        }
        // return User::create([
        //     'name' => $data['name'],
        //     'email' => $data['email'],
        //     'password' => Hash::make($data['password']),
        // ]);
    }

    public function registration(Request $request)
    {
        $url = "https://vas.standardmedia.co.ke/api/register";
    
        $client = new Client(['headers' => [ 'Content-Type' => 'application/json' ],'verify'=> false,'http_errors'=>false]);
        $password = $request->password;
        try {
            $res = $client->request('POST', $url, [
                'form_params' => [
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'password' => $request->password,
                    'password_confirmation' => $request->password_confirmation,
                    "otp"=> 0,
                    "verify"=> 0,
                    'id' => 10,
                    'secret' => 'pFvwrdA3ycw6VKq3',
                    'app_id' => 2,
			        'app_secret' => 'DrpRd4DqZsEEH3DW'
                ]]);
            
            $response = json_decode($res->getBody());
            // dd($response->message);
            if(isset($response->message)){
                // dd($response->message);
                // $user = new User(
                //     [
                //         "name" => $request->name,
                //         "email"=> $request->email,
                //         "phone"=> $request->phone,
                //     ]
                // );

                // Session::put('user', $user);
                return redirect('/')->with('error',$response->message);
            }else{
                $user = new User(
                    [
                        "name" => $response->name,
                        "email"=>$response->email,
                        "phone"=>$response->phone,
                        "password"=>$password,
                    ]
                );

                Session::put('user', $user);
                
                Auth::login($user);
                return redirect('/');
            }
        } catch (ClientException $e) {
            return back()->with('error','An error has occurred....Please try again!');
            // dd($e);
        } catch (ServerException $e) {
            return back()->with('error','An error has occurred....Please try again!');
            // dd($e);
        }
    }
}
