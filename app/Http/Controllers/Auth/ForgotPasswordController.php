<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

use GuzzleHttp\Client;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function sendResetLinkEmail(Request $request)
    {
        $url = "https://vas.standardmedia.co.ke/api/email/password";
    
        $client = new Client(['headers' => [ 'Content-Type' => 'application/json' ],'verify'=> false,'http_errors'=>false]);
        
        try {
            $res = $client->request('POST', $url, [
                'form_params' => [
                    'email' => $request->email,
                    "redirect_url" => "https://evewoman.standardmedia.co.ke/",
                    'app_id' => 2,
			        'app_secret' => 'DrpRd4DqZsEEH3DW'
                ]]);
                $response = json_decode($res->getBody());
            return redirect('/')->with('success',$response->message);
        } catch (ClientException $e) {
            return back()->with('error','Your credentials are invalid....Please try again!');
       } catch (ServerException $e) {
            return back()->with('error','An error has occurred....Please try again!');
        }
    }
}
