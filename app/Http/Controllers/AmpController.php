<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Eve;
use Illuminate\Support\Str;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

use GuzzleHttp\Client;
use App\User;
Use Session;
use Cache;
use Illuminate\Support\Facades\Validator;

class AmpController extends Controller
{
    //
    public function index()
    {
        $nav = 'home';
        $mainArticle = Eve::getMainArticle();
        $getSideLatest = Eve::getLatest(3, 1);
        $getLatest = Eve::getLatest(3, 4);
        $getTwoLatest = Eve::getLatest(3, 7);
        $getMoreTwoLatest = Eve::getLatest(3, 10);
        $getFashionAndBeauty = Eve::getFashionAndBeautyLatest(4);
        $getRelationshipArticle = Eve::getRelationshipArticle(4);
        $getMoreBigLatest = Eve::getBigLatest(3, 18);
        $getMore2BigLatest = Eve::getBigLatest(3, 24);
        $homeVideo = Eve::getHomeVideo(1, 0);
        $name = 'Eve magazine - Relationships, career, recipes, business, health, fashion and beauty';
        return view('amp.index',compact('nav','mainArticle','getSideLatest','getLatest','getTwoLatest',
                    'getMoreTwoLatest','getFashionAndBeauty','getRelationshipArticle','getMoreBigLatest',
                    'getMore2BigLatest','homeVideo','name'));
    }

    public function catArticle($category,$id,$name)
    {
        $nav = 'home';
        
        $article = Eve::get_single_article($id);
        if($article == null){
            return redirect('/');
        }
        // dd( $article->title);
        $getArticleLatest = Eve::get_article_latest(6);
        $related_news = Eve::related_articles($article);
        $popular = Cache::remember("popular.articles", now()->addSeconds(172800), function () {
            return Eve::getTrendingArticles(6, "today");
        });
        // dd($popular);
        // $popular = Eve::popular_articles(6);
        $name = $article->title.' - Eve Woman';
        return view('amp.article',compact('nav','popular','related_news','getArticleLatest','article','name'));
    }

    public function catArticleId($category,$id) {
        $nav = 'home';
        
        $article = Eve::get_single_article($id);
        if($article == null){
            return redirect('/');
        }
        // dd( $article->title);
        $getArticleLatest = Eve::get_article_latest(6);
        $related_news = Eve::related_articles($article);
        $popular = Cache::remember("popular.articles", now()->addSeconds(1800), function () {
            return Eve::getTrendingArticles(6, "today");
        });
        // dd($popular);
        // $popular = Eve::popular_articles(6);
        $name = $article->title.' - Eve Woman';
        return view('amp.article',compact('nav','popular','related_news','getArticleLatest','article','name'));
    }

    public function article($id,$name)
    {
        
        $nav = 'home';
        $article = Eve::get_single_article($id);
        if($article == null){
            return redirect('/');
        }
        // dd($id,$article);
        $getArticleLatest = Eve::get_article_latest(6);
        $related_news = Eve::related_articles($article);
        $popular = Eve::popular_articles(6);
        $name = $article->title.' - Eve Woman';
        return view('amp.article',compact('nav','popular','related_news','getArticleLatest','article','name'));
    }

    public function articleId($id)
    {
        
        $nav = 'home';
        $article = Eve::get_single_article($id);
        if($article == null){
            return redirect('/');
        }
        // dd($id,$article);
        $getArticleLatest = Eve::get_article_latest(6);
        $related_news = Eve::related_articles($article);
        $popular = Eve::popular_articles(6);
        $name = $article->title.' - Eve Woman';
        return view('amp.article',compact('nav','popular','related_news','getArticleLatest','article','name'));
    }

    public function category($categoryid,$name)
    {
        $nav = 'home';
        $mainCatArticles = Eve::catStories($categoryid, 1,0);
        $sideCatLatest = Eve::catStories($categoryid, 3,1);
        $bigCat = Eve::catStories($categoryid, 6,4);
        $name = ucfirst(str_replace('-','  ',$name)).' - Eve Woman';
        return view('amp.categories',compact('nav','mainCatArticles','sideCatLatest','bigCat','categoryid','name'));
    }

    public function categoryId($categoryid)
    {
        $nav = 'home';
        $mainCatArticles = Eve::catStories($categoryid, 1,0);
        $sideCatLatest = Eve::catStories($categoryid, 3,1);
        $bigCat = Eve::catStories($categoryid, 6,4);
        $name = ucfirst(str_replace('-','  ',$name)).' - Eve Woman';
        return view('amp.categories',compact('nav','mainCatArticles','sideCatLatest','bigCat','categoryid','name'));
    }

    public function living()
    {
        $nav = 'home';
        $categoryid = 273;
        $mainCatArticles = Eve::catStories($categoryid, 1);
        $sideCatLatest = Eve::catStories($categoryid, 4);
        $bigCat = Eve::catStories($categoryid, 6);
        $name = 'Living - Eve Woman';
        return view('amp.categories',compact('nav','mainCatArticles','sideCatLatest','bigCat','categoryid','name'));
    }

    public function fashion()
    {
        $nav = 'home';
        $categoryid = 265;
        $mainCatArticles = Eve::catStories($categoryid, 1);
        $sideCatLatest = Eve::catStories($categoryid, 4);
        $bigCat = Eve::catStories($categoryid, 6);
        $name = 'Fashion and Beauty - Eve Woman';
        return view('amp.categories',compact('nav','mainCatArticles','sideCatLatest','bigCat','categoryid','name'));
    }

    public function relationships()
    {
        $nav = 'home';
        $categoryid = 264;
        $mainCatArticles = Eve::catStories($categoryid, 1);
        $sideCatLatest = Eve::catStories($categoryid, 4);
        $bigCat = Eve::catStories($categoryid, 6);
        $name = 'Relationships - Eve Woman';
        return view('amp.categories',compact('nav','mainCatArticles','sideCatLatest','bigCat','categoryid','name'));
    }

    public function parenting()
    {
        $nav = 'home';
        $categoryid = 262;
        $mainCatArticles = Eve::catStories($categoryid, 1);
        $sideCatLatest = Eve::catStories($categoryid, 4);
        $bigCat = Eve::catStories($categoryid, 6);
        $name = 'Parenting - Eve Woman';
        return view('amp.categories',compact('nav','mainCatArticles','sideCatLatest','bigCat','categoryid','name'));
    }

    public function readers()
    {
        $nav = 'home';
        $categoryid = 350;
        $mainCatArticles = Eve::catStories($categoryid, 1);
        $sideCatLatest = Eve::catStories($categoryid, 4);
        $bigCat = Eve::catStories($categoryid, 6);
        $name = 'Reader Lounge - Eve Woman';
        return view('amp.categories',compact('nav','mainCatArticles','sideCatLatest','bigCat','categoryid','name'));
    }

    public function bridal()
    {
        $nav = 'home';
        $categoryid = 272;
        $mainCatArticles = Eve::catStories($categoryid, 1);
        $sideCatLatest = Eve::catStories($categoryid, 4);
        $bigCat = Eve::catStories($categoryid, 6);
        $name = 'Bridal - Eve Woman';
        return view('amp.categories',compact('nav','mainCatArticles','sideCatLatest','bigCat','categoryid','name'));
    }

    public function topic($categoryid)
    {
        $nav = 'home';
        $mainCatArticles = Eve::getTopic($categoryid,1,0);
        $sideCatLatest = Eve::getTopic($categoryid,3,1);
        $bigCat = Eve::getTopic($categoryid,6,4);
        $name = ucfirst(str_replace("-", " ", $categoryid)).' - Eve Woman';
        return view('amp.topic',compact('nav','mainCatArticles','sideCatLatest','bigCat','categoryid','name'));
    }public function sitemap(Request $request)
    {
        $articles = News::getTodays();
        $sitemap = News::getTodays();
        $categories = News::getCategories();

        // $categories = $categories->top->merge($categories->more);
        //return;
        return response()->view('sitemap',['sitemap' => $sitemap,'articles'=>$articles,'categories'=> $categories])
            ->header('Content-Type', 'text/xml');
    }

    public function googlenews(Request $request)
    {
        $articles = News::getTodays();
        $sitemap = News::getTodays();

        $categories = News::getCategories();
        //return;
        return response()->view('googlenews',['sitemap' => $sitemap,'articles'=>$articles,'categories'=> $categories])
            ->header('Content-Type', 'text/xml');
    }

    public function author($author)
    {
        $nav = 'home';
        $mainCatArticles = Eve::author($author,1,0);
        $sideCatLatest = Eve::author($author,3,1);
        $bigCat = Eve::author($author,6,4);
        $name = ucfirst(str_replace("-", " ", $author)).' - Eve Woman';
        return view('amp.author',compact('nav','mainCatArticles','sideCatLatest','bigCat','author','name'));
    }

    public function authorMore(Request $request)
    {
        $categoryid = $request->categoryid;
        $limit = $request->limit;
        $end = $request->end;
        $keywords = $request->keywords;
        $loaded = Eve::author($categoryid,$limit, $end);
        $display = '';

        if ($loaded != null) {
            foreach ($loaded as $data) {
                $display .= ' <div class="col-lg-4 col-md-4 col-12">
                    
                        <a href="' . url(Str::slug(Eve::getCatName($data->categoryid)).'/article/' . $data->id . '/' . Str::slug($data->title)) . '">
                            <img src="https://cdn.standardmedia.co.ke' . $data->thumbURL . '"
                            class="card-img-top img-fluid mt-3 mb-4" alt="...">
                        </a>
                        
                            <div class="mt-1">
                            <a href="" style="color:#3a3a3a!important">
                                <div class="byline mb-2"> 
                                    <span class="bycolor mb-2">
                                        <a href="' . url('author/' . $data->author_id . '/' . Str::slug($data->author)) . '" style="color:#3a3a3a!important">
                                            ' . $data->author . '
                                        </a>
                                    </span>
                                </div>
                            </a>
                            <h2>
                                <a style="color:#3a3a3a!important" href="' . url('article/' . $data->id . '/' . Str::slug($data->title)) . '">
                                    ' . $data->title . '
                                    </a>
                            </h2>
                        </div>
                    </div>';
            }
        }
        echo $display;
    }

    public function topicMore(Request $request)
    {
        $categoryid = $request->categoryid;
        $limit = $request->limit;
        $end = $request->end;
        $keywords = $request->keywords;
        $loaded = Eve::getTopic($categoryid,$limit, $end);
        $display = '';

        if ($loaded != null) {
            foreach ($loaded as $data) {
                $display .= ' <div class="col-lg-4 col-md-4 col-12">
                    
                        <a href="' . url(Str::slug(Eve::getCatName($data->categoryid)).'/article/' . $data->id . '/' . Str::slug($data->title)) . '">
                            <img src="https://cdn.standardmedia.co.ke' . $data->thumbURL . '"
                            class="card-img-top img-fluid mt-3 mb-4" alt="...">
                        </a>
                        
                            <div class="mt-1">
                            <a href="' . url('author/' . $data->author_id . '/' . Str::slug($data->author)) . '" style="color:#3a3a3a!important">
                                <div class="byline mb-2"> 
                                    <span class="bycolor mb-2">
                                                ' . $data->author . '
                                    </span>
                                </div>
                            </a>
                            <h2>
                                <a style="color:#3a3a3a!important" href="' . url('article/' . $data->id . '/' . Str::slug($data->title)) . '">
                                    ' . $data->title . '
                                    </a>
                            </h2>
                        </div>
                    </div>';
            }
        }
        echo $display;
    }

    public function loadMore(Request $request)
    {
        $categoryid = $request->categoryid;
        // $keywords = $request->keywords;
        $limit = $request->limit;
        $end = $request->end;
        $loaded = Eve::catLoadMoreStories($categoryid, $limit, $end);
        $display = '';

        if (count($loaded) > 0) {
            foreach ($loaded as $alldata) {
            foreach ($alldata as $data) {

                $display .= ' <div class="col-lg-4 col-md-4 col-12">
                        
                           <a href="' . url(Str::slug(Eve::getCatName($data->categoryid)).'/article/' . $data->id . '/' . Str::slug($data->title)) . '">
                               <img src="https://cdn.standardmedia.co.ke' . $data->thumbURL . '"
                                class="card-img-top img-fluid mt-3 mb-4" alt="...">
                           </a>
                            
                             <div class="mt-1">
                                <a href="" style="color:#3a3a3a!important">
                                    <div class="byline mb-2"> 
                                        <span class="bycolor mb-2">
                                            <a href="' . url('author/' . $data->author_id . '/' . Str::slug($data->author)) . '" style="color:#3a3a3a!important">
                                                 ' . $data->author . '
                                            </a>
                                        </span>
                                    </div>
                                </a>
                                <h2>
                                    <a style="color:#3a3a3a!important" href="' . url('article/' . $data->id . '/' . Str::slug($data->title)) . '">
                                     ' . $data->title . '
                                     </a>
                                </h2>
                            </div>
                        </div>';
                }
            }
        }
        echo $display;
    }

    public function videos()
    {
        $nav = 'home';
        $videos = Eve::show_videos();
        $name = 'Videos - Eve Woman';
        return view('amp.videos',compact('nav','videos','name'));
    }

    public function singleVideo($id, $name)
	{
        $nav = 'home';
        $video = Eve::get_video($id);
        $recommended = Eve::get_selected_video_recommends($video);
        $getArticleLatest = Eve::get_article_latest(6);
        // $popular = Eve::popular_articles(6);
        $popular = Cache::remember("popular.articles", now()->addSeconds(1800), function () {
            return Eve::getTrendingArticles(6, "today");
        });
        $name = str_replace('-','  ',$video->title).' - Eve Woman';
        return view('amp.watch_video',compact('nav','video','recommended','getArticleLatest','popular','name'));
    }

    public function about()
    {
        $nav = 'home';
        $name = 'About Us - Eve Woman';
        return view('about',compact('nav','name'));
    }

    public function search() {
        $nav = '';
        $name = 'Search Result - Eve Woman';
        return view('search',compact('nav','name'));
    }

    public function profile(Request $request,$id)
    {
        $url = "https://vas.standardmedia.co.ke/api/profile/".$id;
        $client = new Client(['headers' => [ 'Content-Type' => 'application/json'],'verify'=> false,'http_errors'=>false]);

        try {
            $res = $client->request('POST', $url, [
                'form_params' => [
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'app_id' => 2,
			        'app_secret' => 'DrpRd4DqZsEEH3DW'
                ]]);
            $response = json_decode($res->getBody());
            if(isset($response->message) && $response->message == "User updated successfully"){
                $user = new User(
                    [
                        "id" => $id,
                        "name" => $request->name,
                        "email"=> $request->email,
                        "phone"=> $request->phone,
                    ]
                );

                Session::put('user', $user);
                return redirect('/')->with('success','Profile Successfully updated!');
            }else{
                $user = new User(
                    [
                        "id" => $id,
                        "name" => $request->name,
                        "email"=> $request->email,
                        "phone"=> $request->phone,
                    ]
                );

                Session::put('user', $user);
                return redirect('/')->with('error',$response->message);
            }
        } catch (ClientException $e) {
            dd($e);
            return back()->with('error','An error has occurred....Please try again!');
            // dd($e);
        } catch (ServerException $e) {
            dd($e);
            return back()->with('error','An error has occurred....Please try again!');
            // dd($e);
        }
    }
    public function password(Request $request,$id)
    {
        $url = "https://vas.standardmedia.co.ke/api/profile/".$id;
        $client = new Client(['headers' => [ 'Content-Type' => 'application/json'],'verify'=> false,'http_errors'=>false]);

        try {
            $res = $client->request('POST', $url, [
                'form_params' => [
                    'email' => $request->email,
                    'current_password' => $request->current_password,
                    'password' => $request->password,
                    'password_confirmation' => $request->password_confirmation,
                    'app_id' => 2,
			        'app_secret' => 'DrpRd4DqZsEEH3DW'
                ]]);
            $response = json_decode($res->getBody());
            if(isset($response->message) && $response->message == "User updated successfully"){
                return redirect('/')->with('success','Password Successfully updated!');
            }else{
                return redirect('/')->with('error',$response->message);
            }
        } catch (ClientException $e) {
            dd($e);
            return back()->with('error','An error has occurred....Please try again!');
            // dd($e);
        } catch (ServerException $e) {
            dd($e);
            return back()->with('error','An error has occurred....Please try again!');
            // dd($e);
        }
    }

    public function subscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);
        if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator)->withInput();
        }
        $url = "https://mail.standarddigitalworld.co.ke/api/subscription";
        $client = new Client(['headers' => 
                    [ 
                        'Content-Type' => 'application/json',
                        'appkey' => 'VHV1VElESjRMNklTYnNubGJlZVY0WkRieVNiZU9vendsSm5mRU56NDdYR2w2WXBESEF2UnVQWU9MdXhm5eaa84f9ba4bc'
                    ], 
                    'verify'=> false,
                    'http_errors'=>false
                    ]);

        try {
            $res = $client->request('POST', $url, [
                'form_params' => [
                    'email' => $request->email,
                    'category_id' => $request->category_id,
                ]]);
            $response = json_decode($res->getBody());
            if(isset($response->error)){
                return redirect('/')->with('error',$response->error);
            }else{
                return redirect('/')->with('success','A subscription email has been sent to your email....Please confirm to subscribe to this Newsletter');
            }
        } catch (ClientException $e) {
            dd($e);
            return back()->with('error','An error has occurred....Please try again!');
            // dd($e);
        } catch (ServerException $e) {
            dd($e);
            return back()->with('error','An error has occurred....Please try again!');
            // dd($e);
        }
    }
}
