<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use DateTime;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

use GuzzleHttp\Client;

class Eve extends Model
{
    //
    public static function getNav()
    {  
        $dbh = DB::table('std_category')->where("site", "evewoman")
                ->whereNull("inactive")
                ->whereIn("id",array(266, 416, 272, 262, 278, 307, 350))
                ->orderBy("id", "desc")
                ->orderBy("listorder","asc")
                ->get();
        if($dbh)
        {
            $x = $dbh;
            return $x;
        }
    }

    public static function getCategories()
    {  
        $dbh = DB::table('std_category')->where("site", "farmers")
                ->whereNull("inactive")
                ->whereIn("id",array(266, 416, 272, 262, 278, 307, 350, 273, 267, 265, 264))
                ->orderBy("id", "desc")
                ->orderBy("listorder","asc")
                ->get();
        if($dbh)
        {
            $x = $dbh;
            return $x;
        }
    }

    public static function getTodays()
    {

        // $categories = DB::table('std_article')->where('id',6)->orWhere('parentid',6)->whereNull('inactive')->get(['id'])->pluck('id')->toArray();
        $dbh = DB::table('std_article')
                ->where("source","evewoman")
                ->where("publishday",date("Y-m-d"))
                ->whereNull('inactive')
                ->orderBy('publishday','DESC')
                ->get();
        return $dbh;
        
        // Article::on('mysql')
        //     ->orderBy('publishday','DESC')
        //     ->where('source','main')
        //     ->whereNull('inactive')
        //     ->whereIn('categoryid',$categories)
        //     ->where('publishday',date('Y-m-d'))->get(['id','categoryid','title','thumbURL','summary','author','keywords','publishday']);
    }

    public static function getAllNav()
    {  
        $dbh = DB::table('std_category')->where("site", "evewoman")
                ->whereNull("inactive")
                ->whereIn("id",array(266, 416, 272, 262, 278, 307, 350, 273, 267, 265, 264))
                ->orderBy("id", "desc")
                ->orderBy("listorder","asc")
                ->get();
        if($dbh)
        {
            $x = $dbh;
            return $x;
        }
    }

    public static function getLimitNav()
    {
        $dbh = DB::table('std_category')->where("site", "evewoman")
                ->whereNull("inactive")
                ->whereIn("id",array(273, 267, 265, 264))
                ->orderBy("id", "desc")
                ->orderBy("listorder","asc")
                ->limit(3)
                ->get();
        // $dbh = DB::select("select name,id from std_category where site='evewoman' and inactive is null and parentid=0 order by listorder asc limit 3");
        if($dbh)
        {
            $x = $dbh;
            return $x;
        }

    }

    public static function getCatName($id)
    {
        // dd($id);
        //$dbh = DB::select('select * from std_category where site="evewoman" and refer_id=' . $id)->first();
        $dbh = DB::table('std_category')
                // ->where("site", "evewoman")
                ->where("refer_id",$id)
                ->first();
        if ($dbh != null) {
            return $dbh != null ? $dbh->name : 'The Standard';
        } else {
            //$dbh = DB::select('select * from std_category where site="evewoman" and refer_id=0 and id=' . $id)->first();
            $dbh = DB::table('std_category')
                    // ->where("site", "evewoman")
                    ->where("id",$id)
                    ->first();
            return $dbh != null ? $dbh->name : 'The Standard';
        }
    }

    public function get_navigation()
    {
        $dbh = $this->db->where("site", "evewoman")
            ->where("inactive is null")
            ->where("(id = '273' OR id = '262' OR id = '265' OR id = '264' OR id = '350')", NULL, FALSE)
            ->order_by("id", "desc")
            ->get('std_category');
        return $dbh->result_array();
    }

    function register_user($data)
    {
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }

    public function login_user($email, $pass)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email', $email);
        $this->db->where('password', $pass);

        if ($query = $this->db->get()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public static function getMainArticle($slug = FALSE)
    {
        if ($slug === FALSE) {

            $now = date("Y-m-d H:i:s");

            $mainArticle = DB::table('std_article')->where("source", "evewoman")
                ->where("publishdate", "<=", $now)
                ->whereNull("std_article.inactive")
                ->orderBy("publishday", "DESC")
                ->orderBy("homepagelistorder", "asc")
                ->orderBy("listorder", "asc")
                ->limit(1)
                ->first();

            return $mainArticle;
        }

    }

    public static function getLatest($limit, $offet)
    {
        $publishdate = date('Y-m-d H:i:s');

        $dbh = DB::table('std_article')->whereNull("std_article.inactive")
            ->where("publishdate","<=",$publishdate)
            ->where("source", 'evewoman')
            ->limit($limit)
            ->offset($offet)
            ->orderBy("publishday", "DESC")
            ->orderBy("homepagelistorder", "asc")
            ->orderBy("listorder", "asc")
            ->get();

        return $dbh;
    }

    public static function get_article_latest($limit)
    {
        $publishdate = date('Y-m-d H:i:s');

        $dbh = DB::table('std_article')->whereNull("std_article.inactive")
            ->where("publishdate","<=",$publishdate)
            ->where("source", 'evewoman')
            ->limit($limit)
            ->orderBy("publishday", "DESC")
            ->orderBy("homepagelistorder", "asc")
            ->orderBy("listorder", "asc")
            ->get();

        return $dbh;
    }

    public static function getFashionAndBeautyLatest($limit)
    {
        $publishdate = date('Y-m-d H:i:s');
        $dbh = DB::table('std_article')->whereNull("std_article.inactive")
            ->where("publishdate","<=",$publishdate)
            ->where("source", 'evewoman')
            ->limit($limit)
            ->orderBy("publishday", "DESC")
            ->orderBy("homepagelistorder", "asc")
            ->orderBy("listorder", "asc")
            ->get();

        return $dbh;
    }

    public static function getTopic($name, $limit, $start = 0){
        $topic 	= str_replace("-", " ", $name);
        $articles	= DB::table("std_article")
                        ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid", "std_article.source", "std_article.publishdate", "std_article.author", "std_article.author_id", "std_article.summary","std_article.keywords")
                        ->whereNull("inactive")
                        ->where("keywords", "like", "%" .$topic . "%")
                        ->where("source","evewoman")
                        ->orderBy("publishdate","DESC")
                        ->limit($limit)
                        ->offset($start)
                        ->get();

        if ($articles){
            return $articles;
        }
    }

    public static function getRelationshipArticle($limit)
    {
        $publishdate = date('Y-m-d H:i:s');
        $dbh = DB::table('std_article')->whereNull("std_article.inactive")
            ->where("publishdate","<=",$publishdate)
            ->where("source", 'evewoman')
            ->where("categoryid", '3')
            ->limit($limit)
            ->orderBy("publishday", "DESC")
            ->orderBy("homepagelistorder", "asc")
            ->orderBy("listorder", "asc")
            ->get();

        return $dbh;
    }

    public static function getBigLatest($limit, $offet)
    {
        $publishdate = date('Y-m-d H:i:s');

        $dbh = DB::table('std_article')->select("std_article.*", "std_category.name as category")
            ->join("std_category", "std_category.refer_id","=","std_article.categoryid")
            ->whereNull("std_article.inactive")
            ->where("publishdate","<=",$publishdate)
            ->where("source", 'evewoman')
            ->limit($limit)
            ->offset($offet)
            ->orderBy("publishday", "DESC")
            ->orderBy("homepagelistorder", "asc")
            ->orderBy("listorder", "asc")
            ->groupBy("std_article.id")
            ->get();
        return $dbh;
    }

    public static function getHomeVideo($limit, $offset)
    {
        $dbh = DB::table('ktn_video')->where("ktn_video_category.videotypeid", 6)
            ->whereNull("ktn_video.inactive")
            ->whereNull("ktn_video_category.inactive")
            ->orderBy("ktn_video.publishdate", "desc")
            ->join('ktn_video_category', 'ktn_video.categoryid', '=', 'ktn_video_category.id')
            ->limit($limit)
            ->offset($offset)
            ->first();

        return $dbh;
    }

    public function get_category_articles($uid)
    {
        $publishdate = date('Y-m-d H:i:s');

        $dbh = $this->db->select("std_article.*, std_category.name as category")
            ->join("std_category", "std_category.refer_id=std_article.categoryid", 'left')
            ->where("std_article.inactive is null")
            ->where("publishdate<='$publishdate'")
            ->where("source", 'evewoman')
            ->where('categoryid', $uid)
            ->where("(categoryid ='$uid' OR categoryid='refer_id')", NULL, FALSE)
            ->limit(1)
            ->order_by("publishday", "DESC")
            ->order_by("homepagelistorder", "asc")
            ->order_by("listorder", "asc")
            ->get("std_article");

        return $dbh->result_array();

    }

    public static function confusedCategory($categoryid)
    {
        $dbh = DB::table('std_category')->where('refer_id', 0)
                ->where('parentid', 0)
                ->where('site', 'evewoman')
                ->whereNull('inactive')
                ->where('id', $categoryid)
                ->get();
        if (count($dbh) > 0) {
            return true;
        } else {
            return false;
        }
    }


    public static function get_single_article($id = FALSE, $title = FALSE)
    {
        $id  =  (int)$id;
        $article = DB::table('std_article')
                        ->where("source", 'evewoman')
                        ->where("id", $id)
                        ->whereNull("inactive")
                        ->first();
        if($article) {
                return $article;
            } else {
                $article = DB::connection('mysql3')->table('std_article')
                    ->where("id", $id)
                    ->where("source", 'evewoman')
                    ->whereNull("inactive")
                    ->first();
                if ($article) {
                    return $article;
                } else {
                    $article = DB::connection('mysql2')->table('std_article')
                        ->where("id", $id)
                        ->where("source", 'evewoman')
                        ->whereNull("inactive")
                        ->first();
                    if ($article) {
                        return $article;
                    } else {
                        return [];
                    }
                }
            }
    }

    public static function related_articles($key, $limit = 4)
    {
        if ($key !== null && $key->keywords != NULL) {
            $data = str_replace(".", ",", $key->keywords);
            $data = str_replace(":", ",", $data);
            $data = str_replace(";", ",", $data);
            $keywords = explode(',', str_replace("'", "", $data));
            $keywords = array_diff($keywords, array(" "));
            if (is_array($keywords) && (!empty($keywords))) {
                $sql = "select * from std_article where keywords is not null and publishdate<='" . date("Y-m-d H:i:s") . "' and inactive is null and id!='" . $key->id . "' and source='evewoman' and (";
                $sqlt = "";
                foreach ($keywords as $keyword) {
                    $sqlt .= "title like '%" . $keyword . "%' or keywords like '%" . $keyword . "%' or ";
                }
                $sql .= substr($sqlt, 0, -3);

                $sql .= ") order by publishdate desc limit " . $limit . "";
                $dbh = DB::select($sql);
                if (count($dbh) > 0) {
                    return $dbh;
                } else {
                    return [];
                }
            } else {
                return [];
            }
        }else{
            return [];
        }
    }

    public static function time_difference($datetime, $full = false)
    {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public static function hasChild($id)
    {
        $x = array('child' => 0);
        $id = static::getRealCatId($id);
        $dbh = DB::table('std_category')->where('parentid', $id)
            ->where('refer_id','!=','0')
            ->where('site', 'evewoman')
            ->get();
        if (count($dbh) > 0) {
            $x = array('child' => 1, 'result' => $dbh);
        }
        return (object)$x;
    }


    public function getOtherCatId($categoryid)
    {
        $this->db->where('refer_id', $categoryid)
            ->where('refer_id!=0')
            ->where('site', 'evewoman');
        $dbh = $this->db->get('std_category');
        if ($dbh) {
            if ($dbh->num_rows() > 0) {
                return $dbh->row()->id;
            } else {
                return $categoryid;
            }
        }
    }

    public static function getRealCatId($categoryid)
    {
        $dbh = DB::table('std_category')->where('id', $categoryid)
                ->where('refer_id','!=','0')
                ->whereIn('site', array('evewoman', 'main'))
                ->first();
        if ($dbh != null) {
            return $dbh->refer_id;
        } else {
            return $categoryid;
        }
    }

    public static function catStories($categoryid,$limit=7,$offset=0)
      {
        //first database
        $data = array();
        if (static::confusedCategory($categoryid)) {
            $dbh = DB::table('std_article')->where('categoryid', $categoryid);
        } else {
            $categoryid = static::getRealCatId($categoryid);

            $children[] = $categoryid;
            if (static::hasChild($categoryid)->child == 0) {
                $dbh = DB::table('std_article')->where('categoryid', $categoryid);
            } else {
                foreach (static::hasChild($categoryid)->result as $value) {
                    $children[] = $value->refer_id;
                }
                $dbh = DB::table('std_article')->whereIn('categoryid', $children);
            }
        }
        $results = $dbh->whereNull('inactive')
            ->where('source', 'evewoman')
            ->orderBy("publishday", "desc")
            ->orderBy("listorder", "asc")
            ->orderBy("homepagelistorder", "asc")
            ->limit($limit)
            ->offset($offset)
            ->get();
        if(count($results) > 0){
            array_push($data,$results);
        } 
        if(count($results) < $limit)
          {
            $limit=(int)(count($results));
            //second Db
            // array_push($data,$results);
            
            if(static::confusedCategory($categoryid))
              {
                $dbh2 = DB::connection('mysql3')->table('std_article')->where('categoryid',$categoryid);
              }
            else
              {
                $categoryid=static::getRealCatId($categoryid);
                
                $children[]=$categoryid;
                if(static::hasChild($categoryid)->child==0)
                  {
                    $dbh2 = DB::connection('mysql3')->table('std_article')->where('categoryid',$categoryid);
                  }
                else
                  {
                    foreach($this->hasChild($categoryid)->result as $value)
                      {
                        $children[]=$value->refer_id;
                      }            
                      $dbh2 = DB::connection('mysql2')->table('std_article')->whereIn('categoryid',$children);
                  }
              }
              $results = $dbh2->whereNull('inactive')
                     ->where('source','evewoman')
                     ->orderBy("publishday","desc")
                     ->orderBy("listorder","asc")
                     ->orderBy("homepagelistorder","asc")
                     ->limit($limit)
                     ->offset($offset)
                     ->get();
            if(count($results) > 0){
                array_push($data,$results);
            } 
            if(count($results) < $limit)
              {
                $limit=(int)(count($results));
                //third db
                // array_push($data,$results);
                if(static::confusedCategory($categoryid))
                  {
                    $dbh3 = DB::connection('mysql2')->table('std_article')->where('categoryid',$categoryid);
                  }
                else
                  {
                    $categoryid=static::getRealCatId($categoryid);
                    
                    $children[]=$categoryid;
                    if(static::hasChild($categoryid)->child==0)
                      {
                        $dbh3 = DB::connection('mysql3')->table('std_article')->where('categoryid',$categoryid);
                      }
                    else
                      {
                        foreach(static::hasChild($categoryid)->result as $value)
                          {
                            $children[]=$value->refer_id;
                          }            
                        $dbh3 = DB::connection('mysql2')->table('std_article')->whereIn('categoryid',$children);
                      }
                  }
                  $results = $dbh3->whereNull('inactive')
                         ->where('source','evewoman')
                         ->orderBy("publishday","desc")
                         ->orderBy("listorder","asc")
                         ->orderBy("homepagelistorder","asc")
                         ->limit($limit)
                         ->offset($offset)
                         ->get();
                if(count($results) > 0){
                    array_push($data,$results);
                } 
                if(count($results) < $limit)
                  {
                    $limit=(int)(count($results));
                    return $data;

                  }
              }
            else
              {
                return $data;
              }         

          }
        else
          {
            return $data;
          }         
      }

    public static function catStories1($categoryid, $limit)
    {
        //first database
        if (static::confusedCategory($categoryid)) {
            $dbh = DB::table('std_article')->where('categoryid', $categoryid);
        } else {
            $categoryid = static::getRealCatId($categoryid);

            $children[] = $categoryid;
            if (static::hasChild($categoryid)->child == 0) {
                $dbh = DB::table('std_article')->where('categoryid', $categoryid);
            } else {
                foreach (static::hasChild($categoryid)->result as $value) {
                    $children[] = $value->refer_id;
                }
                $dbh = DB::table('std_article')->whereIn('categoryid', $children);
            }
        }
        $results = $dbh->whereNull('inactive')
            ->where('source', 'evewoman')
            ->orderBy("publishday", "desc")
            ->orderBy("listorder", "asc")
            ->orderBy("homepagelistorder", "asc")
            ->limit($limit)
            ->get();
        //var_dump($this->db->last_query());
        //die();

        return $results;
    }

    public static function catLoadMoreStories($categoryid, $limit, $offset)
    {

        $data = array();
        if (static::confusedCategory($categoryid)) {
            $dbh = DB::table('std_article')->where('categoryid', $categoryid);
        } else {
            $categoryid = static::getRealCatId($categoryid);

            $children[] = $categoryid;
            if (static::hasChild($categoryid)->child == 0) {
                $dbh = DB::table('std_article')->where('categoryid', $categoryid);
            } else {
                foreach (static::hasChild($categoryid)->result as $value) {
                    $children[] = $value->refer_id;
                }
                $dbh = DB::table('std_article')->whereIn('categoryid', $children);
            }
        }
        $results = $dbh->whereNull('inactive')
            ->where('source', 'evewoman')
            ->orderBy("publishday", "desc")
            ->orderBy("listorder", "asc")
            ->orderBy("homepagelistorder", "asc")
            ->limit($limit)
            ->offset($offset)
            ->get();
        if(count($results) > 0){
            array_push($data,$results);
        } 
        if(count($results) < $limit)
          {
            $limit=(int)(count($results));
            //second Db
            // array_push($data,$results);
            if(static::confusedCategory($categoryid))
              {
                $dbh2 = DB::connection('mysql2')->table('std_article')->where('categoryid',$categoryid);
              }
            else
              {
                $categoryid=static::getRealCatId($categoryid);
                
                $children[]=$categoryid;
                if(static::hasChild($categoryid)->child==0)
                  {
                    $dbh2 = DB::connection('mysql2')->table('std_article')->where('categoryid',$categoryid);
                  }
                else
                  {
                    foreach(static::hasChild($categoryid)->result as $value)
                      {
                        $children[]=$value->refer_id;
                      }            
                      $dbh2 = DB::connection('mysql2')->table('std_article')->whereIn('categoryid',$children);
                  }
              }
              $results = $dbh2->whereNull('inactive')
                     ->where('source','evewoman')
                     ->orderBy("publishday","desc")
                     ->orderBy("listorder","asc")
                     ->orderBy("homepagelistorder","asc")
                     ->limit($limit)
                     ->offset($offset)
                     ->get();
            if(count($results) > 0){
                array_push($data,$results);
            } 
            if(count($results) < $limit)
              {
                $limit=(int)(count($results));
                //third db
                // array_push($data,$results);
                if(static::confusedCategory($categoryid))
                  {
                    $dbh3 = DB::connection('mysql3')->table('std_article')->where('categoryid',$categoryid);
                  }
                else
                  {
                    $categoryid=static::getRealCatId($categoryid);
                    
                    $children[]=$categoryid;
                    if(static::hasChild($categoryid)->child==0)
                      {
                        $dbh3 = DB::connection('mysql3')->table('std_article')->where('categoryid',$categoryid);
                      }
                    else
                      {
                        foreach(static::hasChild($categoryid)->result as $value)
                          {
                            $children[]=$value->refer_id;
                          }            
                        $dbh3 = DB::connection('mysql3')->table('std_article')->whereIn('categoryid',$children);
                      }
                  }
                  $results = $dbh3->whereNull('inactive')
                         ->where('source','evewoman')
                         ->orderBy("publishday","desc")
                         ->orderBy("listorder","asc")
                         ->orderBy("homepagelistorder","asc")
                         ->limit($limit)
                         ->offset($offset)
                         ->get();
                if(count($results) > 0){
                    array_push($data,$results);
                } 
                if(count($results) < $limit)
                  {
                    $limit=(int)(count($results));
                    return $data;

                  }
              }
            else
              {
                return $data;
              }         

          }
        else
          {
            return $data;
          }       
    }

    public static function show_videos($limit=32)
    {
        $data = array();
        $dbh=DB::table('ktn_video')->select('ktn_video.title','ktn_video.videoURL','ktn_video.id','ktn_video.publishdate','ktn_video_category.name')
                    ->join("ktn_video_category","ktn_video.categoryid","=","ktn_video_category.id")
                    ->join("ktn_video_type","ktn_video_category.videotypeid","=","ktn_video_type.id")
                    ->where("ktn_video_type.source","evewoman")
                    ->whereNull("ktn_video.inactive")
                    ->orderBy('ktn_video.publishdate','DESC')
                    ->orderBy('ktn_video.id', 'desc')
                    ->paginate($limit);
                    
        $x=count($dbh);
        array_push($data,$dbh);
        $dbh=NULL;
        if($limit === $x)
            {
                return $data;
            }
        else
            {
                $limit -= $x;
                $dbh = DB::connection('mysql2')->table('ktn_video')->select('ktn_video.title','ktn_video.videoURL','ktn_video.id','ktn_video.publishdate','ktn_video_category.name')
                    ->join("ktn_video_category","ktn_video.categoryid","=","ktn_video_category.id")
                    ->join("ktn_video_type","ktn_video_category.videotypeid","=","ktn_video_type.id")
                    ->where("ktn_video_type.source","evewoman")
                    ->whereNull("ktn_video.inactive")
                    ->orderBy('ktn_video.publishdate','DESC')
                    ->orderBy('ktn_video.id', 'desc')
                    ->paginate($limit);
                    
                $x=count($dbh);
                array_push($data,$dbh);
                $dbh=NULL;
                if($limit === $x)
                    {
                        return $data;
                    }
                else
                    {
                        $limit -= $x;
                        $dbh = DB::connection('mysql3')->table('ktn_video')->select('ktn_video.title','ktn_video.videoURL','ktn_video.id','ktn_video.publishdate','ktn_video_category.name')
                                    ->join("ktn_video_category","ktn_video.categoryid","=","ktn_video_category.id")
                                    ->join("ktn_video_type","ktn_video_category.videotypeid","=","ktn_video_type.id")
                                    ->where("ktn_video_type.source","evewoman")
                                    ->whereNull("ktn_video.inactive")
                                    ->orderBy('ktn_video.publishdate','DESC')
                                    ->orderBy('ktn_video.id', 'desc')
                                    ->paginate($limit);
                                    array_push($data,$dbh);
                        return $data;
                    }
            }

    }

    public static function keyword_search($keyword,$limit,$start=0)
	        {
                $data = array();
                $dbh =	DB::table('std_article')->whereNull("inactive")
	                     ->where('source','evewoman')
	                     ->where("publishdate","<=",date("Y-m-d H:i:s"))
	                     ->where('Keywords','like','%'.$keyword.'%')
                         ->limit($limit,$start)
                         ->get();
	            $x 		= 	count($dbh);
	            array_push($data,$dbh);
	            if($limit === $x)
	              	{
	                	return $data;
	              	}
	            else
	            	{
	            		$limit -=	$x;
	            		$dbh=DB::connection('mysql2')->table('std_article')->whereNull("inactive")
                                    ->where('source','evewoman')
                                    ->where("publishdate","<=",date("Y-m-d H:i:s"))
                                    ->where('Keywords','like','%'.$keyword.'%')
                                    ->limit($limit,$start)
                                  ->get();
                        $x = count($dbh);
	            		array_push($data,$dbh);
			            if($limit === $x)
			              	{
			                	return $data;
			              	}
			            else
			            	{
			            		$dbh=DB::connection('mysql2')->table('std_article')->whereNull("inactive")
                                        ->where('source','evewoman')
                                        ->where("publishdate","<=",date("Y-m-d H:i:s"))
                                        ->where('Keywords','like','%'.$keyword.'%')
                                        ->limit($limit,$start)
                                         ->limit($limit,$start)
                                         ->get();
                                array_push($data,$dbh);
					           	return $data;
			            	}
	            	}
	        }

    public function get_latest_videos($limit,$start=0)
         	{
            	$this->db->select('v.title, v.videoURL, v.id,c.name ')
            			->from("ktn_video as v, ktn_video_category as c, ktn_video_type as t")
            			->where("v.categoryid=c.id AND c.videotypeid=t.id and t.source='evewoman' AND v.`inactive` IS   NULL")
            			->order_by('v.publishdate','DESC')
            			->order_by( 'v.id', 'desc')
            			->limit($limit,$start);
            	$dbh=$this->db->get();
            	$data=$dbh->result();
            	if($x=$dbh->num_rows()>0)
	            	{

					 $this->db1 = $this->load->database("seconddb",TRUE);
	            		if($x==$limit)
		            		{
		            			return $data;
		            		}
		            	else
		            		{

		            			$limit=$limit-$x;
		            			$this->db1->select('v.title, v.videoURL, v.id,c.name ')
				            			->from("ktn_video as v, ktn_video_category as c, ktn_video_type as t")
				            			->where("v.categoryid=c.id AND c.videotypeid=t.id and t.source='evewoman' AND v.`inactive` IS   NULL")
				            			->order_by('v.publishdate','DESC')
				            			->order_by( 'v.id', 'desc')
				            			->limit($limit,$start);
				            	$dbh=$this->db1->get();
				            	$data = array_merge($data,$dbh->result());
				            	if($x=$dbh->num_rows()	=== $limit)
				            		{
				            			return $data;
				            		}
				            	else
				            		{
										$this->db2 = $this->load->database("thirddb",TRUE);
				            			$limit=$limit-$x;
				            			$this->db2->select('v.title, v.videoURL, v.id,c.name ')
						            			->from("ktn_video as v, ktn_video_category as c, ktn_video_type as t")
						            			->where("v.categoryid=c.id AND c.videotypeid=t.id and t.source='evewoman' AND v.`inactive` IS   NULL")
						            			->order_by('v.publishdate','DESC')
						            			->order_by( 'v.id', 'desc')
						            			->limit($limit,$start);
						            	$dbh=$this->db2->get();
						            	$data = array_merge($data,$dbh->result());
				            		}
		            		}
	            	}
	            else
	            	{

          		$this->db1 = $this->load->database("seconddb",TRUE);
	            		$this->db1->select('v.title, v.videoURL, v.id,c.name ')
		            			->from("ktn_video as v, ktn_video_category as c, ktn_video_type as t")
		            			->where("v.categoryid=c.id AND c.videotypeid=t.id and t.source='evewoman' AND v.`inactive` IS   NULL")
		            			->order_by('v.publishdate','DESC')
		            			->order_by( 'v.id', 'desc')
		            			->limit($limit,$start);
		            	$dbh=$this->db1->get();
		            	$data=$dbh->result();
		            	if($x=$dbh->num_rows()>0)
			            	{
			            		if($x==$limit)
				            		{
				            			return $data;
				            		}
				            	else
				            		{
										$this->db2 = $this->load->database("thirddb",TRUE);
				            			$limit=$limit-$x;
				            			$this->db2->select('v.title, v.videoURL, v.id,c.name ')
						            			->from("ktn_video as v, ktn_video_category as c, ktn_video_type as t")
						            			->where("v.categoryid=c.id AND c.videotypeid=t.id and t.source='evewoman' AND v.`inactive` IS   NULL")
						            			->order_by('v.publishdate','DESC')
						            			->order_by( 'v.id', 'desc')
						            			->limit($limit,$start);
						            	$dbh=$this->db2->get();
						            	$data = array_merge($data,$dbh->result());
						            	return $data;
				            		}
			            	}
			            else
			            	{
								$this->db2 = $this->load->database("thirddb",TRUE);
			            		$this->db2->select('v.title, v.videoURL, v.id,c.name ')
				            			->from("ktn_video as v, ktn_video_category as c, ktn_video_type as t")
				            			->where("v.categoryid=c.id AND c.videotypeid=t.id and t.source='evewoman' AND v.`inactive` IS   NULL")
				            			->order_by('v.publishdate','DESC')
				            			->order_by( 'v.id', 'desc')
				            			->limit($limit,$start);
				            	$dbh=$this->db2->get();
				            	return $dbh->result();
			            	}
	            	}

         	}
      	public static function get_video($id)
         	{

            	$dbh=DB::table('ktn_video')
            				  ->where('ktn_video.id',$id)
            				  ->first();

            	if(!is_null($dbh))
                {
                    return $dbh;
                }
            	else
              	{

                    $dbh=DB::connection('mysql2')->table('ktn_video')
                    ->where('ktn_video.id',$id)
                    ->first();
                    if(!is_null($dbh))
                    {
                        return $dbh;
                    }
                    else
                    {
                        $dbh=DB::connection('mysql3')->table('ktn_video')
                        ->where('ktn_video.id',$id)
                        ->first();
                        if(!is_null($dbh))
                        {
                            return $dbh;
                        }
                        else
                        {
                            return [];
                        }
                    }
                }
         	}
      	public function get_related_video($dat,$limit=12)
         	{
            	$this->db->select('v.title, v.videoURL, v.id,v.publishdate,c.name ');
            	$this->db->from("ktn_video as v, ktn_video_category as c, ktn_video_type as t");
            	$this->db->where("v.categoryid=c.id AND c.videotypeid=t.id and t.source='evewoman' AND v.`inactive` IS   NULL");
            	$this->db->order_by('v.publishdate','DESC');
            	$this->db->order_by( 'v.id', 'desc');
            	$this->db->limit($limit);
            	$dbh=$this->db->get();
            	return $dbh->result();
         	}


    public static function getVideos()
    {
        $dbh = DB::table('ktn_video')
                ->where("ktn_video_category.videotypeid", 6)
                ->whereNull("ktn_video.inactive")
                ->whereNull("ktn_video_category.inactive")
                ->where('ktn_video.keywords', 'not like', "%pmva%")
                ->orderBy("ktn_video.publishdate", "desc")
                ->orderBy("ktn_video.listorder", "asc")
                ->join('ktn_video_category', 'ktn_video.categoryid','=','ktn_video_category.id')
                ->select("ktn_video.*")
                ->paginate(20);

        return $dbh;

        $sql = "SELECT v.* from ktn_video as v, ktn_video_category as c, ktn_video_type as t
            where v.categoryid=c.id and c.videotypeid=t.id and c.videotypeid=6 and v.inactive is null and v.keywords not like '%pmva%'
            order by v.publishdate desc, v.listorder asc limit $start, $per_page";
        $result = $this->db->query($sql);

		// var_dump($this->db->last_query());
        // die();

        return $result->result_array();

        return $dbh->result_array();
    }

    public static function get_selected_video($id) {
        
        $dbh = DB::table('ktn_video')->where("id",$id)
                ->first();
        return $dbh;
    }

    public static function get_selected_video_recommends($id,$limit=15) {
        $data = array();
        $dbh=DB::table('ktn_video')->select('ktn_video.title','ktn_video.videoURL','ktn_video.id','ktn_video.publishdate','ktn_video_category.name')
                    ->join("ktn_video_category","ktn_video.categoryid","=","ktn_video_category.id")
                    ->join("ktn_video_type","ktn_video_category.videotypeid","=","ktn_video_type.id")
                    ->where("ktn_video_type.source","evewoman")
                    ->whereNull("ktn_video.inactive")
                    ->orderBy('ktn_video.publishdate','DESC')
                    ->orderBy('ktn_video.id', 'desc')
                    ->where('ktn_video.id','!=',$id)
                    ->paginate($limit);
                    
        $x=count($dbh);
        array_push($data,$dbh);
        $dbh=NULL;
        if($limit === $x)
            {
                return $data;
            }
        else
            {
                $limit -= $x;
                $dbh = DB::connection('mysql2')->table('ktn_video')->select('ktn_video.title','ktn_video.videoURL','ktn_video.id','ktn_video.publishdate','ktn_video_category.name')
                    ->join("ktn_video_category","ktn_video.categoryid","=","ktn_video_category.id")
                    ->join("ktn_video_type","ktn_video_category.videotypeid","=","ktn_video_type.id")
                    ->where("ktn_video_type.source","evewoman")
                    ->whereNull("ktn_video.inactive")
                    ->orderBy('ktn_video.publishdate','DESC')
                    ->orderBy('ktn_video.id', 'desc')
                    ->where('ktn_video.id','!=',$id)
                    ->paginate($limit);
                    
                $x=count($dbh);
                array_push($data,$dbh);
                $dbh=NULL;
                if($limit === $x)
                    {
                        return $data;
                    }
                else
                    {
                        $limit -= $x;
                        $dbh = DB::connection('mysql3')->table('ktn_video')->select('ktn_video.title','ktn_video.videoURL','ktn_video.id','ktn_video.publishdate','ktn_video_category.name')
                                    ->join("ktn_video_category","ktn_video.categoryid","=","ktn_video_category.id")
                                    ->join("ktn_video_type","ktn_video_category.videotypeid","=","ktn_video_type.id")
                                    ->where("ktn_video_type.source","evewoman")
                                    ->whereNull("ktn_video.inactive")
                                    ->orderBy('ktn_video.publishdate','DESC')
                                    ->orderBy('ktn_video.id', 'desc')
                                    ->where('ktn_video.id','!=',$id)
                                    ->paginate($limit);
                                    array_push($data,$dbh);
                        return $data;
                    }
            }
    }

    public static function get_selected_video_recommends1($key,$limit=15) {
        $recommended = array();
        if($key->keywords != NULL)
        {
            $data = str_replace(".", ",", $key->keywords);
            $data = str_replace(":", ",", $data);
            $data = str_replace(";", ",", $data);
            $keywords = explode(',', str_replace("'", "", $data));
            $keywords=array_diff($keywords,array(" "));
            
            if(is_array($keywords) && (!empty($keywords)))
            {
                $sql="select v.* from ktn_video as v, ktn_video_category as c, ktn_video_type as t where v.categoryid=c.id and c.videotypeid=t.id and c.videotypeid=6 and v.keywords is not null and v.publishdate<='".date("Y-m-d H:i:s")."' and v.inactive is null and v.id != '".$key->id."' and (";
                $sqlt="";
                foreach ($keywords as $keyword) 
                {
                    $sqlt.="v.title like '%".$keyword."%' or v.keywords like '%".$keyword."%' or ";
                }
                $sql.=substr($sqlt,0,-3);

                $sql.=") order by v.publishdate desc limit ".$limit."";
                $dbh =  DB::select($sql);
                
                array_push($recommended,$dbh);
                $x=count($dbh);
                $dbh=NULL;
                if($limit === $x)
                {
                    return $recommended;
                }
                else
                {
                    $limit -= $x;

                    $sql="select v.* from ktn_video as v, ktn_video_category as c, ktn_video_type as t where v.categoryid=c.id and c.videotypeid=t.id and c.videotypeid=6 and v.keywords is not null and v.publishdate<='".date("Y-m-d H:i:s")."' and v.inactive is null and v.id != '".$key->id."' and (";
                    $sqlt="";
                    foreach ($keywords as $keyword) 
                    {
                        $sqlt.="v.title like '%".$keyword."%' or v.keywords like '%".$keyword."%' or ";
                    }
                    $sql.=substr($sqlt,0,-3);

                    $sql.=") order by v.publishdate desc limit ".$limit."";
                    $dbh =  DB::connection('mysql2')->select($sql);
                    
                    $x=count($dbh);
                    array_push($recommended,$dbh);
                    $dbh=NULL;
                    if($limit === $x)
                    {
                        return $recommended;
                    }
                    else
                    {
                        $limit -= $x;
                        $sql="select v.* from ktn_video as v, ktn_video_category as c, ktn_video_type as t where v.categoryid=c.id and c.videotypeid=t.id and c.videotypeid=6 and v.keywords is not null and v.publishdate<='".date("Y-m-d H:i:s")."' and v.inactive is null and v.id != '".$key->id."' and (";
                        $sqlt="";
                        foreach ($keywords as $keyword) 
                        {
                            $sqlt.="v.title like '%".$keyword."%' or v.keywords like '%".$keyword."%' or ";
                        }
                        $sql.=substr($sqlt,0,-3);

                        $sql.=") order by v.publishdate desc limit ".$limit."";
                        $dbh =  DB::connection('mysql3')->select($sql);
                        array_push($recommended,$dbh);
                        return $recommended;
                    }
                }
            }
        }
    }

    public function getVideos2($limit)
    {
        $dbh = $this->db->select('v.title, v.videoURL, v.id,c.name')
            ->from("ktn_video as v, ktn_video_category as c, ktn_video_type as t")
            ->where("v.categoryid=c.id AND c.videotypeid=t.id and t.source='evewoman' AND v.`inactive` IS   NULL")
            ->where("ktn_video.inactive is null")
            ->where("ktn_video_category.inactive is null")
            ->where("ktn_video_category.inactive is null")
            ->order_by("ktn_video.publishdate", "desc")
            ->join('ktn_video_category', 'ktn_video.categoryid = ktn_video_category.id')
            ->from('ktn_video')
            ->limit($limit)
            ->get();

        return $dbh->result_array();
    }

    public static function popular_articles($limit=15)
    {
        $dbh=DB::table('std_article')->select("id","title","publishdate","thumbURL","thumbcaption","createdby","author","author_id","categoryid")
            ->join("std_article_hits","std_article.id","=","std_article_hits.std_article_hits_article_id")
            ->where("source","evewoman")
            ->where("publishdate","<=",date("Y-m-d H:i:s"))
            ->whereNull('inactive')
            ->orderBy("std_article_hits","DESC")
            ->limit($limit)
            ->get();
        
        if(count($dbh) > 0)
        {
            return $dbh;
        }else{
            return [];
        }

    }

    public static function author($authorid,$limit,$start=0)
    {
        $dbh=DB::table('std_article')->where('author_id',$authorid)->where("source","evewoman")->where("publishdate","<=",date("Y-m-d H:i:s"))->orderBy('publishday','DESC')->limit($limit)
        ->offset($start)->get();
        if(count($dbh) > 0) {
            return $dbh;
        } else {
            $dbh = DB::connection('mysql3')->table('std_article')
            ->where('author_id',$authorid)->where("source","evewoman")->where("publishdate","<=",date("Y-m-d H:i:s"))->orderBy('publishday','DESC')->limit($limit)
            ->offset($start)->get();
            if (count($dbh) > 0) {
                return $dbh;
            } else {
                $dbh = DB::connection('mysql2')->table('std_article')
                ->where('author_id',$authorid)->where("source","evewoman")->where("publishdate","<=",date("Y-m-d H:i:s"))->orderBy('publishday','DESC')->limit($limit)
                ->offset($start)->get();
                if (count($dbh) > 0) {
                    return $dbh;
                } else {
                    return [];
                }
            }
        }
    }

    public static function getTrendingArticles($perPage, $period){
        $params = [
            'key' => 'ae48488c6ef1ea95354d3ffb5c496ca8',
            'entities' => [
                'articles' => [
                    'entity' => 'articles',
                    'details' => ["pageviews", "author", "category", "timeread", "social", "sources","id"],
                    "filters" => [
                        "category" => "Evewoman"
                    ]
                ]
            ],
            'options' => [
                'period' => [
                    'name' => $period
                ],
                'per_page' => $perPage
            ]
        ];
        $url = 'https://api.onthe.io/ata6CLk8UhmPPvS3PfZYx5wKAloQz24K';

        $client = new Client(['headers' => [ 'Content-Type' => 'application/x-www-form-urlencoded' ],'verify'=> true,'http_errors'=>false]);

        try {
            $result = $client->request('POST', $url, [ 'form_params' => $params]);
            $response = $result->getBody();
            $response = json_decode($response);
            return $response->articles->list;

        }catch (ClientException $e){
            echo "Ooopsy, you just hit a wall, my bad!";
        }

    }
}