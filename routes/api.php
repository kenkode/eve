<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/load_more', 'EveController@loadMore')->name('load_more');
Route::post('/topic_load_more', 'EveController@topicMore')->name('topic_load_more');
Route::post('/author_load_more', 'EveController@authorMore')->name('author_load_more');
