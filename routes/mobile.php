<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/whoops', function(){
    return view('whoops');
});
Route::get('/', 'AmpController@index')->name('index');
Route::get('{category}/article/{id}/{name}', 'AmpController@catArticle')->name('catArticle');
Route::get('/article/{id}/{name}', 'AmpController@article')->name('article');
Route::get('/author/{id}/{name}', 'AmpController@author')->name('author');
Route::get('/living', 'AmpController@living')->name('living');
Route::get('/fashion-and-beauty', 'AmpController@fashion')->name('fashion');
Route::get('/parenting', 'AmpController@parenting')->name('parenting');
Route::get('/relationships', 'AmpController@relationships')->name('relationships');
Route::get('/readers-lounge', 'AmpController@readers')->name('readers');
Route::get('/bridal', 'AmpController@bridal')->name('bridal');
Route::get('/category/{id}/{name}', 'AmpController@category')->name('category');
Route::get('/topic/{name}', 'AmpController@topic')->name('topic');
Route::get('/videos', 'AmpController@videos')->name('videos');
Route::get('videos/watch/{id}/{name}', 'AmpController@singleVideo')->name('singleVideo');
Route::get('/about-us', 'AmpController@about')->name('about');
Route::get('/search', 'AmpController@search')->name('search');
Route::post('/subscribe', 'AmpController@subscribe')->name('subscribe');
Auth::routes();
Route::get('/login', 'AmpController@index')->name('login');
Route::get('/register', 'AmpController@index')->name('register');
Route::post('/register', 'Auth\RegisterController@registration')->name('register');
Route::post('/profile/{id}', 'AmpController@profile')->name('profile');
Route::post('/password/{id}', 'AmpController@password')->name('password');
Route::get('/home', 'AmpController@index')->name('home');
