<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/whoops', function(){
    return view('whoops');
});
Route::get('/', 'EveController@index')->name('index');
Route::get('{category}/article/{id}/{name}', 'EveController@catArticle')->name('catArticle');
Route::get('{category}/article/{id}', 'EveController@catArticleId')->name('catArticleId');
Route::get('/sitemap', 'HomeController@sitemap')->name('sitemap');
Route::get('/googlenews', 'HomeController@googlenews')->name('googlenews');
Route::get('/article/{id}/{name}', 'EveController@article')->name('article');
Route::get('/article/{id}', 'EveController@articleId')->name('articleId');
Route::get('/author/{id}/{name}', 'EveController@author')->name('author');
Route::get('/living', 'EveController@living')->name('living');
Route::get('/fashion-and-beauty', 'EveController@fashion')->name('fashion');
Route::get('/parenting', 'EveController@parenting')->name('parenting');
Route::get('/relationships', 'EveController@relationships')->name('relationships');
Route::get('/readers-lounge', 'EveController@readers')->name('readers');
Route::get('/bridal', 'EveController@bridal')->name('bridal');
Route::get('/category/{id}/{name}', 'EveController@category')->name('category');
Route::get('/category/{id}', 'EveController@categoryId')->name('categoryId');
Route::get('/topic/{name}', 'EveController@topic')->name('topic');
Route::get('/videos', 'EveController@videos')->name('videos');
Route::get('videos/watch/{id}/{name}', 'EveController@singleVideo')->name('singleVideo');
Route::get('/about-us', 'EveController@about')->name('about');
Route::get('/search', 'EveController@search')->name('search');
Route::post('/subscribe', 'EveController@subscribe')->name('subscribe');
Auth::routes();
Route::get('/login', 'EveController@index')->name('login');
Route::get('/register', 'EveController@index')->name('register');
Route::post('/register', 'Auth\RegisterController@registration')->name('register');
Route::post('/profile/{id}', 'EveController@profile')->name('profile');
Route::post('/password/{id}', 'EveController@password')->name('password');
Route::get('/home', 'EveController@index')->name('home');
